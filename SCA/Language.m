//
//  Language.m
//  ChickenCode
//
//  Created by kyTang on 2/3/16.
//  Copyright © 2016 Apps Design Studio Sdn. Bhd. All rights reserved.
//

#import "Language.h"
#import "Constant.h"
#import <objc/runtime.h>

static const char _bundle=0;

@implementation Language

- (NSString*)localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName
{
    NSBundle* bundle=objc_getAssociatedObject(self, &_bundle);
    return bundle ? [bundle localizedStringForKey:key value:value table:tableName] : [super localizedStringForKey:key value:value table:tableName];
}

@end

@implementation NSBundle (Language)

+ (void)setLanguage:(NSString *)language
{
    [[NSUserDefaults standardUserDefaults] setObject:language forKey:APP_LANGUAGE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        object_setClass([NSBundle mainBundle],[Language class]);
    });
    objc_setAssociatedObject([NSBundle mainBundle], &_bundle, language ? [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"]] : nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
