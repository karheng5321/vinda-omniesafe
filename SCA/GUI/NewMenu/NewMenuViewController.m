//
//  NewMenuViewController.m
//  SCA
//
//  Created by kyTang on 15/12/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "NewMenuViewController.h"
#import "AsyncApi.h"
#import "ReportDetailsModel.h"
#import "BOSMenuViewController.h"
#import "AsyncApi.h"
#import "AppUtil.h"
#import "SelectSiteViewController.h"
#import "AppDelegate.h"

@interface NewMenuViewController () <AsyncApiDelegate> {
    UILabel *_ucBadgeLabel;
    UILabel *_bosBadgeLabel;
    
    UILabel *_titleLabel;
    UIView *_nameBg;
    UILabel *_usernameLabel;
}

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIView *unsafeConditionButtonView;
@property (weak, nonatomic) IBOutlet UIView *bosButtonView;
@property (weak, nonatomic) IBOutlet UILabel *unsafeConditionLabel;
@property (weak, nonatomic) IBOutlet UILabel *bosLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *unsafeConditionButtonViewTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bosButtonViewTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraint;
//@property (nonatomic, retain) UILabel *unsafeBadgeNumberLabel;
//@property (nonatomic, retain) UILabel *bosBadgeNumberLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bosBadgeWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *unsafeBadgeWithConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *unsafeBadgeTrailingConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *iconView;

@end

@implementation NewMenuViewController

//@synthesize unsafeBadgeNumberLabel = _unsafeBadgeNumberLabel;
//@synthesize bosBadgeNumberLabel = _bosBadgeNumberLabel;

static NSString* const GET_PAGE_DATA_CALL_ID = @"getpagedata";

static CGFloat const  badgeMinSize = 40.0;

- (void) setupLayout
{
    CGFloat prefixWidth = [[UIScreen mainScreen] bounds].size.width-(70*2);
    
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
        self.bosButtonViewTopConstraint.constant = 10;
        
        /////iphone 4 issue
        self.unsafeConditionButtonViewTopConstraint.constant = 65.0;
        prefixWidth = [[UIScreen mainScreen] bounds].size.width-(90*2);
    }
    
    if ([GeneralHelper isDeviceiPhone6plus])
    {
        self.unsafeConditionButtonViewTopConstraint.constant = 100;
        prefixWidth = [[UIScreen mainScreen] bounds].size.width-(120*2);
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.unsafeConditionButtonViewTopConstraint.constant = 80;
        prefixWidth = [[UIScreen mainScreen] bounds].size.width-(95*2);
    }
    
    NSLog(@"self.topBarHeightConstraint.constant: %f", self.topBarHeightConstraint.constant);
    NSLog(@"self.unsafeConditionButtonViewTopConstraint.constant: %f", self.unsafeConditionButtonViewTopConstraint.constant);
    
    self.buttonWidthConstraint.constant = prefixWidth;
    self.buttonHeightConstraint.constant = prefixWidth;
    
    self.unsafeConditionButtonView.layer.cornerRadius = prefixWidth/2;
    self.unsafeConditionButtonView.layer.masksToBounds = YES;
    
    self.bosButtonView.layer.cornerRadius = prefixWidth/2;
    self.bosButtonView.layer.masksToBounds = YES;
    
    self.unsafeConditionLabel.text = NSLocalizedString(@"UNSAFE_CONDITION", nil);
    self.bosLabel.text = NSLocalizedString(@"BOS", nil);
    
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.unsafeConditionLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
        self.bosLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    }
    
    _ucBadgeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _ucBadgeLabel.textAlignment = NSTextAlignmentCenter;
    _ucBadgeLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
    _ucBadgeLabel.backgroundColor = [UIColor redColor];
    _ucBadgeLabel.textColor = [UIColor whiteColor];
    _ucBadgeLabel.layer.cornerRadius = badgeMinSize/2.0;
    _ucBadgeLabel.clipsToBounds = YES;
    [self.unsafeConditionButtonView.superview addSubview:_ucBadgeLabel];
    
    _bosBadgeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _bosBadgeLabel.textAlignment = NSTextAlignmentCenter;
    _bosBadgeLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
    _bosBadgeLabel.backgroundColor = [UIColor redColor];
    _bosBadgeLabel.textColor = [UIColor whiteColor];
    _bosBadgeLabel.layer.cornerRadius = badgeMinSize/2.0;
    _bosBadgeLabel.clipsToBounds = YES;
    [self.bosButtonView.superview addSubview:_bosBadgeLabel];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    BOOL isBosAdmin = [[roles objectForKey:ROLES_BOS_SUPERVISOR_KEY] boolValue];
    
    if (!(isAdmin || isBosAdmin))
    {
        _ucBadgeLabel.hidden = YES;
        _bosBadgeLabel.hidden = YES;
    }
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, self.iconView.frame.origin.y+self.iconView.frame.size.height+5.0, self.view.frame.size.width-2*10.0, 60.0)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor colorWithRed:11.0/255.0 green:47.0/255.0 blue:143.0/255.0 alpha:1.0];
    _titleLabel.text = @"MAIN MENU";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:24.0];
    _titleLabel.numberOfLines = 1;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(self.view.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _titleLabel.frame.origin.y, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [self.view addSubview:_titleLabel];
    
    _nameBg = [[UIView alloc] initWithFrame:CGRectMake(0.0, _titleLabel.frame.origin.y+_titleLabel.frame.size.height+5.0, self.view.frame.size.width, 25.0)];
    _nameBg.backgroundColor = [UIColor colorWithRed:11.0/255.0 green:47.0/255.0 blue:143.0/255.0 alpha:1.0];
    [self.view addSubview:_nameBg];
    
    _usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, _nameBg.frame.size.width-2*10.0, _nameBg.frame.size.height)];
    _usernameLabel.backgroundColor = [UIColor clearColor];
    _usernameLabel.textColor = [UIColor whiteColor];
    _usernameLabel.text = [user objectForKey:@"ism_user_fullname"];
    _usernameLabel.textAlignment = NSTextAlignmentCenter;
    _usernameLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    _usernameLabel.numberOfLines = 1;
    _usernameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_nameBg addSubview:_usernameLabel];
    
    BOOL isSite1 = [[roles objectForKey:ROLES_SITE1_KEY] boolValue];
    BOOL isSite2 = [[roles objectForKey:ROLES_SITE2_KEY] boolValue];
    
    self.backButton.hidden = !(isSite1 || isSite2);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self getPageData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

#pragma mark - UIButtons Actions
- (IBAction)unsafeConditionPressed:(id)sender
{
    NSLog(@"unsafeConditionPressed");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    //BOOL isUser = [[roles objectForKey:ROLES_USER_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    
    if (isAdmin || isOwner || isMonitor)
    {
        [self performSegueWithIdentifier:@"gotoMenuFromNewMenu" sender:self];
    }
    else
    {
        [self performSegueWithIdentifier:@"gotoUserMenuFromNewMenu" sender:self];
    }
}

- (IBAction)bosPressed:(id)sender
{
    BOSMenuViewController *bosVC = [[BOSMenuViewController alloc] init];
    [self.navigationController pushViewController:bosVC animated:YES];
}

- (IBAction)logoutPressed:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"logout_alert", nil)
                                                    message:NSLocalizedString(@"logout_message", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"yes", nil), nil];
    [alert show];
}

- (IBAction)backPressed:(id)sender
{
    SelectSiteViewController *selectVC = [[SelectSiteViewController alloc] init];
    
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appDelegate.window duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void)
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         
         [appDelegate.window setRootViewController:selectVC];
         [appDelegate.window makeKeyAndVisible];
         
         [UIView setAnimationsEnabled:oldState];
         
     }completion:nil];
}

#pragma mark - Segue
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"gotoMenuFromNewMenu"])
    {
        // TODO: goto Admin & Supervisor Main Menu
    }
    else if ([segue.identifier isEqualToString:@"gotoUserMenuFromNewMenu"])
    {
        
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
        {
            [self logoutFromServer];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults removeObjectForKey:NSUserDefaultKey_USER];
            [ReportDetailsModel clearAllReportDetailKey];
            [self switchToDashboard:@"LoginViewController"];
        }
            break;
    }
}

- (void)logoutFromServer
{
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:nil];
    [asyncApi logoutFromServer:nil];
}

- (void)getPageData
{
    NSLog(@"bosGetBadgeNumber");
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi bosGetBadgeNumber:GET_PAGE_DATA_CALL_ID];
}

- (void)refreshBadge:(NSMutableDictionary *)badge
{
    NSString *ucBadge = [NSString stringWithFormat:@"%@", [badge valueForKey:@"uc"]];
    NSString *bosBadge = [NSString stringWithFormat:@"%@", [badge valueForKey:@"bos"]];
    
    _ucBadgeLabel.text = ucBadge;
    _bosBadgeLabel.text = bosBadge;
    
    CGFloat textMaxWidth = self.view.frame.size.width/4.0;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _ucBadgeLabel.font, NSFontAttributeName,
                                  nil];
    CGRect ucRect = [ucBadge boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    CGRect bosRect = [bosBadge boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    CGFloat ucWidth = MAX(ucRect.size.width+20.0, badgeMinSize);
    _ucBadgeLabel.frame = CGRectMake(self.unsafeConditionButtonView.frame.origin.x+self.unsafeConditionButtonView.frame.size.width*0.75, self.unsafeConditionButtonView.frame.origin.y, ucWidth, badgeMinSize);
    
    CGFloat bosWidth = MAX(bosRect.size.width+20.0, badgeMinSize);
    _bosBadgeLabel.frame = CGRectMake(self.bosButtonView.frame.origin.x+self.bosButtonView.frame.size.width*0.75, self.bosButtonView.frame.origin.y, bosWidth, badgeMinSize);
    
    _ucBadgeLabel.hidden = ([ucBadge integerValue] == 0);
    _bosBadgeLabel.hidden = ([bosBadge integerValue] == 0);
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[ucBadge integerValue]+[bosBadge integerValue]];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_PAGE_DATA_CALL_ID])
    {
        //[self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableDictionary *content = [AppUtil removeNullElementFromObject:result.result];
            [self refreshBadge:content];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
