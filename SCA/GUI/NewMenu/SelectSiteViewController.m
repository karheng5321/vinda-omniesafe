//
//  SelectSiteViewController.m
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "SelectSiteViewController.h"
#import "AsyncApi.h"
#import "Constant.h"
#import "AppUtil.h"
#import "BOSObservationViewController.h"
#import "BOSReportListViewController.h"
#import "SafeViewController.h"
#import "AppDelegate.h"
#import "ReportDetailsModel.h"
#import "NewMenuViewController.h"
#import "MainMenuViewController.h"
#import "AdminMainMenuViewController.h"

@interface SelectSiteViewController () <AsyncApiDelegate, UITextFieldDelegate> {
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    float _titleHeight;
    
    UIView *_loadingView;
    
    UIView *_navView;
    UILabel *_titleLabel;
    UILabel *_usernameLabel;
    UIScrollView *_mainView;
    NSMutableArray *_dataArray;
    
    UILabel *_bosBadgeLabel;
    UIButton *_deptBtn;
    
    UIButton *_unsaveBtn;
}

@end

@implementation SelectSiteViewController

static CGFloat const  badgeMinSize = 40.0;

static NSString* const GET_PAGE_DATA_CALL_ID = @"getpagedata";

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        //self.navigationItem.title = @"";
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
    }
    return self;
}

- (void)initPage
{
    _statusBarHeight = 0.0;
    _navBarHeight = 120.0;
    _titleHeight = 95.0;
    _tabBarHeight = 0.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_statusBarHeight-_tabBarHeight;
    
    _navView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _screenWidth, _navBarHeight)];
    _navView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_navView];
    
    /*_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(51.0, 0.0, _screenWidth-2*51.0, _navView.frame.size.height)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor blackColor];
    _titleLabel.text = NSLocalizedString(@"select_site_title", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:35.0];
    _titleLabel.numberOfLines = 2;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(_navView.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _navView.frame.size.height/2.0-_titleLabel.frame.size.height/2.0, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [_navView addSubview:_titleLabel];*/
    
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(_navView.frame.size.width/2.0-155.0/2.0, _navView.frame.size.height/2.0-76.0/2.0+4.0, 155.0, 76.0)];
    logoView.image = [UIImage imageNamed:@"OmnieSafe icon.png"];
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    [_navView addSubview:logoView];
    
    /*UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(8.0, 42.0, 35.0, _navBarHeight-2*42);
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];*/
    
    //OmnieSafe icon
    
    _usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, _navBarHeight-20.0, _screenWidth-2*10.0, 20.0)];
    _usernameLabel.backgroundColor = [UIColor clearColor];
    _usernameLabel.textColor = [UIColor darkGrayColor];
    _usernameLabel.text = NSLocalizedString(@"select_site_title", nil);
    _usernameLabel.textAlignment = NSTextAlignmentCenter;
    _usernameLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    _usernameLabel.numberOfLines = 1;
    _usernameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_navView addSubview:_usernameLabel];
    
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _navBarHeight+_statusBarHeight, _screenWidth, _screenHeight)];
    _mainView.backgroundColor = [UIColor clearColor];
    _mainView.showsHorizontalScrollIndicator = NO;
    _mainView.bounces = NO;
    _mainView.contentSize = _mainView.frame.size;
    [self.view addSubview:_mainView];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _mainView.frame.size.width, 3.0)];
    line.backgroundColor = [UIColor appSiteGreyColor];
    [_mainView addSubview:line];
    
    CGFloat sideGap = 10.0;
    CGFloat btnWidth = (_mainView.frame.size.width-4*sideGap)/2.0;
    CGFloat currentHeight = _mainView.frame.size.height/2.0-sideGap-btnWidth;
    
    UIButton *reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    reportBtn.frame = CGRectMake(_mainView.frame.size.width/2.0-btnWidth/2.0, currentHeight, btnWidth, btnWidth);
    reportBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [reportBtn addTarget:self action:@selector(site1Tapped:) forControlEvents:UIControlEventTouchUpInside];
    reportBtn.backgroundColor = [UIColor appUnsafeBlueColor];
    reportBtn.layer.cornerRadius = btnWidth/2.0;
    [_mainView addSubview:reportBtn];
    
    CGFloat iconSize = btnWidth/2.0;
    UIImageView *reportIcon = [[UIImageView alloc] initWithFrame:CGRectMake(reportBtn.frame.size.width/2.0-iconSize/2.0, btnWidth*0.1, iconSize, iconSize)];
    reportIcon.image = [UIImage imageNamed:@"icon_sites.png"];
    reportIcon.contentMode = UIViewContentModeScaleAspectFit;
    [reportBtn addSubview:reportIcon];
    
    CGFloat initY = reportIcon.frame.origin.y+reportIcon.frame.size.height;
    UILabel *reportLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnWidth*0.1, initY, reportBtn.frame.size.width-2.0*btnWidth*0.1, reportBtn.frame.size.height-initY-btnWidth*0.1)];
    reportLabel.backgroundColor = [UIColor clearColor];
    reportLabel.textColor = [UIColor whiteColor];
    reportLabel.text = NSLocalizedString(@"Site_1", nil);
    reportLabel.textAlignment = NSTextAlignmentCenter;
    reportLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
    reportLabel.numberOfLines = 2;
    reportLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [reportBtn addSubview:reportLabel];
    
    currentHeight += reportBtn.frame.size.height+2*sideGap;
    
    reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    reportBtn.frame = CGRectMake(_mainView.frame.size.width/2.0-btnWidth/2.0, currentHeight, btnWidth, btnWidth);
    reportBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [reportBtn addTarget:self action:@selector(site2Tapped:) forControlEvents:UIControlEventTouchUpInside];
    reportBtn.backgroundColor = [UIColor appSiteTurqoiseColor];
    reportBtn.layer.cornerRadius = btnWidth/2.0;
    [_mainView addSubview:reportBtn];
    
    iconSize = btnWidth/2.0;
    reportIcon = [[UIImageView alloc] initWithFrame:CGRectMake(reportBtn.frame.size.width/2.0-iconSize/2.0, btnWidth*0.1, iconSize, iconSize)];
    reportIcon.image = [UIImage imageNamed:@"icon_sites.png"];
    reportIcon.contentMode = UIViewContentModeScaleAspectFit;
    [reportBtn addSubview:reportIcon];
    
    initY = reportIcon.frame.origin.y+reportIcon.frame.size.height;
    reportLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnWidth*0.1, initY, reportBtn.frame.size.width-2.0*btnWidth*0.1, reportBtn.frame.size.height-initY-btnWidth*0.1)];
    reportLabel.backgroundColor = [UIColor clearColor];
    reportLabel.textColor = [UIColor whiteColor];
    reportLabel.text = NSLocalizedString(@"Site_2", nil);
    reportLabel.textAlignment = NSTextAlignmentCenter;
    reportLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
    reportLabel.numberOfLines = 2;
    reportLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [reportBtn addSubview:reportLabel];
    
    UIImage *btnImage = [UIImage imageNamed:@"logout_icon.png"];
    CGFloat scale = btnImage.size.width/20.0;
    btnWidth = btnImage.size.width/scale;
    CGFloat btnHeight = btnImage.size.height/scale;
    
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoutBtn.frame = CGRectMake(_mainView.frame.size.width-10.0-btnWidth, _mainView.frame.size.height-10.0-btnHeight, btnWidth, btnHeight);
    logoutBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [logoutBtn setImage:btnImage forState:UIControlStateNormal];
    [logoutBtn addTarget:self action:@selector(logoutTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_mainView addSubview:logoutBtn];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)logoutTapped:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"logout_alert", nil) message:NSLocalizedString(@"logout_message", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self logoutFromServer];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:NSUserDefaultKey_USER];
        [ReportDetailsModel clearAllReportDetailKey];
        [self switchToDashboard:@"LoginViewController"];
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)logoutFromServer
{
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:nil];
    [asyncApi logoutFromServer:nil];
}

- (void)switchToDashboard: (NSString *) navcontrollerIdentifier
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appDelegate.window duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void)
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
         
         UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:navcontrollerIdentifier];
         //[appDelegate.window addSubview:navigationController.view];
         [appDelegate.window setRootViewController:navigationController];
         [appDelegate.window makeKeyAndVisible];
         
         [UIView setAnimationsEnabled:oldState];
         
     }completion:nil];
}

- (void)site1Tapped:(UIButton *)sender
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDel.API_SITE_URL = SITE1_API_LOCATION_PREFIX;
    [self proceedNext];
    
    [appDel setSiteIndicator:@"S1"];
}

- (void)site2Tapped:(UIButton *)sender
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDel.API_SITE_URL = SITE2_API_LOCATION_PREFIX;
    [self proceedNext];
    
    [appDel setSiteIndicator:@"S2"];
}

- (void)proceedNext
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDel.ON_DIFFERENT_SITE = ![appDel.API_SITE_URL isEqualToString:API_LOCATION_PREFIX];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSLog(@"user: %@", user);
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    //BOOL isUser = [[roles objectForKey:ROLES_USER_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    BOOL isBosSuper = [[roles objectForKey:ROLES_BOS_SUPERVISOR_KEY] boolValue];
    BOOL isBosUser = [[roles objectForKey:ROLES_BOS_USER_KEY] boolValue];
    //BOOL isSite1 = [[roles objectForKey:ROLES_SITE1_KEY] boolValue];
    //BOOL isSite2 = [[roles objectForKey:ROLES_SITE2_KEY] boolValue];
    
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if (isBosSuper || isBosUser || isAdmin)
    {
        [self switchToDashboard:@"NewMenuView"];
        
        //NewMenuViewController *reportVC = [storyboard instantiateViewControllerWithIdentifier:@"NewMenuView"];
        //[self.navigationController pushViewController:reportVC animated:YES];
    }
    else if (isOwner || isMonitor)
    {
        [self switchToDashboard:@"AdminMainMenuView"];
        //[self switchToDashboard:@"NewMenuView"];
        
        //AdminMainMenuViewController *reportVC = [storyboard instantiateViewControllerWithIdentifier:@"AdminMainMenuView"];
        //[self.navigationController pushViewController:reportVC animated:YES];
    }
    else
    {
        [self switchToDashboard:@"UserMainMenuView"];
        
        //MainMenuViewController *reportVC = [storyboard instantiateViewControllerWithIdentifier:@"UserMainMenuView"];
        //[self.navigationController pushViewController:reportVC animated:YES];
    }
}

- (UIView *)findFirstResponder
{
    for (UIView *subview in _mainView.subviews) {
        if ([subview isFirstResponder])
            return subview;
    }
    
    return nil;
}

- (void)expandMainView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight);
    
    [UIView commitAnimations];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)collapseMainView:(float)keyboardHeight
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight-keyboardHeight+_tabBarHeight);
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDel setSiteIndicator:@""];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /*if ([self.navigationController.viewControllers count] <= 1)
    {
        UIImage *btnImage = [UIImage imageNamed:@"logout_icon.png"];
        float scale = btnImage.size.width/20.0;
        float btnWidth = btnImage.size.width/scale;
        float btnHeight = btnImage.size.height/scale;
        
        UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        logoutBtn.frame = CGRectMake(_mainView.frame.size.width-10.0-btnWidth, _mainView.frame.size.height-10.0-btnHeight, btnWidth, btnHeight);
        logoutBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [logoutBtn setImage:btnImage forState:UIControlStateNormal];
        [logoutBtn addTarget:self action:@selector(logoutTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_mainView addSubview:logoutBtn];
    }*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate

- (void)keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    //NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    NSLog(@"keyboardBounds: %f, %f, %f, %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.height, keyboardBounds.size.width);
    //[self collapseMainView:keyboardBounds.size.height];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    //[self expandMainView];
    
    //[self performSearch];
    
    return YES;
}

- (void)textFieldDidChanged:(NSNotification *)notification
{
    //_searchPlaceholder.hidden = _searchBar.text.length;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_PAGE_DATA_CALL_ID])
    {
        //[self setLoadingHidden:YES];
        
        if (result.status)
        {
            
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
