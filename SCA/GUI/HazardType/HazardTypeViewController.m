//
//  HazardTypeViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "HazardTypeViewController.h"
#import "HazardTypeModel.h"
#import "HazardTypeTableViewCell.h"
#import "AsyncApi.h"
#import "AppUtil.h"
#import "AppDelegate.h"

@interface HazardTypeViewController () <AsyncApiDelegate> {
    UIView *_loadingView;
}

@property (weak, nonatomic) IBOutlet UITableView *hazardtypeTable;
@end

@implementation HazardTypeViewController

static NSString * const GET_LIST_CALL_ID = @"getlist";

NSMutableArray *hazardTypeArray;

- (void)setupLayout
{
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    _loadingView.center = self.view.center;//CGPointMake(_screenWidth/2.0, _screenHeight/2.0);
    _loadingView.layer.cornerRadius = 10.0;
    _loadingView.hidden = YES;
    //[self.view addSubview:_loadingView];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
    [_loadingView addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void) createData
{
    hazardTypeArray = [NSMutableArray new];
    
    HazardTypeModel *sample1 = [HazardTypeModel new];
    sample1.hazardName = @"Machinery / Work Equipment";
    sample1.isChecked = NO;
    
    HazardTypeModel *sample2 = [HazardTypeModel new];
    sample2.hazardName = @"Electrical";
    sample2.isChecked = NO;
    
    HazardTypeModel *sample3 = [HazardTypeModel new];
    sample3.hazardName = @"Slip / Trip & Fall";
    sample3.isChecked = NO;
    
    HazardTypeModel *sample4 = [HazardTypeModel new];
    sample4.hazardName = @"Chemical";
    sample4.isChecked = NO;
    
    HazardTypeModel *sample5 = [HazardTypeModel new];
    sample5.hazardName = @"Ergonomic";
    sample5.isChecked = NO;
    
    HazardTypeModel *sample6 = [HazardTypeModel new];
    sample6.hazardName = @"Housekeeping & 5S";
    sample6.isChecked = NO;
    
    HazardTypeModel *sample7 = [HazardTypeModel new];
    sample7.hazardName = @"Not Sure";
    sample7.isChecked = NO;
    
    [hazardTypeArray addObjectsFromArray:@[sample1, sample2, sample3, sample4, sample5, sample6, sample7]];
}

- (void)processData:(NSMutableArray *)array
{
    hazardTypeArray = [NSMutableArray new];
    
    for (NSInteger i = 0; i < [array count]; i++)
    {
        NSDictionary *dict = [array objectAtIndex:i];
        
        HazardTypeModel *hazard = [HazardTypeModel new];
        hazard.hazardName = [dict objectForKey:@"name"];
        hazard.isChecked = NO;
        hazard.hazardId = [[dict objectForKey:@"id"] integerValue];
        
        [hazardTypeArray addObject:hazard];
    }
}

- (void)downloadData
{
    hazardTypeArray = nil;
    [self.hazardtypeTable reloadData];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDel isInternetAvailable])
    {
        NSMutableArray *dataArray = isUpdate? [[defaults objectForKey:NSUserDefaultKey_HAZARD_CONTROL] mutableCopy]:[[defaults objectForKey:NSUserDefaultKey_HAZARD_LIST] mutableCopy];
        if (!dataArray)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"no_internet", nil) message:NSLocalizedString(@"retry", nil) delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        [self processData:dataArray];
    }
    else
    {
        //[self setLoadingHidden:NO];
        
        AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
        if (isUpdate)
            [asyncApi getHazardControlList:GET_LIST_CALL_ID];
        else
            [asyncApi getHazardList:GET_LIST_CALL_ID];
    }
}

/*- (void)downloadData
{
    hazardTypeArray = nil;
    [self.hazardtypeTable reloadData];
    
    [self setLoadingHidden:NO];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    if (isUpdate)
        [asyncApi getHazardControlList:GET_LIST_CALL_ID];
    else
        [asyncApi getHazardList:GET_LIST_CALL_ID];
}*/

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupLayout];
    //[self createData];
    
    NSLog(@"HazardTypeViewController viewDidLoad");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self downloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Some calculation to determine height of row/cell.
    return 45;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return hazardTypeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HazardTypeTableViewCell *cell = (HazardTypeTableViewCell* )[tableView dequeueReusableCellWithIdentifier:@"HazardTypeCell"];
    [cell updateDisplay:hazardTypeArray[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setGreyWhiteCellColor:cell row:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (HazardTypeModel *model in hazardTypeArray)
    {
        model.isChecked = NO;
    }
    
    HazardTypeModel *model = (HazardTypeModel *)hazardTypeArray[indexPath.row];
    model.isChecked = !model.isChecked;
    
    [ReportDetailsModel saveValue: model.hazardName key:NSUSERDEFAULT_KEY_HAZARDTYPE];
    [ReportDetailsModel saveValue: [NSNumber numberWithInteger:model.hazardId] key:NSUSERDEFAULT_KEY_HAZARDID];
    [tableView reloadData];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_LIST_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableArray *array = (NSMutableArray *)result.result;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
            if (isUpdate)
                [defaults setObject:array forKey:NSUserDefaultKey_HAZARD_CONTROL];
            else
                [defaults setObject:array forKey:NSUserDefaultKey_HAZARD_LIST];
            
            [self processData:array];
            [self.hazardtypeTable reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
