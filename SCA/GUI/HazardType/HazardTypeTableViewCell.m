//
//  HazardTypeTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "HazardTypeTableViewCell.h"
#import "GeneralHelper.h"

@interface HazardTypeTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *hazardtypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *booleanImage;

@end

@implementation HazardTypeTableViewCell

- (void) updateDisplay:(HazardTypeModel *)model
{
    [self.hazardtypeLabel setText: model.hazardName];
    
    if (model.isChecked)
    {
        [self.booleanImage setImage: [UIImage imageNamed:@"radio_black"]];
    }
    else
    {
        [self.booleanImage setImage: [UIImage imageNamed:@"radio_empty"]];
    }
    

}

@end
