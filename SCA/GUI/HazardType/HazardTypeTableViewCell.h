//
//  HazardTypeTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HazardTypeModel.h"

@interface HazardTypeTableViewCell : UITableViewCell

- (void) updateDisplay:(HazardTypeModel *)model;

@end
