//
//  ReportDetailsTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "reviewUIModel.h"

@interface ReportDetailsTableViewCell : UITableViewCell

- (void) updateDisplay:(reviewUIModel *)model;

@end
