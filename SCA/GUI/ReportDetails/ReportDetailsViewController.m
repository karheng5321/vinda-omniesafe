//
//  ReportDetailsViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "ReportDetailsViewController.h"
#import "ReportDetailsTableViewCell.h"
#import "ReportDetailsPictureTableViewCell.h"
#import "MWPhotoBrowser.h"
#import "AsyncApi.h"
#import "AppUtil.h"
#import "InfoViewController.h"

@interface ReportDetailsViewController ()<ReportDetailsPictureTableViewCellDelegate, MWPhotoBrowserDelegate, AsyncApiDelegate> {
    UIView *_loadingView;
    
    NSMutableArray *_updateArray;
    NSDictionary *_dataDict;
    
    NSString *_activeUrl;
    UINavigationController *_activeVC;
}

@property (weak, nonatomic) IBOutlet UITableView *reportDetailTableView;
@property (nonatomic, strong) NSMutableArray *reportDetails;
@property (strong, nonatomic) NSMutableDictionary *offscreenCells;
@property (strong, nonatomic) NSMutableArray *imageViewerArray;

@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;

@end

@implementation ReportDetailsViewController

@synthesize fromNotification = _fromNotification;
@synthesize sameSite = _sameSite;

static NSString * const GET_DETAILS_CALL_ID = @"getdetails";
static NSString * const CLOSE_REPORT_CALL_ID = @"closereport";

static NSInteger const CLOSE_REPORT_ALERT_TAG = 999;

- (void) setupLayout
{
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
        [self.okButton setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-50, 35)];
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        [self.okButton setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-50, 40)];
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.topBarHeightConstraint.constant = 120;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    //BOOL isUser = [[roles objectForKey:ROLES_USER_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    //BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    NSLog(@"userId: %@", userId);
    NSLog(@"self.ownerId: %@", self.ownerId);
    
    if (_fromNotification)
        [self.okButton setTitle:@"OK" forState:UIControlStateNormal];
    else if (isAdmin)
        [self.okButton setTitle:NSLocalizedString(@"close_report", nil) forState:UIControlStateNormal];
    else if (isOwner && [userId isEqualToString:self.ownerId])
        [self.okButton setTitle:NSLocalizedString(@"take_action", nil) forState:UIControlStateNormal];
    else
        [self.okButton setTitle:@"OK" forState:UIControlStateNormal];
    
    self.reportDetailTableView.backgroundColor = [UIColor clearColor];
    
    self.updateButton.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
    [self roundButton: self.okButton];
    [self roundButton: self.updateButton];
    
    //[self createData];
    [self downloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

#pragma mark - UIButtons Actions
- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    reviewUIModel *model = self.reportDetails[indexPath.row];
    
    switch (model.cellType)
    {
        case SubmitCellTypeText:
        {
            NSString *reuseIdentifier = NSStringFromClass([ReportDetailsTableViewCell class]);
            
            ReportDetailsTableViewCell *cell = [self.offscreenCells objectForKey:reuseIdentifier];
            if(cell == nil)
            {
                cell = (ReportDetailsTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
            }
            //[cell updateDisplay:self.reportDetails[indexPath.row]];
            [cell updateDisplay:(indexPath.section == 0)? self.reportDetails[indexPath.row]:_updateArray[indexPath.section-1][indexPath.row]];
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            
            cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
            
            [cell setNeedsLayout];
            [cell layoutIfNeeded];
            
            CGFloat height = 0.0;
            height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            
            height += 1;
            return height;
        }
            break;
            
        case SubmitCellTypeMedia:
        {
            return 71;
        }
            break;
            
        default:
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_updateArray count]+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return [self.reportDetails count];
    else
    {
        NSArray *details = [_updateArray objectAtIndex:section-1];
        return [details count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (section == 0)? 40.0:40.0;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 40.0)];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10.0, 40.0-1.0, tableView.frame.size.width-20.0, 1.0)];
    line.backgroundColor = [UIColor grayColor];
    [headerView addSubview:line];
    
    if (section == 0)
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor blackColor];
        label.text = [[_dataDict objectForKey:@"report"] objectForKey:@"code"];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Roboto-Medium" size:18.0];
        [label sizeToFit];
        label.frame = CGRectMake(headerView.frame.size.width/2.0-label.frame.size.width/2.0, headerView.frame.size.height/2.0-label.frame.size.height/2.0, label.frame.size.width, label.frame.size.height);
        [headerView addSubview:label];
        
        return headerView;
    }
    
    NSArray *updates = [_dataDict objectForKey:@"update"];
    NSDictionary *update = [updates objectAtIndex:section-1];
    NSString *timestampString = [update valueForKey:@"added_timestamp"];
    NSLog(@"timestampString: %@", timestampString);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *reportDate = [dateFormatter dateFromString:timestampString];
    [dateFormatter setDateFormat:@"dd/MM/yyyy, hh:mm aa"];
    NSString *submitDateString = [dateFormatter stringFromDate:reportDate];
    NSLog(@"submitDateString: %@", submitDateString);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"update_at", nil), submitDateString];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    [label sizeToFit];
    label.frame = CGRectMake(headerView.frame.size.width/2.0-label.frame.size.width/2.0, headerView.frame.size.height/2.0-label.frame.size.height/2.0, label.frame.size.width, label.frame.size.height);
    [headerView addSubview:label];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReportDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ReportDetailsTableViewCell class])];
    reviewUIModel *model = (indexPath.section == 0)? self.reportDetails[indexPath.row]:_updateArray[indexPath.section-1][indexPath.row];
    
    switch (model.cellType)
    {
        case SubmitCellTypeText:
        {
            ReportDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ReportDetailsTableViewCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            //[cell updateDisplay:self.reportDetails[indexPath.row]];
            [cell updateDisplay:(indexPath.section == 0)? self.reportDetails[indexPath.row]:_updateArray[indexPath.section-1][indexPath.row]];
            [self.offscreenCells setObject:cell forKey:@"SubmitTableViewCell"];
            
            return cell;
        }
            break;
            
        case SubmitCellTypeMedia:
        {
            ReportDetailsPictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ReportDetailsPictureTableViewCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            cell.delegate = self;
            cell.fileUrl = model.fileUrl;
            //[cell updateDisplay:self.reportDetails[indexPath.row]];
            [cell updateDisplay:(indexPath.section == 0)? self.reportDetails[indexPath.row]:_updateArray[indexPath.section-1][indexPath.row]];
            
            return cell;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - UIButtons Actions

- (IBAction)okTapped:(id)sender
{
    if (_fromNotification)
    {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    //BOOL isUser = [[roles objectForKey:ROLES_USER_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    //BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    
    if (isAdmin)
        [self closeReportTapped:sender];
    else if (isOwner && [userId isEqualToString:self.ownerId])
        [self updateReport];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)updateTapped:(id)sender
{
    [self updateReport];
}

#pragma mark - ReportDetailsPictureTableViewCellDelegate
- (void) ReportDetailsPictureTableViewCellDidClickAtImage:(NSString *)imageUrl
{
    NSLog(@"ReportDetailsPictureTableViewCellDidClickAtImageIndex %@", imageUrl);
    
    _activeUrl = imageUrl;
    
    if ([imageUrl rangeOfString:@".mp4"].length)
    {
        MPMoviePlayerViewController *player = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:imageUrl]];
        //player.moviePlayer.controlStyle = MPMovieControlStyleNone;
        //player.view.userInteractionEnabled = NO;
        //player.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
        player.moviePlayer.shouldAutoplay = YES;
        //[player.view setFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
        //[player.moviePlayer.backgroundView addSubview:splashBg];
        
        /*UIImage *btnImage = [UIImage imageNamed:@"share_icon.png"];
        float scale = btnImage.size.width/20.0;
        float btnWidth = btnImage.size.width/scale;
        float btnHeight = btnImage.size.height/scale;
        
        UIButton *sideMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sideMenuBtn.frame = CGRectMake(0.0, 0.0, btnWidth, btnHeight);
        sideMenuBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [sideMenuBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
        [sideMenuBtn addTarget:self action:@selector(shareMedia:) forControlEvents:UIControlEventTouchUpInside];
        player.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sideMenuBtn];
        
        UINavigationController * nc = [[UINavigationController alloc] initWithRootViewController:player];
        nc.navigationBar.barTintColor = [UIColor blackColor];*/
        [self presentViewController:player animated:YES completion:nil];
        return;
    }
    
    self.imageViewerArray = [NSMutableArray new];
    
    MWPhoto *p = [MWPhoto photoWithURL:[NSURL URLWithString:imageUrl]];
    [self.imageViewerArray addObject:p];
    
    [self performPhotoSelectionAction:0];
}


#pragma mark - MWPhotoBrowserDelegate
- (void)performPhotoSelectionAction:(NSInteger)initialIndex
{
    MWPhotoBrowser * browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    [browser setCurrentPhotoIndex:initialIndex];
    browser.displayActionButton = NO;
    browser.enableLongPressToSave = YES;
    
    UINavigationController * nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nc animated:YES completion:nil];
    
    /*UIImage *btnImage = [UIImage imageNamed:@"share_icon.png"];
    float scale = btnImage.size.width/20.0;
    float btnWidth = btnImage.size.width/scale;
    float btnHeight = btnImage.size.height/scale;
    
    UIButton *sideMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sideMenuBtn.frame = CGRectMake(0.0, 0.0, btnWidth, btnHeight);
    sideMenuBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [sideMenuBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
    [sideMenuBtn addTarget:self action:@selector(shareMedia:) forControlEvents:UIControlEventTouchUpInside];
    browser.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sideMenuBtn];*/
    
    browser.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"share_icon.png"] style:UITableViewStylePlain target:self action:@selector(shareMedia:)];
    
    _activeVC = nc;
}

- (void)shareMedia:(UIBarButtonItem *)sender
{
    NSLog(@"shareMedia");
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]
                                            initWithActivityItems:[NSArray arrayWithObjects:_activeUrl, nil]
                                            applicationActivities:nil];
    [activityVC setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
        NSLog(@"activityVC CompletionWithItemsHandler: %d", completed);
    }];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor blackColor]];
    
    NSDictionary *attributes = @{
                                 NSForegroundColorAttributeName : [UIColor blackColor],
                                 NSFontAttributeName: [UIFont appFontWithSize:18.0]
                                 };
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    
    [[UISegmentedControl appearance] setTintColor:[UIColor whiteColor]];
    
    [_activeVC presentViewController:activityVC animated:YES completion:nil];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [self.imageViewerArray count];
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    if (index < [self.imageViewerArray count])
        return [self.imageViewerArray objectAtIndex:index];
    
    return nil;
}

#pragma mark - Private

- (void)downloadData
{
    [self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi report:self.reportNo getDetails:_sameSite :GET_DETAILS_CALL_ID];
}

- (void)closeReportTapped:(UIButton *)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Are you sure to close report #%@?", self.reportNo] message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    alert.tag = CLOSE_REPORT_ALERT_TAG;
    [alert show];
}

- (void)closeReport
{
    [self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi report:self.reportNo close:CLOSE_REPORT_CALL_ID];
}

- (void)updateReport
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:NSUSERDEFAULT_KEY_UPDATING_REPORT];
    
    NSDictionary *report = [_dataDict objectForKey:@"report"];
    
    NSString *criticality = [report objectForKey:@"critical_level"];
    NSString *location = [report objectForKey:@"area_name"];
    NSString *owner = [report objectForKey:@"assigned_uacc_id"];
    
    [defaults setObject:criticality forKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
    [defaults setObject:location forKey:NSUSERDEFAULT_KEY_AREA_LOCATION];
    [defaults setObject:owner forKey:NSUSERDEFAULT_KEY_AREA_OWNER];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    InfoViewController *reportVC = [storyboard instantiateViewControllerWithIdentifier:@"InfoViewController"];
    reportVC.reportNo = self.reportNo;
    [self.navigationController pushViewController:reportVC animated:YES];
}

- (void)processData:(NSDictionary *)details
{
    NSDictionary *report = [details objectForKey:@"report"];
    
    self.reportDetails = [NSMutableArray new];
    _updateArray = [NSMutableArray new];
    
    NSString *timestampString = [report valueForKey:@"reported_timestamp"];
     NSLog(@"timestampString: %@", timestampString);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *reportDate = [dateFormatter dateFromString:timestampString];
    [dateFormatter setDateFormat:@"dd/MM/yyyy, hh:mm aa"];
    NSString *submitDateString = [dateFormatter stringFromDate:reportDate];
    NSLog(@"submitDateString: %@", submitDateString);
    
    NSInteger statusInt = [[report valueForKey:@"status"] integerValue];
    NSString *statusString = @"";
    
    switch (statusInt) {
        case 1:
            statusString = NSLocalizedString(@"new", nil);
            break;
        case 2:
            statusString = NSLocalizedString(@"overdue", nil);
            break;
        case 3:
            statusString = NSLocalizedString(@"pending", nil);
            break;
        case 4:
            statusString = NSLocalizedString(@"reopen", nil);
            break;
        case 5:
            statusString = NSLocalizedString(@"reopen_overdue", nil);
            break;
        case 6:
            statusString = NSLocalizedString(@"closed", nil);
            break;
        case 7:
            statusString = NSLocalizedString(@"reopen_closed", nil);
            break;
        default:
            break;
    }
    
    reviewUIModel *name = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"submitter_id", nil) content:[report valueForKey:@"reporter_uacc_id"] cellType:SubmitCellTypeText];
    reviewUIModel *dateTime = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"date_time", nil) content:submitDateString cellType:SubmitCellTypeText];
    reviewUIModel *hazard = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"hazard_type", nil) content:[report valueForKey:@"hazard_name"] cellType:SubmitCellTypeText];
    reviewUIModel *area = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"area_location", nil) content:[report valueForKey:@"area_name"] cellType:SubmitCellTypeText];
    reviewUIModel *owner = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"area_owner_id", nil) content:[report valueForKey:@"assigned_uacc_id"] cellType:SubmitCellTypeText];
    reviewUIModel *critically = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"criticallity_type", nil) content:[report valueForKey:@"critical_level"] cellType:SubmitCellTypeText];
    reviewUIModel *remark = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"description", nil) content:[report valueForKey:@"summary"] cellType:SubmitCellTypeText];
    //reviewUIModel *status = [reviewUIModel reviewUIModelWithTitle:@"Status" content:statusString cellType:SubmitCellTypeText];
    
    NSMutableArray *imageArray = [NSMutableArray new];
    NSString *picUrl = [report valueForKey:@"uploaded_pic_thumbnail_url"];
    if (picUrl.length)
        [imageArray addObject:[report valueForKey:@"uploaded_pic_thumbnail_url"]];
    NSString *thumbUrl = [report valueForKey:@"vid_thumbnail"];
    if (thumbUrl.length)
        [imageArray addObject:[report valueForKey:@"vid_thumbnail"]];
    reviewUIModel *media = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"picture_video_uploaded", nil) media:imageArray cellType:SubmitCellTypeMedia];
    
    NSString *imgUrl = [report valueForKey:@"uploaded_pic_url"];
    NSString *vidUrl = [report valueForKey:@"uploaded_vid_url"];
    NSMutableArray *mediaArray = [NSMutableArray new];
    if (imgUrl.length)
        [mediaArray addObject:imgUrl];
    if (vidUrl.length)
        [mediaArray addObject:vidUrl];
    media.fileUrl = [mediaArray count]? mediaArray:nil;
    
    [self.reportDetails addObjectsFromArray:@[name, dateTime, hazard, area, owner, critically, remark, media]];
    
    if ([[details objectForKey:@"update"] isKindOfClass:[NSArray class]])
    {
        NSArray *updates = [details objectForKey:@"update"];
        for (NSInteger i = 0; i < [updates count]; i++)
        {
            NSDictionary *update = [updates objectAtIndex:i];
            
            NSString *timestampString = [update valueForKey:@"added_timestamp"];
            NSLog(@"timestampString: %@", timestampString);
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *reportDate = [dateFormatter dateFromString:timestampString];
            [dateFormatter setDateFormat:@"dd/MM/yyyy, hh:mm aa"];
            NSString *submitDateString = [dateFormatter stringFromDate:reportDate];
            NSLog(@"submitDateString: %@", submitDateString);
            
            //reviewUIModel *dateTime = [reviewUIModel reviewUIModelWithTitle:@"Added on" content:submitDateString cellType:SubmitCellTypeText];
            reviewUIModel *remark = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"description", nil) content:[update valueForKey:@"remark"] cellType:SubmitCellTypeText];
            reviewUIModel *status = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"status", nil) content:statusString cellType:SubmitCellTypeText];
            
            NSMutableArray *imageArray = [NSMutableArray new];
            NSString *picUrl = [update valueForKey:@"uploaded_pic_thumbnail_url"];
            if (picUrl.length)
                [imageArray addObject:[update valueForKey:@"uploaded_pic_thumbnail_url"]];
            NSString *thumbUrl = [update valueForKey:@"vid_thumbnail"];
            if (thumbUrl.length)
                [imageArray addObject:[update valueForKey:@"vid_thumbnail"]];
            reviewUIModel *media = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"picture_video_uploaded", nil) media:imageArray cellType:SubmitCellTypeMedia];
            
            NSString *imgUrl = [update valueForKey:@"uploaded_pic_url"];
            NSString *vidUrl = [update valueForKey:@"uploaded_vid_url"];
            NSMutableArray *mediaArray = [NSMutableArray new];
            if (imgUrl.length)
                [mediaArray addObject:imgUrl];
            if (vidUrl.length)
                [mediaArray addObject:vidUrl];
            media.fileUrl = [mediaArray count]? mediaArray:nil;
            
            NSMutableArray *array = [NSMutableArray new];
            //[array addObjectsFromArray:@[dateTime, remark, media]];
            [array addObjectsFromArray:@[remark, status, media]];
            [_updateArray addObject:array];
        }
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    //BOOL isUser = [[roles objectForKey:ROLES_USER_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    //BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    //BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    NSString *ownerId = [NSString stringWithFormat:@"%@", [report valueForKey:@"assigned_uacc_id"]];
    
    if (isAdmin)
        self.updateButton.hidden = ![userId isEqualToString:ownerId];
    self.tableViewBottomConstraint.constant = self.updateButton.hidden? 13.0:61.0;
}

- (void) createData
{
    self.reportDetails = [NSMutableArray new];
    
    NSString *todayDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy, HH:mm aa"];
    todayDate = [formatter stringFromDate:[NSDate date]];
    
    reviewUIModel *name = [reviewUIModel reviewUIModelWithTitle:@"Submitter ID" content:@"123322" cellType:SubmitCellTypeText];
    reviewUIModel *dateTime = [reviewUIModel reviewUIModelWithTitle:@"Date and Time" content:todayDate cellType:SubmitCellTypeText];
    reviewUIModel *hazard = [reviewUIModel reviewUIModelWithTitle:@"Type of Hazard" content:@"Ergonomic" cellType:SubmitCellTypeText];
    reviewUIModel *area = [reviewUIModel reviewUIModelWithTitle:@"Area / Location" content:@"Production - BD4" cellType:SubmitCellTypeText];
    reviewUIModel *owner = [reviewUIModel reviewUIModelWithTitle:@"Area Owner" content:@"Armitraj" cellType:SubmitCellTypeText];
    reviewUIModel *critically = [reviewUIModel reviewUIModelWithTitle:@"Type of Criticallity" content:@"Major" cellType:SubmitCellTypeText];
    reviewUIModel *remark = [reviewUIModel reviewUIModelWithTitle:@"Description" content:@"" cellType:SubmitCellTypeText];
    
    NSArray *imageArray = [NSArray arrayWithObject:@"http://static7.depositphotos.com/1003722/678/v/950/depositphotos_6785816-Two-wokers-on-building-area.jpg"];
    reviewUIModel *media = [reviewUIModel reviewUIModelWithTitle:@"Picture / Video Uploaded" media:imageArray cellType:SubmitCellTypeMedia];
    
    [self.reportDetails addObjectsFromArray:@[name, dateTime, hazard, area, owner, critically, remark, media]];
    [self.reportDetailTableView reloadData];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == CLOSE_REPORT_ALERT_TAG)
    {
        if (buttonIndex == 1)
        {
            [self closeReport];
        }
    }
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_DETAILS_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSDictionary *details = (NSDictionary *)[AppUtil removeNullElementFromObject:result.result];
            _dataDict = details;
            [self processData:details];
            [self.reportDetailTableView reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
    else if ([callId isEqualToString:CLOSE_REPORT_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            [self.navigationController popViewControllerAnimated:YES];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
