//
//  ReportDetailsTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "ReportDetailsTableViewCell.h"

@interface ReportDetailsTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *reportTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportContentLabel;

@end

@implementation ReportDetailsTableViewCell

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.reportTitleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.reportTitleLabel.frame);
    self.reportContentLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.reportContentLabel.frame);
    
    self.reportTitleLabel.backgroundColor = [UIColor clearColor];
    self.reportContentLabel.backgroundColor = [UIColor clearColor];
}

- (void) updateDisplay:(reviewUIModel *)model
{
    self.reportTitleLabel.text = [NSString stringWithFormat:@"%@", model.title];
    
    self.reportContentLabel.text = [NSString stringWithFormat:@"%@", model.content];
    if ([model.content length] == 0)
    {
        self.reportContentLabel.text = @"-";
    }
    
    [self.reportTitleLabel sizeToFit];
}

@end
