//
//  ReportDetailsViewController.h
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BaseTableViewController.h"
#import "ReportDetailsModel.h"

@interface ReportDetailsViewController : BaseTableViewController

//@property (strong, nonatomic) ReportDetailsModel *reportDetailsModel;
@property (nonatomic, retain) NSString *reportNo;
@property (nonatomic, retain) NSString *ownerId;
@property (nonatomic, readwrite) BOOL fromNotification;
@property (nonatomic, readwrite) BOOL sameSite;

@end
