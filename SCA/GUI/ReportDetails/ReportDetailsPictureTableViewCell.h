//
//  ReportHistoryPictureTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "reviewUIModel.h"

@interface ReportDetailsPictureTableViewCell : UITableViewCell

@property id delegate;
@property (nonatomic, strong) NSArray *fileUrl;

- (void) updateDisplay:(reviewUIModel *)model;

@end

@protocol ReportDetailsPictureTableViewCellDelegate <NSObject>

- (void) ReportDetailsPictureTableViewCellDidClickAtImage:(NSString *)imageUrl;

@end