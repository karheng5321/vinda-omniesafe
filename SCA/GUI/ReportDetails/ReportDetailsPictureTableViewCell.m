//
//  ReportHistoryPictureTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "ReportDetailsPictureTableViewCell.h"
#import "ReportDetailsPictureCollectionViewCell.h"

@interface ReportDetailsPictureTableViewCell()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *pictureLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *picvidCollectionView;

@property (nonatomic, strong) NSArray *medias;

@end

@implementation ReportDetailsPictureTableViewCell

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.pictureLabel.backgroundColor = [UIColor clearColor];
    self.picvidCollectionView.backgroundColor = [UIColor clearColor];
}

- (void) updateDisplay:(reviewUIModel *)model
{
    [self.picvidCollectionView registerNib:[UINib nibWithNibName:@"ReportDetailsPictureCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ReportDetailsPictureCollectionViewCell"];
    self.picvidCollectionView.dataSource = self;
    self.picvidCollectionView.delegate = self;
    
    self.pictureLabel.text = [NSString stringWithFormat:@"%@", model.title];
    self.medias = model.media;
    [self.picvidCollectionView reloadData];
}

#pragma mark - UICollectionView Data Source & Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.medias count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReportDetailsPictureCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ReportDetailsPictureCollectionViewCell" forIndexPath:indexPath];
    [cell updateDisplay:self.medias[indexPath.row]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(ReportDetailsPictureTableViewCellDidClickAtImage:)])
    {
        //[self.delegate ReportDetailsPictureTableViewCellDidClickAtImage:self.medias[indexPath.row]];
        [self.delegate ReportDetailsPictureTableViewCellDidClickAtImage:self.fileUrl[indexPath.row]];
    }
}

@end
