//
//  ReportDetailsPictureCollectionViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "ReportDetailsPictureCollectionViewCell.h"
#import "ReportDetailsModel.h"
#import "UIImageView+WebCache.h"

@interface ReportDetailsPictureCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *picvidImage;

@end

@implementation ReportDetailsPictureCollectionViewCell

- (void) updateDisplay:(NSString *)mediaName
{
    [self.picvidImage sd_setImageWithURL:[NSURL URLWithString:mediaName] placeholderImage:[UIImage imageNamed:@""]];
}

@end
