//
//  CriticallyTypeViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "CriticallyTypeViewController.h"
#import "CriticallyTypeTableViewCell.h"
#import "CriticallyTypeModel.h"
#import "AsyncApi.h"
#import "AppUtil.h"
#import "AppDelegate.h"

@interface CriticallyTypeViewController () <AsyncApiDelegate> {
    
}

@end

@interface CriticallyTypeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *criticallytypeTable;

@end

@implementation CriticallyTypeViewController

static NSString * const GET_LIST_CALL_ID = @"getlist";

NSMutableArray *criticallyTypeArray;

- (void) setupLayout
{
    if ([GeneralHelper isDeviceiPhone4])
    {
//        self.hazardImageWidthConstraint.constant = 20;
//        self.hazardImageHeightConstraint.constant = 20;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
//        self.hazardImageWidthConstraint.constant = 30;
//        self.hazardImageHeightConstraint.constant = 30;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
    //[self downloadData];
    /*criticallyTypeArray = [NSMutableArray new];
    
    CriticallyTypeModel *sample1 = [CriticallyTypeModel new];
    sample1.criticallyName = @"Medium";
    sample1.isChecked = NO;
    
    CriticallyTypeModel *sample2 = [CriticallyTypeModel new];
    sample2.criticallyName = @"High";
    sample2.isChecked = NO;
    
    [criticallyTypeArray addObjectsFromArray:@[sample1, sample2]];*/
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self downloadData];
}

- (void)processData:(NSMutableArray *)array
{
    criticallyTypeArray = [NSMutableArray new];
    
    for (NSInteger i = 0; i < [array count]; i++)
    {
        NSDictionary *dict = [array objectAtIndex:i];
        
        CriticallyTypeModel *level = [CriticallyTypeModel new];
        level.criticallyName = [dict objectForKey:@"name"];
        level.isChecked = NO;
        level.criticalityId = [[dict objectForKey:@"id"] integerValue];
        
        [criticallyTypeArray addObject:level];
    }
}

- (void)downloadData
{
    criticallyTypeArray = nil;
    [self.criticallytypeTable reloadData];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDel isInternetAvailable])
    {
        NSMutableArray *dataArray = [[defaults objectForKey:NSUserDefaultKey_CRITICALITY_LEVEL] mutableCopy];
        if (!dataArray)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"no_internet", nil) message:NSLocalizedString(@"retry", nil) delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        [self processData:dataArray];
    }
    else
    {
        //[self setLoadingHidden:NO];
        
        AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
        [asyncApi getCriticalityList:GET_LIST_CALL_ID];
    }
}

/*- (void)downloadData
{
    criticallyTypeArray = nil;
    [self.criticallytypeTable reloadData];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi getCriticalityList:GET_LIST_CALL_ID];
}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Some calculation to determine height of row/cell.
    return 45;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return criticallyTypeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CriticallyTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CriticallyTypeCell"];
    
    [cell updateDisplay:criticallyTypeArray[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setGreyWhiteCellColor:cell row:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (CriticallyTypeModel *model in criticallyTypeArray)
    {
        model.isChecked = NO;
    }
    
    CriticallyTypeModel *model = (CriticallyTypeModel *)criticallyTypeArray[indexPath.row];
    model.isChecked = !model.isChecked;
    
    [ReportDetailsModel saveValue: model.criticallyName key:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
    [ReportDetailsModel saveValue: [NSNumber numberWithInteger: model.criticalityId] key:NSUSERDEFAULT_KEY_CRITICALLY_ID];
    [tableView reloadData];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_LIST_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        //[self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableArray *array = (NSMutableArray *)result.result;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:array forKey:NSUserDefaultKey_CRITICALITY_LEVEL];
            
            [self processData:array];
            [self.criticallytypeTable reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
