//
//  CriticallyTypeTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CriticallyTypeModel.h"

@interface CriticallyTypeTableViewCell : UITableViewCell

- (void) updateDisplay:(CriticallyTypeModel *)model;

@end
