//
//  CriticallyTypeTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "CriticallyTypeTableViewCell.h"
#import "GeneralHelper.h"

@interface CriticallyTypeTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *booleanImage;
@property (weak, nonatomic) IBOutlet UILabel *criticallytypeLabel;

@end

@implementation CriticallyTypeTableViewCell

- (void) updateDisplay:(CriticallyTypeModel *)model
{
    [self.criticallytypeLabel setText: model.criticallyName];
    
    if (model.isChecked)
    {
        [self.booleanImage setImage: [UIImage imageNamed:@"radio_black"]];
    }
    else
    {
        [self.booleanImage setImage: [UIImage imageNamed:@"radio_empty"]];
    }
    
    
}


@end
