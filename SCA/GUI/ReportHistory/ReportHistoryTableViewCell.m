//
//  ReportHistoryTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "ReportHistoryTableViewCell.h"
#import "GeneralHelper.h"

@interface ReportHistoryTableViewCell()

@end

@implementation ReportHistoryTableViewCell

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.dateLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.dateLabel.frame);
    self.typeofhazardLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.typeofhazardLabel.frame);
    self.typeofcriticallyLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.typeofcriticallyLabel.frame);
    
    self.dateLabel.backgroundColor = [UIColor clearColor];
    self.typeofhazardLabel.backgroundColor = [UIColor clearColor];
    self.typeofcriticallyLabel.backgroundColor = [UIColor clearColor];
}

- (void) updateDisplay:(ReportDetailsModel *)model
{
    [self.dateLabel setText: model.dateTime];
    [self.typeofhazardLabel setText: model.hazardType];
    [self.typeofcriticallyLabel setText: model.criticallyType];
    [self.rightarrowImage setImage: [UIImage imageNamed:@"right_arrow"]];
    
    self.dateLabel.font = [UIFont systemFontOfSize:13.0];
    self.typeofhazardLabel.font = [UIFont systemFontOfSize:13.0];
    self.typeofcriticallyLabel.font = [UIFont systemFontOfSize:13.0];
}

@end
