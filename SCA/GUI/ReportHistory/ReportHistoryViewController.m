//
//  ReportHistoryViewController.m
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "ReportHistoryViewController.h"
#import "ReportHistoryTableViewCell.h"
#import "ReportDetailsViewController.h"
#import "ReportDetailsModel.h"
#import "AsyncApi.h"
#import "AppUtil.h"

@interface ReportHistoryViewController () <AsyncApiDelegate> {
    UIView *_loadingView;
}

@property (nonatomic, strong) NSMutableArray *reportArray;
@property (strong, nonatomic) NSMutableDictionary *offscreenCells;
@property (weak, nonatomic) IBOutlet UITableView *reporthistoryTable;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (nonatomic) NSInteger selectedIndex;

@end

@implementation ReportHistoryViewController

static NSString * const GET_REPORT_CALL_ID = @"getreport";

- (void) setupLayout
{
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.topBarHeightConstraint.constant = 120;
    }
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    _loadingView.center = self.view.center;//CGPointMake(_screenWidth/2.0, _screenHeight/2.0);
    _loadingView.layer.cornerRadius = 10.0;
    _loadingView.hidden = YES;
    [self.view addSubview:_loadingView];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
    [_loadingView addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupLayout];
    
    //[self createData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self downloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

#pragma mark - UIButtons Actions
- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = NSStringFromClass([ReportHistoryTableViewCell class]);
    
    ReportHistoryTableViewCell *cell = [self.offscreenCells objectForKey:reuseIdentifier];
    if(cell == nil)
    {
        cell = (ReportHistoryTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    [cell updateDisplay:self.reportArray[indexPath.row]];
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGFloat height = 0.0;
    height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    height += 1;
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.reportArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReportHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ReportHistoryTableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    [cell updateDisplay:self.reportArray[indexPath.row]];
    [self.offscreenCells setObject:cell forKey:@"ReportHistoryTableViewCell"];
    
    cell.rightarrowImage.hidden = YES;
    
    UIView *view = [cell.contentView viewWithTag:999];
    if (view)
        [view removeFromSuperview];
    
    ReportDetailsModel *model = self.reportArray[indexPath.row];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
    label.backgroundColor = ([model.status isEqualToString:NSLocalizedString(@"new", nil)])? [UIColor blueColor]: ([model.status rangeOfString:NSLocalizedString(@"overdue", nil)].length)? [UIColor redColor]:[UIColor orangeColor];
    label.textColor = [UIColor whiteColor];
    label.text = ([model.status isEqualToString:NSLocalizedString(@"new", nil)])? [model.status uppercaseString]:model.status;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont italicSystemFontOfSize:12.0];
    [label sizeToFit];
    label.frame = CGRectMake(tableView.frame.size.width-5.0-label.frame.size.width, 5.0, label.frame.size.width, label.frame.size.height);
    label.tag = 999;
    label.font = [UIFont italicSystemFontOfSize:10.0];
    [cell.contentView addSubview:label];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setGreyWhiteCellColor:cell row:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReportDetailsModel *report = self.reportArray[indexPath.row];
    //NSLog(@"report: %@", report.dateTime);
    self.selectedIndex = indexPath.row;
    [self performSegueWithIdentifier:@"gotoReportDetailsView" sender:nil];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue");
    if ([segue.identifier isEqualToString:@"gotoReportDetailsView"])
    {
        ReportDetailsViewController *destinationVC = segue.destinationViewController;
        ReportDetailsModel *report = [self.reportArray objectAtIndex:self.selectedIndex];
        destinationVC.reportNo = report.reportNo;
        destinationVC.ownerId = report.areaOwner;
        destinationVC.fromNotification = NO;
        destinationVC.sameSite = NO;
    }
}

#pragma mark - Private

- (void)downloadData
{
    [self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi getUserReportHistory:GET_REPORT_CALL_ID];
}

- (void)processData:(NSMutableArray *)array
{
    self.reportArray = [NSMutableArray new];
    
    for (NSInteger i = 0; i < [array count]; i++) {
        NSDictionary *dict = [array objectAtIndex:i];
        
        NSString *timestampString = [dict valueForKey:@"reported_timestamp"];
        //NSArray *timestampArray = [timestampString componentsSeparatedByString:@" "];
        //NSString *dateString = [timestampArray objectAtIndex:0];
        //NSLog(@"dateString: <%@>", dateString);
        //NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd"];
        //NSDate *reportDate = [dateFormatter dateFromString:dateString];
        //NSLog(@"reportDate: %@", reportDate);
        //[dateFormatter setDateFormat:@"dd/MM/yyyy"];
        //NSString *submitDateString = [dateFormatter stringFromDate:reportDate];
        //NSLog(@"submitDateString: %@", submitDateString);
        
        ReportDetailsModel *report = [ReportDetailsModel new];
        report.reportNo = [dict valueForKey:@"id"];
        report.submitterName = [dict valueForKey:@"reporter_name"];
        report.dateTime = timestampString;
        report.hazardType = [dict valueForKey:@"hazard_name"];
        report.areaLocation = [dict valueForKey:@"area_name"];
        report.areaOwner = [dict valueForKey:@"assigned_uacc_id"];
        report.criticallyType = [dict valueForKey:@"critical_level"];
        report.remark = [dict valueForKey:@"summary"];
        
        NSInteger status = [[dict valueForKey:@"status"] integerValue];
        
        switch (status) {
            case 1:
                report.status = NSLocalizedString(@"new", nil);
                break;
            case 2:
                report.status = NSLocalizedString(@"overdue", nil);
                break;
            case 3:
                report.status = NSLocalizedString(@"pending", nil);
                break;
            case 4:
                report.status = NSLocalizedString(@"reopen", nil);
                break;
            case 5:
                report.status = NSLocalizedString(@"reopen_overdue", nil);
                break;
            case 6:
                report.status = NSLocalizedString(@"closed", nil);
                break;
            case 7:
                report.status = NSLocalizedString(@"reopen_closed", nil);
                break;
            default:
                break;
        }
        
        NSString *picUrl = [dict valueForKey:@"uploaded_pic_thumbnail_url"];
        NSString *videoUrl = [dict valueForKey:@"uploaded_vid_url"];
        
        report.uploadPicVidArray = [NSMutableArray new];
        if (picUrl.length)
            [report.uploadPicVidArray addObject:picUrl];
        if (videoUrl.length)
            [report.uploadPicVidArray addObject:videoUrl];
        
        [self.reportArray addObject:report];
    }
}

- (void) createData
{
    self.reportArray = [NSMutableArray new];
    
    ReportDetailsModel *sample1 = [ReportDetailsModel new];
    sample1.submitterName = @"Peter";
    sample1.dateTime = @"19/1/2016";
    sample1.hazardType = @"Fire / Explosion Siti Test";
    sample1.areaLocation = @"KL";
    sample1.areaOwner = @"MBPJ";
    sample1.criticallyType = @"Major";
    sample1.remark = @"Dirty air";
    sample1.uploadPicVidArray = [NSMutableArray new];
    [sample1.uploadPicVidArray addObject:@"http://static7.depositphotos.com/1003722/678/v/950/depositphotos_6785816-Two-wokers-on-building-area.jpg"];
    
    ReportDetailsModel *sample2 = [ReportDetailsModel new];
    sample2.submitterName = @"Peter";
    sample2.dateTime = @"19/1/2016";
    sample2.hazardType = @"Electrical";
    sample2.areaLocation = @"KL";
    sample2.areaOwner = @"MBPJ";
    sample2.criticallyType = @"Minor";
    sample2.remark = @"Dirty air";
    sample2.uploadPicVidArray = [NSMutableArray new];
    [sample2.uploadPicVidArray addObject:@"http://static7.depositphotos.com/1003722/678/v/950/depositphotos_6785816-Two-wokers-on-building-area.jpg"];
    
    ReportDetailsModel *sample3 = [ReportDetailsModel new];
    sample3.submitterName = @"Peter";
    sample3.dateTime = @"19/1/2016";
    sample3.hazardType = @"Ergonomic";
    sample3.areaLocation = @"KL";
    sample3.areaOwner = @"MBPJ";
    sample3.criticallyType = @"Medium";
    sample3.remark = @"Dirty air";
    sample3.uploadPicVidArray = [NSMutableArray new];
    [sample3.uploadPicVidArray addObject:@"http://static7.depositphotos.com/1003722/678/v/950/depositphotos_6785816-Two-wokers-on-building-area.jpg"];
    
    [self.reportArray addObjectsFromArray:@[sample1, sample2, sample3, sample1, sample2]];
    [self.reporthistoryTable reloadData];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_REPORT_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableArray *array = (NSMutableArray *)result.result;
            [self processData:array];
            [self.reporthistoryTable reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
