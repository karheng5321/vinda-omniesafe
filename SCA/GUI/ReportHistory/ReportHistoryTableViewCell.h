//
//  ReportHistoryTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportDetailsModel.h"

@interface ReportHistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeofhazardLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeofcriticallyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightarrowImage;

@property (nonatomic, retain) UIButton *deleteBtn;

- (void) updateDisplay:(ReportDetailsModel *)model;

@end
