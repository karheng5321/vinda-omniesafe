//
//  InfoViewController.h
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BaseTableViewController.h"
#import "ReportDetailsModel.h"

@interface InfoViewController : BaseTableViewController

@property (weak, nonatomic) IBOutlet UIButton *skipButton;

@property (nonatomic, retain) NSString *reportNo;

@end
