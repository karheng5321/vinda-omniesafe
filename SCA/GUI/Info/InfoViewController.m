//
//  InfoViewController.m
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "InfoViewController.h"
#import "ContainerViewController.h"
#import "ReportDetailsModel.h"
#import "SubmitViewController.h"

@interface InfoViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *photovideoLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeofhazardLabel;
@property (weak, nonatomic) IBOutlet UILabel *arealocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeofCritically;
@property (nonatomic, weak) ContainerViewController *containerViewController;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIButton *remarkButton;
@property (weak, nonatomic) IBOutlet UIButton *hazardButton;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *criticallityButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *photoVideoBulletView;
@property (weak, nonatomic) IBOutlet UIView *remarkBulletView;
@property (weak, nonatomic) IBOutlet UIView *hazardTypeBulletView;
@property (weak, nonatomic) IBOutlet UIView *areaLocationBulletView;
@property (weak, nonatomic) IBOutlet UIView *criticallyTypeBulletView;

@property (weak, nonatomic) IBOutlet UIView *photoToRemarkLineView;
@property (weak, nonatomic) IBOutlet UIView *remarkToHazardLineView;
@property (weak, nonatomic) IBOutlet UIView *hazardToAreaLineView;
@property (weak, nonatomic) IBOutlet UIView *areaToCriticallyLineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remarkHazardConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remarkPhotoConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationCriticallyConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationHazardConstraint;

@end

@implementation InfoViewController

- (void) setupLayout
{
    CGFloat gapSpace = ([[UIScreen mainScreen] bounds].size.width-(5*20)-(2*20))/4;
    self.remarkPhotoConstraint.constant = gapSpace;
    self.remarkHazardConstraint.constant = gapSpace;
    self.locationCriticallyConstraint.constant = gapSpace;
    self.locationHazardConstraint.constant = gapSpace;
    
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
        self.bottomBarHeightConstraint.constant = 45;
        [self.nextButton setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-50, 35)];
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        self.bottomBarHeightConstraint.constant = 55;
        [self.nextButton setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-50, 40)];
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.topBarHeightConstraint.constant = 120;
    }
    
    //Setting bullet layout
    
    self.photoVideoBulletView.layer.borderWidth = 0.5f;
    self.photoVideoBulletView.layer.borderColor = [UIColor blackColor].CGColor;
    self.photoVideoBulletView.layer.cornerRadius = self.photoVideoBulletView.frame.size.width/2;
    self.photoVideoBulletView.layer.masksToBounds = YES;
    
    self.remarkBulletView.layer.borderWidth = 0.5f;
    self.remarkBulletView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.remarkBulletView.layer.cornerRadius = self.remarkBulletView.frame.size.width/2;
    self.remarkBulletView.layer.masksToBounds = YES;
    
    self.hazardTypeBulletView.layer.borderWidth = 0.5f;
    self.hazardTypeBulletView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.hazardTypeBulletView.layer.cornerRadius = self.hazardTypeBulletView.frame.size.width/2;
    self.hazardTypeBulletView.layer.masksToBounds = YES;
    
    self.areaLocationBulletView.layer.borderWidth = 0.5f;
    self.areaLocationBulletView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.areaLocationBulletView.layer.cornerRadius = self.areaLocationBulletView.frame.size.width/2;
    self.areaLocationBulletView.layer.masksToBounds = YES;
    
    self.criticallyTypeBulletView.layer.borderWidth = 0.5f;
    self.criticallyTypeBulletView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.criticallyTypeBulletView.layer.cornerRadius = self.criticallyTypeBulletView.frame.size.width/2;
    self.criticallyTypeBulletView.layer.masksToBounds = YES;
    
    self.photoVideoBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.remarkBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.hazardTypeBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.areaLocationBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.criticallyTypeBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    
    self.photoToRemarkLineView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.photoToRemarkLineView.layer.borderWidth = 0.5f;
    
    self.remarkToHazardLineView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.remarkToHazardLineView.layer.borderWidth = 0.5f;
    
    self.hazardToAreaLineView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.hazardToAreaLineView.layer.borderWidth = 0.5f;
    
    self.areaToCriticallyLineView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.areaToCriticallyLineView.layer.borderWidth = 0.5f;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
    
    if (isUpdate)
        [self.typeofhazardLabel setText: NSLocalizedString(@"description", nil)];
    else
        [self.typeofhazardLabel setText: NSLocalizedString(@"hazard_type", nil)];
    
    if (isUpdate)
        [self.typeofCritically setText: NSLocalizedString(@"hazard_control", nil)];
    else
        [self.typeofCritically setText: NSLocalizedString(@"criticallity_type", nil)];
    
    self.remarkButton.hidden = isUpdate;
    self.remarkBulletView.hidden = isUpdate;
    self.remarkLabel.hidden = isUpdate;
    
    self.locationButton.hidden = isUpdate;
    self.areaLocationBulletView.hidden = isUpdate;
    self.arealocationLabel.hidden = isUpdate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self roundButton: self.nextButton];
    [self setupLayout];
    //[ReportDetailsModel clearAllReportDetailKey];
    
    //[ReportDetailsModel saveValue:@"KY" key:NSUSERDEFAULT_KEY_SUBMITTER_NAME];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (IBAction)skipPressed:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"sure_skip_photo", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"no", nil) otherButtonTitles:NSLocalizedString(@"yes", nil), nil];
    alert.tag = 999;
    [alert show];
}

- (IBAction)backPressed:(id)sender
{
    [ReportDetailsModel clearAllReportDetailKey];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)photoPressed:(id)sender
{
    [self.containerViewController gotoPhotoVideo];
    [self.titleLabel setText: NSLocalizedString(@"photo_video_caps", nil)];
    self.skipButton.hidden = NO;
}

- (IBAction)remarkPressed:(id)sender
{
    [self.containerViewController gotoRemark];
    [self.titleLabel setText: NSLocalizedString(@"description_caps", nil)];
    self.skipButton.hidden = YES;
}

- (IBAction)hazardPressed:(id)sender
{
    [self.containerViewController gotoHazardType];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
    
    if (isUpdate)
        [self.titleLabel setText: NSLocalizedString(@"hazard_control_caps", nil)];
    else
        [self.titleLabel setText: NSLocalizedString(@"hazard_type_caps", nil)];
    self.skipButton.hidden = YES;
}

- (IBAction)arealocationPressed:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
    if (isUpdate)
    {
        [defaults setObject:self.reportNo forKey:NSUSERDEFAULT_KEY_REPORTNO];
        [self performSegueWithIdentifier:@"gotoSubmitView" sender:nil];
        return;
    }
    
    [self.containerViewController gotoAreaLocationType];
    [self.titleLabel setText: NSLocalizedString(@"area_location_caps", nil)];
    self.skipButton.hidden = YES;
}

- (IBAction)criticallyPressed:(id)sender
{
    [self.containerViewController gotoCriticallyType];
    [self.titleLabel setText: NSLocalizedString(@"criticallity_type_caps", nil)];
    self.skipButton.hidden = YES;
}

- (IBAction)nextPressed:(id)sender
{
    ReportDetailsModel *submittingModel = [ReportDetailsModel new];
    submittingModel = [ReportDetailsModel getReportDetailInfo];
    [self checkContent];
}

#pragma mark - Private
- (BOOL) checkMediaFile
{
    if ([ReportDetailsModel getMutableArray:NSUSERDEFAULT_KEY_PICVIDFILE].count == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"media_warning", nil) message:NSLocalizedString(@"media_message", nil) delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (BOOL) checkRemark
{
    if ([ReportDetailsModel getValue:NSUSERDEFAULT_KEY_REMARK].length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"remark_warning", nil) message:NSLocalizedString(@"remark_message", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (BOOL) checkHazard
{
    if ([ReportDetailsModel getValue:NSUSERDEFAULT_KEY_HAZARDTYPE].length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"hazard_message", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (BOOL) checkLocation
{
    if ([ReportDetailsModel getValue:NSUSERDEFAULT_KEY_AREA_LOCATION].length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"area_message", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (BOOL) checkCritically
{
    if ([ReportDetailsModel getValue:NSUSERDEFAULT_KEY_CRITICALLY_TYPE].length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"criticallity_message", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (void) checkContent
{
    NSString *vcName = self.containerViewController.childViewControllers.firstObject.restorationIdentifier;
    if ([vcName isEqualToString: @"PhotoVideoViewController"])
    {
        if ([self checkMediaFile])
        {
            [self remarkPressed: nil];
            [self.remarkBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.photoToRemarkLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
            if (isUpdate)
            {
                [self.hazardTypeBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
                [self.remarkToHazardLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            }
        }
    }
    else if ([vcName isEqualToString: @"RemarkViewController"])
    {
        if ([self checkRemark])
        {
            [self hazardPressed: nil];
            [self.hazardTypeBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.remarkToHazardLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
        }
    }
    else if ([vcName isEqualToString: @"HazardTypeViewController"])
    {
        if ([self checkHazard])
        {
            [self arealocationPressed: nil];
            [self.areaLocationBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.hazardToAreaLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
            if (isUpdate)
            {
                [self.criticallyTypeBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
                [self.areaToCriticallyLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            }
        }
    }
    else if ([vcName isEqualToString: @"AreaLocationViewController"])
    {
        if ([self checkLocation])
        {
            [self criticallyPressed: nil];
            [self.criticallyTypeBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.areaToCriticallyLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
        }
    }
    else if ([vcName isEqualToString: @"CriticallyTypeViewController"])
    {
        if ([self checkCritically])
        {
            [self performSegueWithIdentifier:@"gotoSubmitView" sender:nil];
        }
        
        //[self performSegueWithIdentifier:@"gotoSubmitView" sender:nil];
    }
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if ([segue.identifier isEqualToString:@"embedContainer"])
    {
        self.containerViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:@"gotoSubmitView"])
    {
        [ReportDetailsModel saveValue:[GeneralHelper getCurrentDateTime] key:NSUSERDEFAULT_KEY_DATE_TIME];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 999)
    {
        if (buttonIndex == 1)
        {
            [ReportDetailsModel clearUploadPicArray];
            [self remarkPressed: nil];
            [self.remarkBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.photoToRemarkLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
            if (isUpdate)
            {
                [self.hazardTypeBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
                [self.remarkToHazardLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            }
        }
            
        return;
    }
    
    switch(buttonIndex)
    {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
            [self remarkPressed: nil];
            [self.remarkBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.photoToRemarkLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
            if (isUpdate)
            {
                [self.hazardTypeBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
                [self.remarkToHazardLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            }
            break;
    }
}

@end
