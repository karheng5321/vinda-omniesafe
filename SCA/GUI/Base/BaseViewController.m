//
//  BaseViewController.m
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"

#define NSUSERDEFAULT_KEY_LOGIN_ROLE @"NSUSERDEFAULT_KEY_LOGIN_ROLE"

@implementation BaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"loginBg.png"]];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Navigation

- (void) switchToDashboard: (NSString *) navcontrollerIdentifier 
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appDelegate.window duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void)
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
         
         UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:navcontrollerIdentifier];
         //[appDelegate.window addSubview:navigationController.view];
         [appDelegate.window setRootViewController:navigationController];
         [appDelegate.window makeKeyAndVisible];
         
         [UIView setAnimationsEnabled:oldState];
         
     }completion:nil];
}

#pragma mark - Button
- (void) roundButton: (UIButton*)button
{
    button.layer.cornerRadius = 13.0f;
    button.layer.masksToBounds = YES;
}

#pragma mark - TextView
- (void) setTextViewLayout: (UITextView *)textview
{
    //textview.text = @"Type your remark here...";
    //textview.textColor = [UIColor lightGrayColor];
    textview.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
    [[textview layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[textview layer] setBorderWidth:1.0f];
    [[textview layer] setCornerRadius:15];
}

#pragma mark - UIColor
- (UIColor *)colorWithHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

#pragma mark - Role

- (NSString *)getUserRole
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:NSUSERDEFAULT_KEY_LOGIN_ROLE];
}

- (void)saveUserRole: (NSString*) role
{
    [[NSUserDefaults standardUserDefaults] setObject:role forKey:NSUSERDEFAULT_KEY_LOGIN_ROLE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
