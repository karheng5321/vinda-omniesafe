//
//  BaseViewController.h
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "ServerEnum.h"
#import "GeneralHelper.h"

@interface BaseViewController : UIViewController

- (void) switchToDashboard: (NSString *) navcontrollerIdentifier;
- (void) roundButton: (UIButton*)button;
- (UIColor *)colorWithHexString:(NSString *)hexString;
- (void) setTextViewLayout: (UITextView *)textview;
- (NSString *)getUserRole;
- (void)saveUserRole: (NSString*) role;
@end
