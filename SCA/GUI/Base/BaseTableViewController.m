//
//  BaseTableViewController.m
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)setGreyWhiteCellColor: (UITableViewCell *)eachCell row:(NSInteger)row
{
    UIColor *cellColor;
    if (row % 2 == 0)
    {
        cellColor = [UIColor colorWithRed:217.0f/255.0f
                                    green:219.0f/255.0f
                                     blue:219.0f/255.0f
                                    alpha:0.7f];
    }
    else
    {
        cellColor = [UIColor clearColor];
    }
    
    [eachCell setBackgroundColor: cellColor];
}
@end
