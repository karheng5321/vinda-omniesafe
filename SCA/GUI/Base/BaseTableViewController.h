//
//  BaseTableViewController.h
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BaseViewController.h"
#import "ReportDetailsModel.h"

@interface BaseTableViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

- (void)setGreyWhiteCellColor: (UITableViewCell *)eachCell row: (NSInteger) row;

@end
