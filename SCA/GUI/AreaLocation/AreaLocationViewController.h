//
//  AreaLocationViewController.h
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "InfoViewController.h"

@interface AreaLocationViewController : InfoViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
