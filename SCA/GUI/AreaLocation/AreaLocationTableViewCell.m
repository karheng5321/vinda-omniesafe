//
//  AreaLocationTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 26/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaLocationTableViewCell.h"
#import "ActionSheetPicker.h"
#import "ReportDetailsModel.h"

@implementation AreaLocationTableViewCell

- (void) layoutSubviews
{
    NSLog(@"layoutSubviews");
    [super layoutSubviews];
    
    self.locationTextView.layer.borderColor = [UIColor colorWithRed:19.0f/255.0f green:157.0f/255.0f blue:236.0f/255.0f alpha:0.8].CGColor;
    self.locationTextView.layer.borderWidth = 1.0f;
    self.locationTextView.layer.cornerRadius = 10.0f;
    self.locationTextView.layer.masksToBounds = YES;
    self.locationTextView.backgroundColor = [UIColor whiteColor];
    
    UIView *paddingView_left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
    UIView *paddingView_right = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 16)];
    self.locationTextView.leftView = paddingView_left;
    self.locationTextView.leftViewMode = UITextFieldViewModeAlways;
    self.locationTextView.rightView = paddingView_right;
    self.locationTextView.rightViewMode = UITextFieldViewModeAlways;
}

- (IBAction)locationTapped:(id)sender
{
    NSLog(@"category id: %@", [[NSUserDefaults standardUserDefaults] valueForKey:NSUSERDEFAULT_KEY_AREA_CATEGORY_ID]);
    UIButton *btn = (UIButton *)sender;
    if (![[NSUserDefaults standardUserDefaults] valueForKey:NSUSERDEFAULT_KEY_AREA_CATEGORY_ID] && btn.tag != 1)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please select area category" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Location"
                                            rows:self.locationArray
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                           if ([self.locationArray count])
                                           {
                                               if (btn.tag == 1)
                                               {
                                                   [ReportDetailsModel saveValue: [self.catIdArray objectAtIndex:selectedIndex] key: NSUSERDEFAULT_KEY_AREA_CATEGORY_ID];
                                                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_AREA_LOCATION];
                                                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_AREA_ID];
                                                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_AREA_OWNER];
                                                   
                                                   [self.tableView reloadData];
                                               }
                                               else
                                               {
                                                   [ReportDetailsModel saveValue: selectedValue key: NSUSERDEFAULT_KEY_AREA_LOCATION];
                                                   [ReportDetailsModel saveValue: [self.areaIdArray objectAtIndex:selectedIndex] key: NSUSERDEFAULT_KEY_AREA_ID];
                                                   [ReportDetailsModel saveValue: [self.ownerArray objectAtIndex:selectedIndex] key: NSUSERDEFAULT_KEY_AREA_OWNER];
                                               }
                                               [self.locationTextView setText:selectedValue];
                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
    
    /*[ActionSheetStringPicker showPickerWithTitle:@"Select Location"
                                            rows:self.locationArray
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           [ReportDetailsModel saveValue: selectedValue key: NSUSERDEFAULT_KEY_AREA_LOCATION];
                                           [ReportDetailsModel saveValue: [self.idArray objectAtIndex:selectedIndex] key: NSUSERDEFAULT_KEY_AREA_ID];
                                           [ReportDetailsModel saveValue: [self.ownerArray objectAtIndex:selectedIndex] key: NSUSERDEFAULT_KEY_AREA_OWNER];
                                           [self.locationTextView setText:selectedValue];
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];*/
}

@end

