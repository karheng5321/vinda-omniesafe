//
//  AreaLocationViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaLocationViewController.h"
#import "AreaLocationTableViewCell.h"
#import "AsyncApi.h"
#import "AppUtil.h"
#import "AppDelegate.h"

@interface AreaLocationViewController () <AsyncApiDelegate> {
    NSMutableDictionary *_dataDict;
}

@end

@implementation AreaLocationViewController

static NSString * const GET_LIST_CALL_ID = @"getlist";

NSDictionary *dataDict;
NSMutableDictionary *_nameArray;
NSMutableDictionary *_idArray;
NSMutableDictionary *_ownerArray;

NSMutableArray *_categoryArray;
NSMutableArray *_catIdArray;

NSArray *keyArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    //locationName = @[@"Production"];
    
    // Do any additional setup after loading the view.
    
    //[self downloadData];
    
    self.tableView.bounces = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self downloadData];
}

- (void)downloadData
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDel isInternetAvailable])
    {
        NSMutableDictionary *dataArray = [[defaults objectForKey:NSUserDefaultKey_AREA_LIST] mutableCopy];
        if (!dataArray)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"no_internet", nil) message:NSLocalizedString(@"retry", nil) delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        [self processData:dataArray];
    }
    else
    {
        //[self setLoadingHidden:NO];
        
        AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
        [asyncApi getAreaList:GET_LIST_CALL_ID];
    }
}

/*- (void)downloadData
{
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi getAreaList:GET_LIST_CALL_ID];
}*/

- (void)processData:(NSMutableDictionary *)list
{
    _dataDict = list;
    
    _nameArray = [NSMutableDictionary new];
    _idArray = [NSMutableDictionary new];
    _ownerArray = [NSMutableDictionary new];
    _categoryArray = [NSMutableArray new];
    
    keyArray = [list allKeys];
    for (NSInteger i = 0; i < [keyArray count]; i++)
    {
        NSString *key = [keyArray objectAtIndex:i];
        NSDictionary *dict = [list objectForKey:key];
        NSString *catName = [dict objectForKey:@"name"];
        [_categoryArray addObject:catName];
        
        NSMutableArray *array = [dict objectForKey:@"0"];
        
        NSMutableArray *nameArray = [NSMutableArray new];
        NSMutableArray *idArray = [NSMutableArray new];
        NSMutableArray *ownerArray = [NSMutableArray new];
        for (NSInteger j = 0; j < [array count]; j++)
        {
            NSDictionary *subDict = [array objectAtIndex:j];
            [nameArray addObject:[subDict objectForKey:@"name"]];
            [idArray addObject:[NSNumber numberWithInteger:[[subDict objectForKey:@"id"] integerValue]]];
            [ownerArray addObject:[subDict objectForKey:@"owner_uacc_id"]];
        }
        
        [_nameArray setObject:nameArray forKey:key];
        [_idArray setObject:idArray forKey:key];
        [_ownerArray setObject:ownerArray forKey:key];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;//keyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AreaLocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaLocationCell"];
    //cell.locationArray = [_nameArray objectAtIndex:indexPath.row];
    //cell.idArray = [_idArray objectAtIndex:indexPath.row];
    //cell.ownerArray = [_ownerArray objectAtIndex:indexPath.row];
    //NSString *key = [keyArray objectAtIndex: indexPath.row];
    //NSDictionary *dict = [dataDict objectForKey:key];
    //[cell.locationLabel setText: [dict objectForKey:@"name"]];
    
    cell.tableView = tableView;
    cell.locationButton.tag = indexPath.row+1;
    cell.catIdArray = keyArray;
    
    switch (indexPath.row) {
        case 0:
        {
            [cell.locationLabel setText:NSLocalizedString(@"select_area_category", nil)];
            
            cell.locationArray = _categoryArray;
        }
            break;
        case 1:
        {
            [cell.locationLabel setText:NSLocalizedString(@"select_sub_category", nil)];
            
            if (![ReportDetailsModel getValue:NSUSERDEFAULT_KEY_AREA_ID])
            {
                cell.locationTextView.text = nil;
            }
            
            NSString *key = [ReportDetailsModel getValue:NSUSERDEFAULT_KEY_AREA_CATEGORY_ID];
            cell.locationArray = [_nameArray objectForKey:key];
            cell.areaIdArray = [_idArray objectForKey:key];
            cell.ownerArray = [_ownerArray objectForKey:key];
        }
            break;
        default:
            break;
    }
    
    return cell;
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_LIST_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        //[self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableDictionary *list = (NSMutableDictionary *)result.result;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:list forKey:NSUserDefaultKey_AREA_LIST];
            
            dataDict = list;
            [self processData:list];
            [self.tableView reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
