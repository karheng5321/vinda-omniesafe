//
//  AreaLocationTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 26/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AreaLocationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UITextField *locationTextView;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (retain, nonatomic) NSMutableArray *locationArray;
@property (retain, nonatomic) NSMutableArray *areaIdArray;
@property (retain, nonatomic) NSMutableArray *ownerArray;
@property (retain, nonatomic) NSArray *catIdArray;
@property (retain, nonatomic) UITableView *tableView;

@end
