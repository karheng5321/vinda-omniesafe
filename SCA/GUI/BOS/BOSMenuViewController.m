//
//  BOSMenuViewController.m
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BOSMenuViewController.h"
#import "AsyncApi.h"
#import "Constant.h"
#import "AppUtil.h"
#import "BOSObservationViewController.h"
#import "BOSReportListViewController.h"
#import "SafeViewController.h"
#import "AppDelegate.h"
#import "ReportDetailsModel.h"

@interface BOSMenuViewController () <AsyncApiDelegate, UITextFieldDelegate> {
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    float _titleHeight;
    
    UIView *_loadingView;
    
    UIView *_navView;
    UILabel *_titleLabel;
    UILabel *_usernameLabel;
    UIScrollView *_mainView;
    NSMutableArray *_dataArray;
    
    UILabel *_bosBadgeLabel;
    UIButton *_deptBtn;
    
    UIButton *_unsaveBtn;
}

@end

@implementation BOSMenuViewController

static CGFloat const  badgeMinSize = 40.0;

static NSString* const GET_PAGE_DATA_CALL_ID = @"getpagedata";

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        //self.navigationItem.title = @"";
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
    }
    return self;
}

- (void)initPage
{
    _statusBarHeight = 0.0;
    _navBarHeight = 120.0;
    _titleHeight = 95.0;
    _tabBarHeight = 0.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_statusBarHeight-_tabBarHeight;
    
    _navView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _screenWidth, _navBarHeight)];
    _navView.backgroundColor = [UIColor appBOSBlueColor];
    [self.view addSubview:_navView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(51.0, 0.0, _screenWidth-2*51.0, _navView.frame.size.height)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.text = NSLocalizedString(@"BOS menu", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:35.0];
    _titleLabel.numberOfLines = 2;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(_navView.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _navView.frame.size.height/2.0-_titleLabel.frame.size.height/2.0, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [_navView addSubview:_titleLabel];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(8.0, 42.0, 35.0, _navBarHeight-2*42);
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];
    
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 40.0, 40.0)];
    btnView.backgroundColor = [UIColor clearColor];
    btnView.opaque = NO;
    
    UIImage *iconImage = [UIImage imageNamed:@"ic_assignment_late_white.png"];
    float scale = iconImage.size.width/25.0;
    float iconWidth = iconImage.size.width/scale;
    float iconHeight = iconImage.size.height/scale;
    
    UIImageView *btnIcon = [[UIImageView alloc] initWithFrame:CGRectMake(btnView.frame.size.width/2.0-iconWidth/2.0, btnView.frame.size.height/2.0-iconHeight/2.0, iconWidth, iconHeight)];
    btnIcon.image = iconImage;
    [btnView addSubview:btnIcon];
    
    UIImage *saveImage = [AppUtil imageFromUIView:btnView];
    
    _unsaveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _unsaveBtn.frame = CGRectMake(_navView.frame.size.width-10.0-40.0, _navView.frame.size.height/2.0-40.0/2.0, 40.0, 40.0);
    _unsaveBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [_unsaveBtn setImage:saveImage forState:UIControlStateNormal];
    [_unsaveBtn addTarget:self action:@selector(safeTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:_unsaveBtn];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isBosSuper = [[roles objectForKey:ROLES_BOS_SUPERVISOR_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    
    _usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, _navView.frame.size.height-30.0, _screenWidth-2*10.0, 30.0)];
    _usernameLabel.backgroundColor = [UIColor clearColor];
    _usernameLabel.textColor = [UIColor whiteColor];
    _usernameLabel.text = [user objectForKey:@"ism_user_fullname"];
    _usernameLabel.textAlignment = NSTextAlignmentCenter;
    _usernameLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    _usernameLabel.numberOfLines = 1;
    _usernameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_navView addSubview:_usernameLabel];
    
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _navBarHeight+_statusBarHeight, _screenWidth, _screenHeight)];
    _mainView.backgroundColor = [UIColor clearColor];
    _mainView.showsHorizontalScrollIndicator = NO;
    _mainView.bounces = NO;
    _mainView.contentSize = _mainView.frame.size;
    [self.view addSubview:_mainView];
    
    CGFloat sideGap = 10.0;
    CGFloat btnWidth = (_mainView.frame.size.width-4*sideGap)/2.0;
    CGFloat currentHeight = (isBosSuper && !appDel.ON_DIFFERENT_SITE)? _mainView.frame.size.height/2.0-btnWidth:_mainView.frame.size.height/2.0-10.0-btnWidth;
    
    UIButton *reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    reportBtn.frame = CGRectMake(_mainView.frame.size.width/2.0-btnWidth/2.0, currentHeight, btnWidth, btnWidth);
    reportBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [reportBtn addTarget:self action:@selector(reportTapped:) forControlEvents:UIControlEventTouchUpInside];
    reportBtn.backgroundColor = [UIColor appBOSBlueColor];
    reportBtn.layer.cornerRadius = btnWidth/2.0;
    [_mainView addSubview:reportBtn];
    
    CGFloat iconSize = btnWidth/2.0;
    UIImageView *reportIcon = [[UIImageView alloc] initWithFrame:CGRectMake(reportBtn.frame.size.width/2.0-iconSize/2.0, btnWidth*0.05, iconSize, iconSize)];
    reportIcon.image = [UIImage imageNamed:@"icon_report_bos.png"];
    reportIcon.contentMode = UIViewContentModeScaleAspectFit;
    [reportBtn addSubview:reportIcon];
    
    CGFloat initY = reportIcon.frame.origin.y+reportIcon.frame.size.height;
    UILabel *reportLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnWidth*0.1, initY, reportBtn.frame.size.width-2.0*btnWidth*0.1, reportBtn.frame.size.height-initY-btnWidth*0.1)];
    reportLabel.backgroundColor = [UIColor clearColor];
    reportLabel.textColor = [UIColor whiteColor];
    reportLabel.text = NSLocalizedString(@"BOS report title", nil);
    reportLabel.textAlignment = NSTextAlignmentCenter;
    reportLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
    reportLabel.numberOfLines = 2;
    reportLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [reportBtn addSubview:reportLabel];
    
    currentHeight += reportBtn.frame.size.height;
    if ((!isBosSuper && !isAdmin) || appDel.ON_DIFFERENT_SITE)
        currentHeight += 10.0;
    
    CGFloat initX = (isBosSuper || isAdmin)? (!appDel.ON_DIFFERENT_SITE)? sideGap:reportBtn.frame.origin.x:reportBtn.frame.origin.x;
    
    UIButton *historyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    historyBtn.frame = CGRectMake(initX, currentHeight, btnWidth, btnWidth);
    historyBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [historyBtn addTarget:self action:@selector(historyTapped:) forControlEvents:UIControlEventTouchUpInside];
    historyBtn.backgroundColor = [UIColor appGreenColor];
    historyBtn.layer.cornerRadius = btnWidth/2.0;
    [_mainView addSubview:historyBtn];
    
    UIImageView *historyIcon = [[UIImageView alloc] initWithFrame:CGRectMake(historyBtn.frame.size.width/2.0-iconSize/2.0, btnWidth*0.05, iconSize, iconSize)];
    historyIcon.image = [UIImage imageNamed:@"restore_icon.png"];
    historyIcon.contentMode = UIViewContentModeScaleAspectFit;
    [historyBtn addSubview:historyIcon];
    
    initY = historyIcon.frame.origin.y+historyIcon.frame.size.height;
    UILabel *historyLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnWidth*0.1, initY, historyBtn.frame.size.width-2.0*btnWidth*0.1, historyBtn.frame.size.height-initY-btnWidth*0.1)];
    historyLabel.backgroundColor = [UIColor clearColor];
    historyLabel.textColor = [UIColor whiteColor];
    historyLabel.text = NSLocalizedString(@"BOS history title", nil);
    historyLabel.textAlignment = NSTextAlignmentCenter;
    historyLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
    historyLabel.numberOfLines = 2;
    historyLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [historyBtn addSubview:historyLabel];
    
    if (isBosSuper || isAdmin)
    {
        if (!appDel.ON_DIFFERENT_SITE)
        {
            _deptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            _deptBtn.frame = CGRectMake(_mainView.frame.size.width/2.0+sideGap, historyBtn.frame.origin.y, btnWidth, btnWidth);
            _deptBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [_deptBtn addTarget:self action:@selector(allTapped:) forControlEvents:UIControlEventTouchUpInside];
            _deptBtn.backgroundColor = [UIColor appUnsafeBlueColor];
            _deptBtn.layer.cornerRadius = btnWidth/2.0;
            [_mainView addSubview:_deptBtn];
            
            UIImageView *deptIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_deptBtn.frame.size.width/2.0-iconSize/2.0, btnWidth*0.05, iconSize, iconSize)];
            deptIcon.image = [UIImage imageNamed:@"bos_all_history.png"];
            deptIcon.contentMode = UIViewContentModeScaleAspectFit;
            [_deptBtn addSubview:deptIcon];
            
            initY = deptIcon.frame.origin.y+deptIcon.frame.size.height;
            
            UILabel *deptLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnWidth*0.1, initY, _deptBtn.frame.size.width-2.0*btnWidth*0.1, _deptBtn.frame.size.height-initY-btnWidth*0.1)];
            deptLabel.backgroundColor = [UIColor clearColor];
            deptLabel.textColor = [UIColor whiteColor];
            deptLabel.text = isAdmin? NSLocalizedString(@"all_history", nil):NSLocalizedString(@"BOS department title", nil);
            deptLabel.textAlignment = NSTextAlignmentCenter;
            deptLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
            deptLabel.numberOfLines = 2;
            deptLabel.lineBreakMode = NSLineBreakByWordWrapping;
            [_deptBtn addSubview:deptLabel];
            
            _bosBadgeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            _bosBadgeLabel.textAlignment = NSTextAlignmentCenter;
            _bosBadgeLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
            _bosBadgeLabel.backgroundColor = [UIColor redColor];
            _bosBadgeLabel.textColor = [UIColor whiteColor];
            _bosBadgeLabel.layer.cornerRadius = badgeMinSize/2.0;
            _bosBadgeLabel.clipsToBounds = YES;
            [_mainView addSubview:_bosBadgeLabel];
        }
    }
    
    UIImage *btnImage = [UIImage imageNamed:@"logout_icon.png"];
    scale = btnImage.size.width/20.0;
    btnWidth = btnImage.size.width/scale;
    float btnHeight = btnImage.size.height/scale;
    
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoutBtn.frame = CGRectMake(_mainView.frame.size.width-10.0-btnWidth, _mainView.frame.size.height-10.0-btnHeight, btnWidth, btnHeight);
    logoutBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [logoutBtn setImage:btnImage forState:UIControlStateNormal];
    [logoutBtn addTarget:self action:@selector(logoutTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_mainView addSubview:logoutBtn];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)logoutTapped:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"logout_alert", nil) message:NSLocalizedString(@"logout_message", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self logoutFromServer];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:NSUserDefaultKey_USER];
        [ReportDetailsModel clearAllReportDetailKey];
        [self switchToDashboard:@"LoginViewController"];
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)logoutFromServer
{
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:nil];
    [asyncApi logoutFromServer:nil];
}

- (void) switchToDashboard: (NSString *) navcontrollerIdentifier
{
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [UIView transitionWithView:appDelegate.window duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void)
     {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
         
         UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:navcontrollerIdentifier];
         //[appDelegate.window addSubview:navigationController.view];
         [appDelegate.window setRootViewController:navigationController];
         [appDelegate.window makeKeyAndVisible];
         
         [UIView setAnimationsEnabled:oldState];
         
     }completion:nil];
}

- (void)refreshBadge:(NSMutableDictionary *)badge
{
    NSLog(@"BOSMenuViewController refreshBadge");
    //NSString *ucBadge = [NSString stringWithFormat:@"%@", [badge valueForKey:@"uc"]];
    NSString *bosBadge = [NSString stringWithFormat:@"%@", [badge valueForKey:@"bos"]];
    
    //_ucBadgeLabel.text = ucBadge;
    _bosBadgeLabel.text = bosBadge;
    
    CGFloat textMaxWidth = self.view.frame.size.width/4.0;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _bosBadgeLabel.font, NSFontAttributeName,
                                  nil];
    //CGRect ucRect = [ucBadge boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    CGRect bosRect = [bosBadge boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    //CGFloat ucWidth = MAX(ucRect.size.width+20.0, badgeMinSize);
    //_ucBadgeLabel.frame = CGRectMake(self.unsafeConditionButtonView.frame.origin.x+self.unsafeConditionButtonView.frame.size.width*0.75, self.unsafeConditionButtonView.frame.origin.y, ucWidth, badgeMinSize);
    
    CGFloat bosWidth = MAX(bosRect.size.width+20.0, badgeMinSize);
    _bosBadgeLabel.frame = CGRectMake(_deptBtn.frame.origin.x+_deptBtn.frame.size.width*0.75, _deptBtn.frame.origin.y, bosWidth, badgeMinSize);
    
    //_ucBadgeLabel.hidden = ([ucBadge integerValue] == 0);
    _bosBadgeLabel.hidden = ([bosBadge integerValue] == 0);
}

- (void)reportTapped:(UIButton *)sender
{
    NSMutableDictionary *_sessionDict = [[NSMutableDictionary alloc] init];
    BOSObservationViewController *observeVC = [[BOSObservationViewController alloc] initWithDictionary:_sessionDict];
    [self.navigationController pushViewController:observeVC animated:YES];
}

- (void)historyTapped:(UIButton *)sender
{
    BOSReportListViewController *listVC = [[BOSReportListViewController alloc] initWithType:@"own"];
    [self.navigationController pushViewController:listVC animated:YES];
}

- (void)allTapped:(UIButton *)sender
{
    BOSReportListViewController *listVC = [[BOSReportListViewController alloc] initWithType:@"all"];
    [self.navigationController pushViewController:listVC animated:YES];
}

- (void)safeTapped:(UIButton *)sender
{
    SafeViewController *saveVC = [[SafeViewController alloc] init];
    [self.navigationController pushViewController:saveVC animated:YES];
}

- (void)refreshPage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    
    NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_BOS] mutableCopy];
    if (!reportDict)
        reportDict = [NSMutableDictionary new];
    NSMutableArray *dataArray = [[reportDict objectForKey:userId] mutableCopy];
    if (!dataArray)
        dataArray = [NSMutableArray new];
    
    _unsaveBtn.hidden = ([dataArray count] == 0);
}

- (void)getPageData
{
    NSLog(@"bosGetBadgeNumber");
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi bosGetBadgeNumber:GET_PAGE_DATA_CALL_ID];
}

- (UIView *)findFirstResponder
{
    for (UIView *subview in _mainView.subviews) {
        if ([subview isFirstResponder])
            return subview;
    }
    
    return nil;
}

- (void)expandMainView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight);
    
    [UIView commitAnimations];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)collapseMainView:(float)keyboardHeight
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight-keyboardHeight+_tabBarHeight);
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self getPageData];
    [self refreshPage];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /*if ([self.navigationController.viewControllers count] <= 1)
    {
        UIImage *btnImage = [UIImage imageNamed:@"logout_icon.png"];
        float scale = btnImage.size.width/20.0;
        float btnWidth = btnImage.size.width/scale;
        float btnHeight = btnImage.size.height/scale;
        
        UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        logoutBtn.frame = CGRectMake(_mainView.frame.size.width-10.0-btnWidth, _mainView.frame.size.height-10.0-btnHeight, btnWidth, btnHeight);
        logoutBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [logoutBtn setImage:btnImage forState:UIControlStateNormal];
        [logoutBtn addTarget:self action:@selector(logoutTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_mainView addSubview:logoutBtn];
    }*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate

- (void)keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    //NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    NSLog(@"keyboardBounds: %f, %f, %f, %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.height, keyboardBounds.size.width);
    //[self collapseMainView:keyboardBounds.size.height];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    //[self expandMainView];
    
    //[self performSearch];
    
    return YES;
}

- (void)textFieldDidChanged:(NSNotification *)notification
{
    //_searchPlaceholder.hidden = _searchBar.text.length;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_PAGE_DATA_CALL_ID])
    {
        //[self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
            NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
            BOOL isBosSuper = [[roles objectForKey:ROLES_BOS_SUPERVISOR_KEY] boolValue];
            BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
            
            if (isBosSuper || isAdmin)
            {
                NSMutableDictionary *content = [AppUtil removeNullElementFromObject:result.result];
                [self refreshBadge:content];
            }
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
