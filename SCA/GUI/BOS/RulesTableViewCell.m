//
//  RulesTableViewCell.m
//  SCA
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import "RulesTableViewCell.h"
#import "Constant.h"
#import "AsyncImageView.h"

@interface RulesTableViewCell () {
	UILabel *_textLabel;
    AsyncImageView *_iconView;
    UIView *_bulletView;
}

@end

@implementation RulesTableViewCell

@synthesize textLabel = _textLabel;
@synthesize on = _on;

static float const MARGIN = 10.0;
static float const IMAGE_SIZE = 60.0;
static float const BULLET_SIZE = 15.0;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _on = NO;
		
        _textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textColor = [UIColor blackColor];
        _textLabel.text = @"";
        _textLabel.textAlignment = NSTextAlignmentLeft;
        _textLabel.font = [UIFont systemFontOfSize:14.0];
        _textLabel.numberOfLines = 0;
        [self.contentView addSubview:_textLabel];
        
        _iconView = [[AsyncImageView alloc] initWithFrame:CGRectMake(MARGIN+BULLET_SIZE+MARGIN, MARGIN, IMAGE_SIZE, IMAGE_SIZE) imageUrl:nil autoResize:NO downloadNew:NO];
        _iconView.image = [UIImage imageNamed:@"AppIcon.png"];
        _iconView.backgroundColor = [UIColor whiteColor];
        _iconView.contentMode = UIViewContentModeScaleAspectFill;
        _iconView.layer.cornerRadius = 10.0;
        _iconView.clipsToBounds = YES;
        [self.contentView addSubview:_iconView];
        
        _bulletView = [[UIView alloc] initWithFrame:CGRectMake(MARGIN+BULLET_SIZE+MARGIN, MARGIN, BULLET_SIZE, BULLET_SIZE)];
        _bulletView.backgroundColor = [UIColor clearColor];
        _bulletView.layer.cornerRadius = BULLET_SIZE/2.0;
        _bulletView.clipsToBounds = YES;
        _bulletView.layer.borderColor = [[UIColor blackColor] CGColor];
        _bulletView.layer.borderWidth = 1.0;
        [self.contentView addSubview:_bulletView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setOn:(BOOL)on
{
    _on = on;
    
    _bulletView.backgroundColor = _on? [UIColor appBOSBlueColor]:[UIColor clearColor];
}

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightWithString:(NSString *)string
{
    CGFloat textMaxWidth = tableView.frame.size.width-4*MARGIN-IMAGE_SIZE-BULLET_SIZE;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont systemFontOfSize:14.0], NSFontAttributeName,
                                 nil];
    CGRect textRect = [string boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    return MAX(MARGIN+textRect.size.height+MARGIN,MARGIN+IMAGE_SIZE+MARGIN);
}

- (void)tableView:(UITableView *)tableView layoutCellWithString:(NSString *)string imageUrl:(NSString *)imageUrl
{
    CGFloat rowHeight = [RulesTableViewCell tableView:tableView calculateRowHeightWithString:string];
    CGFloat textMaxWidth = tableView.frame.size.width-4*MARGIN-IMAGE_SIZE-BULLET_SIZE;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont systemFontOfSize:14.0], NSFontAttributeName,
                                  nil];
    CGRect textRect = [string boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    _textLabel.frame = CGRectMake(MARGIN+BULLET_SIZE+MARGIN+IMAGE_SIZE+MARGIN, rowHeight/2.0-textRect.size.height/2.0, textRect.size.width, textRect.size.height);
    _textLabel.text = string;
    
    _bulletView.frame = CGRectMake(MARGIN, rowHeight/2.0-BULLET_SIZE/2.0, BULLET_SIZE, BULLET_SIZE);
    
    [_iconView reloadFromUrl:imageUrl];
    _iconView.frame = CGRectMake(MARGIN+BULLET_SIZE+MARGIN, MARGIN, IMAGE_SIZE, IMAGE_SIZE);
}

+ (CGFloat)tableView:(UITableView *)tableView calculateConditionRowHeightWithString:(NSString *)string
{
    CGFloat textMaxWidth = tableView.frame.size.width-3*MARGIN-IMAGE_SIZE;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont systemFontOfSize:14.0], NSFontAttributeName,
                                  nil];
    CGRect textRect = [string boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    return MAX(MARGIN+textRect.size.height+MARGIN,MARGIN+IMAGE_SIZE+MARGIN);
}

- (void)tableView:(UITableView *)tableView layoutConditionCellWithString:(NSString *)string icon:(UIImage *)image
{
    CGFloat rowHeight = [RulesTableViewCell tableView:tableView calculateRowHeightWithString:string];
    CGFloat textMaxWidth = tableView.frame.size.width-3*MARGIN-IMAGE_SIZE;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont systemFontOfSize:14.0], NSFontAttributeName,
                                  nil];
    CGRect textRect = [string boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    _textLabel.frame = CGRectMake(MARGIN+IMAGE_SIZE+MARGIN, rowHeight/2.0-textRect.size.height/2.0, textRect.size.width, textRect.size.height);
    _textLabel.text = string;
    
    _bulletView.frame = CGRectMake(MARGIN, rowHeight/2.0-BULLET_SIZE/2.0, BULLET_SIZE, BULLET_SIZE);
    _bulletView.hidden = YES;
    
    _iconView.image = image;
    _iconView.frame = CGRectMake(MARGIN, MARGIN, IMAGE_SIZE, IMAGE_SIZE);
}

@end
