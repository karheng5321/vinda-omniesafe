//
//  BOSSummaryTableViewCell.h
//  SCA
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BOSSummaryType) {
    BOSSummaryTypeDate = 0,
    BOSSummaryTypeHeader,
    BOSSummaryTypeListing
};

@interface BOSSummaryTableViewCell : UITableViewCell {
	
}

@property (nonatomic, readonly) UILabel *textLabel;

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightForField:(NSString *)field andValue:(NSString *)value type:(BOSSummaryType)type;
- (void)tableView:(UITableView *)tableView layoutCellWithForField:(NSString *)field andValue:(NSString *)value type:(BOSSummaryType)type;

@end
