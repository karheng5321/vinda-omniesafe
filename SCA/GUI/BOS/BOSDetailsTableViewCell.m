//
//  BOSDetailsTableViewCell.m
//  SCA
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import "BOSDetailsTableViewCell.h"
#import "Constant.h"
#import "AsyncImageView.h"

@interface BOSDetailsTableViewCell () {
	UILabel *_fieldLabel;
    UILabel *_valueLabel;
    
    AsyncImageView *_iconView;
}

@end

@implementation BOSDetailsTableViewCell

static float const IMAGE_SIZE = 60.0;
static float const MARGIN = 10.0;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		
        _fieldLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _fieldLabel.backgroundColor = [UIColor clearColor];
        _fieldLabel.textColor = [UIColor blackColor];
        _fieldLabel.text = @"";
        _fieldLabel.textAlignment = NSTextAlignmentLeft;
        _fieldLabel.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
        _fieldLabel.numberOfLines = 0;
        [self.contentView addSubview:_fieldLabel];
        
        _valueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _valueLabel.backgroundColor = [UIColor clearColor];
        _valueLabel.textColor = [UIColor blackColor];
        _valueLabel.text = @"";
        _valueLabel.textAlignment = NSTextAlignmentLeft;
        _valueLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        _valueLabel.numberOfLines = 0;
        [self.contentView addSubview:_valueLabel];
        
        _iconView = [[AsyncImageView alloc] initWithFrame:CGRectZero imageUrl:nil autoResize:NO downloadNew:NO];
        _iconView.contentMode = UIViewContentModeScaleAspectFill;
        _iconView.layer.cornerRadius = 10.0;
        _iconView.clipsToBounds = YES;
        [self.contentView addSubview:_iconView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightForField:(NSString *)field andValue:(NSString *)value imageUrl:(NSString *)imgUrl
{
    CGFloat fieldMaxWidth = (tableView.frame.size.width-3*MARGIN)*0.4;
    CGFloat valueMaxWidth = (tableView.frame.size.width-3*MARGIN)*0.6;
    if (imgUrl)
        valueMaxWidth = (valueMaxWidth-MARGIN-IMAGE_SIZE);
    NSDictionary *fieldAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [UIFont fontWithName:@"Roboto-Light" size:14.0], NSFontAttributeName,
                                   nil];
    NSDictionary *valueAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [UIFont fontWithName:@"Roboto-Medium" size:14.0], NSFontAttributeName,
                                   nil];
    CGRect fieldRect = [field boundingRectWithSize:CGSizeMake(fieldMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:fieldAttrDict context:nil];
    CGRect valueRect = [value boundingRectWithSize:CGSizeMake(valueMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:valueAttrDict context:nil];
    
    CGFloat valueHeight = imgUrl? MAX(IMAGE_SIZE,valueRect.size.height):valueRect.size.height;
    return MARGIN+MAX(fieldRect.size.height,valueHeight)+MARGIN;
}

- (void)tableView:(UITableView *)tableView layoutCellWithForField:(NSString *)field andValue:(NSString *)value imageUrl:(NSString *)imgUrl
{
    //CGFloat rowHeight = [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:type];
    
    CGFloat fieldMaxWidth = (tableView.frame.size.width-3*MARGIN)*0.4;
    CGFloat valueMaxWidth = (tableView.frame.size.width-3*MARGIN)*0.6;
    if (imgUrl)
        valueMaxWidth = (valueMaxWidth-MARGIN-IMAGE_SIZE);
    NSDictionary *fieldAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [UIFont fontWithName:@"Roboto-Light" size:14.0], NSFontAttributeName,
                                   nil];
    NSDictionary *valueAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [UIFont fontWithName:@"Roboto-Medium" size:14.0], NSFontAttributeName,
                                   nil];
    CGRect fieldRect = [field boundingRectWithSize:CGSizeMake(fieldMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:fieldAttrDict context:nil];
    CGRect valueRect = [value boundingRectWithSize:CGSizeMake(valueMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:valueAttrDict context:nil];
    
    _fieldLabel.frame = CGRectMake(MARGIN, MARGIN, fieldRect.size.width, fieldRect.size.height);
    _fieldLabel.text = field;
    
    float initX = MARGIN+fieldMaxWidth+MARGIN;
    
    if (imgUrl)
    {
        _iconView.frame = CGRectMake(initX, MARGIN, IMAGE_SIZE, IMAGE_SIZE);
        [_iconView reloadFromUrl:imgUrl];
        initX += MARGIN+IMAGE_SIZE;
    }
    else
    {
        _iconView.frame = CGRectZero;
        _iconView.image = nil;
    }
    
    _valueLabel.frame = CGRectMake(initX, MARGIN, valueRect.size.width, valueRect.size.height);
    _valueLabel.text = value;
    
    
}

@end
