//
//  BOSActionViewController.m
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BOSActionViewController.h"
#import "AsyncApi.h"
#import "BOSPreviewViewController.h"
#import "ColorPlaceholderTextField.h"
#import "ActionListViewController.h"

@interface BOSActionViewController () <AsyncApiDelegate, UITextFieldDelegate, UITextViewDelegate, ActionListViewControllerDelegate> {
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    float _titleHeight;
    
    UIView *_loadingView;
    
    UIView *_navView;
    UILabel *_titleLabel;
    UIScrollView *_mainView;
    
    NSMutableArray *_dataArray;
    
    ColorPlaceholderTextField *_titleField;
    UITextView *_detailsField;
    
    NSMutableDictionary *_sessionDict;
}

@end

@implementation BOSActionViewController

static NSString* const SUBMIT_ACTION_CALL_ID = @"SUBMIT_ACTION_CALL_ID";

- (id)initWithDictionary:(NSMutableDictionary *)sessionDict
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        _sessionDict = sessionDict;
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        //self.navigationItem.title = @"";
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        [_mainView addGestureRecognizer:singleTap];
    }
    return self;
}

- (void)initPage
{
    _statusBarHeight = 0.0;
    _navBarHeight = 120.0;
    _titleHeight = 95.0;
    _tabBarHeight = 0.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_statusBarHeight-_tabBarHeight;
    
    _navView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _screenWidth, _navBarHeight)];
    _navView.backgroundColor = [UIColor appBOSBlueColor];
    [self.view addSubview:_navView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30.0, 0.0, _screenWidth-2*30.0, _navView.frame.size.height)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.text = NSLocalizedString(@"action_taken", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:35.0];
    _titleLabel.numberOfLines = 2;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(_navView.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _navView.frame.size.height/2.0-_titleLabel.frame.size.height/2.0, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [_navView addSubview:_titleLabel];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(8.0, 42.0, 35.0, _navBarHeight-2*42);
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];
    
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _navBarHeight+_statusBarHeight, _screenWidth, _screenHeight)];
    _mainView.backgroundColor = [UIColor clearColor];
    _mainView.showsHorizontalScrollIndicator = NO;
    _mainView.bounces = NO;
    _mainView.contentSize = _mainView.frame.size;
    [self.view addSubview:_mainView];
    
    CGFloat currentHeight = 20.0;
    CGFloat sideGap = 20.0;
    CGFloat nextBtnHeight = 38.0;
    
    UIView *detailsBox = [[UIView alloc] initWithFrame:CGRectMake(sideGap, currentHeight, _mainView.frame.size.width-2*sideGap, 50.0)];
    detailsBox.layer.cornerRadius = 10.0;
    detailsBox.layer.borderColor = [[UIColor blackColor] CGColor];
    detailsBox.layer.borderWidth = 1.0;
    [_mainView addSubview:detailsBox];
    
    _titleField = [[ColorPlaceholderTextField alloc] initWithFrame:CGRectMake(10.0, 10.0, detailsBox.frame.size.width-2*10.0, detailsBox.frame.size.height-2*10.0)];
    _titleField.text = @"";
    _titleField.placeholder = [NSLocalizedString(@"select_action", nil) uppercaseString];
    _titleField.placeholderColor = [UIColor blackColor];
    _titleField.delegate = self;
    _titleField.borderStyle = UITextBorderStyleNone;
    _titleField.textColor = [UIColor blackColor];
    _titleField.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    _titleField.keyboardType = UIKeyboardTypeDefault;
    _titleField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _titleField.textAlignment = NSTextAlignmentLeft;
    _titleField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    _titleField.returnKeyType = UIReturnKeyDone;
    [detailsBox addSubview:_titleField];
    
    currentHeight += detailsBox.frame.size.height+20.0;
    
    detailsBox = [[UIView alloc] initWithFrame:CGRectMake(sideGap, currentHeight, _mainView.frame.size.width-2*sideGap, _mainView.frame.size.height-currentHeight-2*17.0-nextBtnHeight)];
    detailsBox.layer.cornerRadius = 10.0;
    detailsBox.layer.borderColor = [[UIColor blackColor] CGColor];
    detailsBox.layer.borderWidth = 1.0;
    [_mainView addSubview:detailsBox];
    
    NSString *descStg = [NSString stringWithFormat:@"%@:", [NSLocalizedString(@"remark", nil) uppercaseString]];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:descStg];
    [attrText addAttribute:NSFontAttributeName
                     value:[UIFont fontWithName:@"Roboto-Medium" size:14.0]
                     range:NSMakeRange(0, descStg.length)];
    CGRect descRect = [attrText boundingRectWithSize:CGSizeMake(detailsBox.frame.size.width-20.0, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil];
    
    UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0, descRect.size.width, descRect.size.height)];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.textColor = [UIColor blackColor];
    descLabel.attributedText = attrText;
    descLabel.textAlignment = NSTextAlignmentLeft;
    descLabel.lineBreakMode = NSLineBreakByWordWrapping;
    descLabel.numberOfLines = 0;
    [detailsBox addSubview:descLabel];
    
    _detailsField = [[UITextView alloc] initWithFrame:CGRectMake(5.0, 10.0+descRect.size.height+5.0, detailsBox.frame.size.width-10.0, detailsBox.frame.size.height-10.0-descRect.size.height-5.0-10.0)];
    _detailsField.text = @"";
    _detailsField.font = [UIFont fontWithName:@"Roboto-Regular" size:14.0];
    _detailsField.textColor = [UIColor blackColor];
    _detailsField.delegate = self;
    _detailsField.keyboardType = UIKeyboardTypeDefault;
    _detailsField.textAlignment = NSTextAlignmentLeft;
    _detailsField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    _detailsField.backgroundColor = [UIColor clearColor];
    [detailsBox addSubview:_detailsField];
    
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(20.0, _mainView.frame.size.height-17.0-nextBtnHeight, _mainView.frame.size.width-2*20.0, nextBtnHeight);
    nextBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [nextBtn setTitle:NSLocalizedString(@"submit", nil) forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.2] forState:UIControlStateHighlighted];
    [nextBtn addTarget:self action:@selector(nextTapped:) forControlEvents:UIControlEventTouchUpInside];
    nextBtn.backgroundColor = [UIColor appGreenColor];
    nextBtn.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:19.0];
    nextBtn.layer.cornerRadius = nextBtnHeight/2.0;
    [_mainView addSubview:nextBtn];
}

- (void)viewTapped:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionTapped:(UITextField *)sender
{
    ActionListViewController *listVC = [[ActionListViewController alloc] initWithString:_titleField.text];
    listVC.delegate = self;
    [self presentViewController:listVC animated:YES completion:nil];
}

- (void)nextTapped:(UIButton *)sender
{
    if (!_detailsField.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"action_error", nil) message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [self setLoadingHidden:NO];
    
    NSString *reportId = [NSString stringWithFormat:@"%@", [_sessionDict valueForKey:API_REPORT_ID_KEY]];
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi report:reportId takeAction:_detailsField.text :SUBMIT_ACTION_CALL_ID];
    
    //[_sessionDict setValue:_detailsField.text forKey:@"action"];
    //BOSPreviewViewController *previewVC = [[BOSPreviewViewController alloc] initWithDictionary:_sessionDict];
    //[self.navigationController pushViewController:previewVC animated:YES];
}

- (UIView *)findFirstResponder
{
    for (UIView *subview in _mainView.subviews) {
        if ([subview isFirstResponder])
            return subview;
    }
    
    return nil;
}

- (void)expandMainView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight);
    
    [UIView commitAnimations];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)collapseMainView:(float)keyboardHeight
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight-keyboardHeight+_tabBarHeight);
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ActionListViewControllerDelegate

- (void)viewController:(UIViewController *)vc menuSelected:(NSMutableArray *)array
{
    //NSLog(@"viewController: %@, menuSelected: %@", vc, array);
    NSDictionary *dict = [array objectAtIndex:0];
    NSString *name = [dict valueForKey:@"text"];
    
    _titleField.text = name;
    
    if ([[name uppercaseString] isEqualToString:[NSLocalizedString(@"others", nil) uppercaseString]])
    {
        _detailsField.text = @"";
        [_detailsField becomeFirstResponder];
    }
    else
    {
        _detailsField.text = name;
        [_detailsField resignFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate

- (void)keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    //NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    NSLog(@"keyboardBounds: %f, %f, %f, %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.height, keyboardBounds.size.width);
    [self collapseMainView:keyboardBounds.size.height];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _titleField)
    {
        [self actionTapped:textField];
        return NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    [self expandMainView];
    
    //[self performSearch];
    
    return YES;
}

- (void)textFieldDidChanged:(NSNotification *)notification
{
    //_searchPlaceholder.hidden = _searchBar.text.length;
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (![[_titleField.text uppercaseString] isEqualToString:[NSLocalizedString(@"others", nil) uppercaseString]])
        return NO;
    NSLog(@"textViewShouldBeginEditing");
    
    UIView *keyboardAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 30.0)];
    keyboardAccessoryView.backgroundColor = [UIColor colorWithWhite:0.85 alpha:1.0];
    
    UIButton *okBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    okBtn.frame = CGRectMake(self.view.frame.size.width-70.0, 0.0, 70.0, 30.0);
    [okBtn setTitle:@"Done" forState:UIControlStateNormal];
    [okBtn setTitle:@"Done" forState:UIControlStateHighlighted];
    [okBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [okBtn addTarget:self action:@selector(doneTapped:) forControlEvents:UIControlEventTouchUpInside];
    okBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
    [keyboardAccessoryView addSubview:okBtn];
    
    textView.inputAccessoryView = keyboardAccessoryView;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    return YES;
}

- (void)doneTapped:(UIButton *)sender
{
    [_detailsField resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView
{
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self.view endEditing:YES];
    [self expandMainView];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:SUBMIT_ACTION_CALL_ID])
    {
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
