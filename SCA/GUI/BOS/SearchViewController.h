//
//  SearchViewController.h
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchViewControllerDelegate <NSObject>

@optional

- (void)searchResultSelected:(NSDictionary *)result;

@end

@interface SearchViewController : UIViewController

- (id)initWithString:(NSString *)string;

@property (nonatomic, retain) id <SearchViewControllerDelegate> delegate;

@end
