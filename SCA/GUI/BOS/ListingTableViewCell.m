//
//  ListingTableViewCell.m
//  SCA
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import "ListingTableViewCell.h"
#import "Constant.h"

@interface ListingTableViewCell () {
	UILabel *_col1Label;
    UILabel *_col2Label;
    UILabel *_col3Label;
    UILabel *_stateLabel;
}

@end

@implementation ListingTableViewCell

static float const MARGIN = 10.0;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		
        _col1Label = [[UILabel alloc] initWithFrame:CGRectZero];
        _col1Label.backgroundColor = [UIColor clearColor];
        _col1Label.textColor = [UIColor blackColor];
        _col1Label.text = @"";
        _col1Label.textAlignment = NSTextAlignmentLeft;
        _col1Label.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
        _col1Label.numberOfLines = 2;
        _col1Label.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.contentView addSubview:_col1Label];
        
        _col2Label = [[UILabel alloc] initWithFrame:CGRectZero];
        _col2Label.backgroundColor = [UIColor clearColor];
        _col2Label.textColor = [UIColor blackColor];
        _col2Label.text = @"";
        _col2Label.textAlignment = NSTextAlignmentLeft;
        _col2Label.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
        _col2Label.numberOfLines = 1;
        _col2Label.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.contentView addSubview:_col2Label];
        
        _col3Label = [[UILabel alloc] initWithFrame:CGRectZero];
        _col3Label.backgroundColor = [UIColor clearColor];
        _col3Label.textColor = [UIColor blackColor];
        _col3Label.text = @"";
        _col3Label.textAlignment = NSTextAlignmentLeft;
        _col3Label.font = [UIFont fontWithName:@"Roboto-Light" size:13.0];
        _col3Label.numberOfLines = 1;
        _col3Label.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.contentView addSubview:_col3Label];
        
        _stateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _stateLabel.backgroundColor = [UIColor redColor];
        _stateLabel.textColor = [UIColor whiteColor];
        _stateLabel.text = @"";
        _stateLabel.textAlignment = NSTextAlignmentCenter;
        _stateLabel.font = [UIFont italicSystemFontOfSize:12.0];
        _stateLabel.numberOfLines = 1;
        _stateLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.contentView addSubview:_stateLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightWithDictionary:(NSDictionary *)dict
{
    return 50.0;
}

- (void)tableView:(UITableView *)tableView layoutCellWithDictionary:(NSDictionary *)dict
{
    CGFloat rowHeight = [ListingTableViewCell tableView:tableView calculateRowHeightWithDictionary:dict];
    
    CGFloat col1Width = (tableView.frame.size.width-4*10.0)*0.3;
    CGFloat col2Width = (tableView.frame.size.width-4*10.0)*0.3;
    CGFloat col3Width = (tableView.frame.size.width-4*10.0)*0.4;
    
    NSString *timestampString = [dict valueForKey:@"reported_timestamp"];
    NSLog(@"timestampString: %@", timestampString);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *reportDate = [dateFormatter dateFromString:timestampString];
    [dateFormatter setDateFormat:@"dd/MM/yyyy, hh:mm aa"];
    NSString *submitDateString = [dateFormatter stringFromDate:reportDate];
    NSLog(@"submitDateString: %@", submitDateString);
    
    _col1Label.frame = CGRectMake(MARGIN, 0.0, col1Width, rowHeight);
    _col1Label.text = submitDateString;
    
    _col2Label.frame = CGRectMake(MARGIN+col1Width+MARGIN, 0.0, col2Width, rowHeight);
    _col2Label.text = [dict valueForKey:API_REPORT_ID_KEY];
    
    _col3Label.frame = CGRectMake(MARGIN+col1Width+MARGIN+col2Width+MARGIN, 0.0, col3Width, rowHeight);
    _col3Label.text = [dict valueForKey:@"name"];
    
    NSInteger status = [[dict valueForKey:@"status"] integerValue];
    NSString *state = @"";
    switch (status) {
        case 1:
            state = NSLocalizedString(@"new", nil);
            break;
        case 2:
            state = NSLocalizedString(@"new", nil);
            break;
        case 3:
            state = NSLocalizedString(@"overdue", nil);
            break;
        default:
            break;
    }
    
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _stateLabel.font, NSFontAttributeName,
                                  nil];
    CGRect textRect = [state boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    _stateLabel.frame = CGRectMake(tableView.frame.size.width-MARGIN-textRect.size.width-4.0, 5.0, textRect.size.width+4.0, textRect.size.height);
    _stateLabel.text = state;
    
    _stateLabel.backgroundColor = ([state isEqualToString:NSLocalizedString(@"new", nil)])? [UIColor blueColor]: ([state rangeOfString:NSLocalizedString(@"overdue", nil)].length)? [UIColor redColor]:[UIColor orangeColor];
}

@end
