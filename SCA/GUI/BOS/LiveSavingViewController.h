//
//  LiveSavingViewController.h
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveSavingViewController : UIViewController

- (id)initWithDictionary:(NSMutableDictionary *)dict;

@end
