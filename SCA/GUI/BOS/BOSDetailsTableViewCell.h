//
//  BOSDetailsTableViewCell.h
//  SCA
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BOSDetailsTableViewCell : UITableViewCell {
	
}

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightForField:(NSString *)field andValue:(NSString *)value imageUrl:(NSString *)imgUrl;
- (void)tableView:(UITableView *)tableView layoutCellWithForField:(NSString *)field andValue:(NSString *)value imageUrl:(NSString *)imgUrl;

@end
