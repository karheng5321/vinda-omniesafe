//
//  DynamicTableViewCell.h
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DynamicTableViewCell : UITableViewCell {
	
}

@property (nonatomic, readonly) UILabel *textLabel;

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightWithString:(NSString *)string;
- (void)tableView:(UITableView *)tableView layoutCellWithString:(NSString *)string;

@end
