//
//  ListingTableViewCell.h
//  SCA
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListingTableViewCell : UITableViewCell {
	
}

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightWithDictionary:(NSDictionary *)dict;
- (void)tableView:(UITableView *)tableView layoutCellWithDictionary:(NSDictionary *)dict;

@end
