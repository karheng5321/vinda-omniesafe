//
//  BOSDetailsViewController.m
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BOSDetailsViewController.h"
#import "AsyncApi.h"
#import "BOSDetailsTableViewCell.h"
#import "BOSActionViewController.h"
#import "AppUtil.h"

@interface BOSDetailsViewController () <AsyncApiDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate> {
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    float _titleHeight;
    
    UIView *_loadingView;
    
    UIView *_navView;
    UILabel *_titleLabel;
    UIScrollView *_mainView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    
    NSInteger _ID;
    NSMutableDictionary *_sessionDict;
    
    BOOL _fromNotif;
    BOOL _sameSite;
}

@end

@implementation BOSDetailsViewController

static NSString* const GET_PAGE_DATA_CALL_ID = @"getpagedata";
static NSString* const SUBMIT_ACTION_CALL_ID = @"SUBMIT_ACTION_CALL_ID";

- (id)initWithId:(NSInteger)ID fromNotification:(BOOL)fromNotif sameSite:(BOOL)sameSite
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        _ID = ID;
        _fromNotif = fromNotif;
        _sameSite = sameSite;
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        //self.navigationItem.title = @"";
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
    }
    return self;
}

- (void)initPage
{
    _statusBarHeight = 0.0;
    _navBarHeight = 120.0;
    _titleHeight = 95.0;
    _tabBarHeight = 0.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_statusBarHeight-_tabBarHeight;
    
    _navView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _screenWidth, _navBarHeight)];
    _navView.backgroundColor = [UIColor appBOSBlueColor];
    [self.view addSubview:_navView];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isBosSuper = [[roles objectForKey:ROLES_BOS_SUPERVISOR_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30.0, 0.0, _screenWidth-2*30.0, _navView.frame.size.height)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.text = NSLocalizedString(@"BOS details title", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:35.0];
    _titleLabel.numberOfLines = 2;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(_navView.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _navView.frame.size.height/2.0-_titleLabel.frame.size.height/2.0, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [_navView addSubview:_titleLabel];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(8.0, 42.0, 35.0, _navBarHeight-2*42);
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];
    
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _navBarHeight+_statusBarHeight, _screenWidth, _screenHeight)];
    _mainView.backgroundColor = [UIColor clearColor];
    _mainView.showsHorizontalScrollIndicator = NO;
    _mainView.bounces = NO;
    _mainView.contentSize = _mainView.frame.size;
    [self.view addSubview:_mainView];
    
    CGFloat currentHeight = 0.0;
    CGFloat nextBtnHeight = 38.0;
    
    if ((isBosSuper || isAdmin) && !_fromNotif)
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, currentHeight, _mainView.frame.size.width, _mainView.frame.size.height-currentHeight-2*17.0-nextBtnHeight) style:UITableViewStylePlain];
    else
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, currentHeight, _mainView.frame.size.width, _mainView.frame.size.height-currentHeight) style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_mainView addSubview:_tableView];
    
    /*UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _tableView.frame.size.width, 40.0)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.text = [_dict objectForKey:@"code"];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Roboto-Medium" size:18.0];
    [label sizeToFit];
    label.frame = CGRectMake(headerView.frame.size.width/2.0-label.frame.size.width/2.0, headerView.frame.size.height/2.0-label.frame.size.height/2.0, label.frame.size.width, label.frame.size.height);
    [headerView addSubview:label];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(15.0, headerView.frame.size.height-1.0, headerView.frame.size.width-2*15.0, 1.0)];
    line.backgroundColor = [UIColor blackColor];
    [headerView addSubview:line];
    
    _tableView.tableHeaderView = headerView;*/
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeTapped:(UIButton *)sender
{
    if (!_sessionDict)
        return;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"close_confirmation", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *reportId = [NSString stringWithFormat:@"%@", [_sessionDict valueForKey:API_REPORT_ID_KEY]];
        AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
        [asyncApi report:reportId takeAction:@"" :SUBMIT_ACTION_CALL_ID];
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)takeActionTapped:(UIButton *)sender
{
    if (!_sessionDict)
        return;
    
    BOSActionViewController *actionVC = [[BOSActionViewController alloc] initWithDictionary:_sessionDict];
    [self.navigationController pushViewController:actionVC animated:YES];
}

- (void)getPageData
{
    [self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi bosReport:[NSString stringWithFormat:@"%ld", _ID] getDetails:_sameSite :GET_PAGE_DATA_CALL_ID];
}

- (void)processData:(NSMutableDictionary *)dict
{
    _sessionDict = dict;
    NSLog(@"_sessionDict: %@", _sessionDict);
    if (_dataArray)
        [_dataArray removeAllObjects];
    else
        _dataArray = [[NSMutableArray alloc] init];
    
    NSString *timestampString = [dict valueForKey:@"reported_timestamp"];
    NSLog(@"timestampString: %@", timestampString);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *reportDate = [dateFormatter dateFromString:timestampString];
    [dateFormatter setDateFormat:@"dd/MM/yyyy, hh:mm aa"];
    NSString *submitDateString = [dateFormatter stringFromDate:reportDate];
    NSLog(@"submitDateString: %@", submitDateString);
    
    NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"submitter_name", nil), @"field",
                          [dict valueForKey:@"reporter_name"], @"value", nil];
    [_dataArray addObject:item];
    item = [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"date_time", nil), @"field",
            submitDateString, @"value", nil];
    [_dataArray addObject:item];
    item = [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"reported_name", nil), @"field",
            [dict valueForKey:@"reported_name"], @"value", nil];
    [_dataArray addObject:item];
    item = [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"lsr_type", nil), @"field",
            [dict valueForKey:@"name"], @"value", [dict valueForKey:@"icon_url"], @"icon", nil];
    [_dataArray addObject:item];
    item = [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"department", nil), @"field",
            [dict valueForKey:@"dept_name"], @"value", nil];
    [_dataArray addObject:item];
    item = [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"immediate supervisor", nil), @"field",
            [dict valueForKey:@"supervisor_name"], @"value", nil];
    [_dataArray addObject:item];
    item = [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"behaviour_type", nil), @"field",
            [dict valueForKey:@"behaviour"], @"value", nil];
    [_dataArray addObject:item];
    item = [NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"remark", nil), @"field",
            [dict valueForKey:@"feedback"], @"value", nil];
    [_dataArray addObject:item];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _tableView.frame.size.width, 40.0)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.text = [dict objectForKey:@"code"];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Roboto-Medium" size:18.0];
    [label sizeToFit];
    label.frame = CGRectMake(headerView.frame.size.width/2.0-label.frame.size.width/2.0, headerView.frame.size.height/2.0-label.frame.size.height/2.0, label.frame.size.width, label.frame.size.height);
    [headerView addSubview:label];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(15.0, headerView.frame.size.height-1.0, headerView.frame.size.width-2*15.0, 1.0)];
    line.backgroundColor = [UIColor blackColor];
    [headerView addSubview:line];
    
    _tableView.tableHeaderView = headerView;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isBosSuper = [[roles objectForKey:ROLES_BOS_SUPERVISOR_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    if ((isBosSuper || isAdmin) && !_fromNotif)
    {
        CGFloat nextBtnHeight = 38.0;
        
        NSString *behaviour = [_sessionDict valueForKey:@"behaviour"];
        BOOL isSafe = [[behaviour uppercaseString] isEqualToString:[NSLocalizedString(@"safe", nil) uppercaseString]];
        
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.frame = CGRectMake(20.0, _mainView.frame.size.height-17.0-nextBtnHeight, _mainView.frame.size.width-2*20.0, nextBtnHeight);
        nextBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [nextBtn setTitle:isSafe? @"OK":NSLocalizedString(@"take_action", nil) forState:UIControlStateNormal];
        [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [nextBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.2] forState:UIControlStateHighlighted];
        if (isSafe)
            [nextBtn addTarget:self action:@selector(closeTapped:) forControlEvents:UIControlEventTouchUpInside];
        else
            [nextBtn addTarget:self action:@selector(takeActionTapped:) forControlEvents:UIControlEventTouchUpInside];
        nextBtn.backgroundColor = [UIColor appGreenColor];
        nextBtn.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:19.0];
        nextBtn.layer.cornerRadius = nextBtnHeight/2.0;
        [_mainView addSubview:nextBtn];
    }
}

- (UIView *)findFirstResponder
{
    for (UIView *subview in _mainView.subviews) {
        if ([subview isFirstResponder])
            return subview;
    }
    
    return nil;
}

- (void)expandMainView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight);
    
    [UIView commitAnimations];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)collapseMainView:(float)keyboardHeight
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight-keyboardHeight+_tabBarHeight);
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self getPageData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate

- (void)keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    //NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    NSLog(@"keyboardBounds: %f, %f, %f, %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.height, keyboardBounds.size.width);
    //[self collapseMainView:keyboardBounds.size.height];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    //[self expandMainView];
    
    //[self performSearch];
    
    return YES;
}

- (void)textFieldDidChanged:(NSNotification *)notification
{
    //_searchPlaceholder.hidden = _searchBar.text.length;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _dataArray? [_dataArray count]:0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    return [BOSDetailsTableViewCell tableView:tableView calculateRowHeightForField:[dict valueForKey:@"field"] andValue:[dict valueForKey:@"value"] imageUrl:[dict valueForKey:@"icon"]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    BOSDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[BOSDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    [cell tableView:tableView layoutCellWithForField:[dict valueForKey:@"field"] andValue:[dict valueForKey:@"value"] imageUrl:[dict valueForKey:@"icon"]];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_PAGE_DATA_CALL_ID])
    {
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableDictionary *content = [AppUtil removeNullElementFromObject:result.result];
            [self processData:content];
            [_tableView reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
    else if ([callId isEqualToString:SUBMIT_ACTION_CALL_ID])
    {
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:result.errorMsg message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
