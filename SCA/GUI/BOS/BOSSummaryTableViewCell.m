//
//  BOSSummaryTableViewCell.m
//  SCA
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import "BOSSummaryTableViewCell.h"
#import "Constant.h"
#import "AsyncImageView.h"

@interface BOSSummaryTableViewCell () {
	UILabel *_fieldLabel;
    UILabel *_valueLabel;
    UILabel *_separatorLabel;
}

@end

@implementation BOSSummaryTableViewCell

@synthesize textLabel = _textLabel;

static float const MARGIN = 10.0;
static NSString * const SEPARATOR = @":";

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		
        _fieldLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _fieldLabel.backgroundColor = [UIColor clearColor];
        _fieldLabel.textColor = [UIColor blackColor];
        _fieldLabel.text = @"";
        _fieldLabel.textAlignment = NSTextAlignmentLeft;
        _fieldLabel.font = [UIFont systemFontOfSize:14.0];
        _fieldLabel.numberOfLines = 0;
        [self.contentView addSubview:_fieldLabel];
        
        _valueLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _valueLabel.backgroundColor = [UIColor clearColor];
        _valueLabel.textColor = [UIColor blackColor];
        _valueLabel.text = @"";
        _valueLabel.textAlignment = NSTextAlignmentLeft;
        _valueLabel.font = [UIFont systemFontOfSize:14.0];
        _valueLabel.numberOfLines = 0;
        [self.contentView addSubview:_valueLabel];
        
        _separatorLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _separatorLabel.backgroundColor = [UIColor clearColor];
        _separatorLabel.textColor = [UIColor blackColor];
        _separatorLabel.text = @"";
        _separatorLabel.textAlignment = NSTextAlignmentLeft;
        _separatorLabel.font = [UIFont systemFontOfSize:14.0];
        _separatorLabel.numberOfLines = 0;
        [self.contentView addSubview:_separatorLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightForField:(NSString *)field andValue:(NSString *)value type:(BOSSummaryType)type
{
    UIFont *font = (type == BOSSummaryTypeListing)? [UIFont systemFontOfSize:14.0]:[UIFont boldSystemFontOfSize:14.0];
    
    CGFloat fieldMaxWidth = (type == BOSSummaryTypeListing)? MAX(80.0, tableView.frame.size.width/4.0):tableView.frame.size.width-2*MARGIN;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                 font, NSFontAttributeName,
                                 nil];
    CGRect fieldRect = [field boundingRectWithSize:CGSizeMake(fieldMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    CGRect separatorRect = [SEPARATOR boundingRectWithSize:CGSizeMake(fieldMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    if (type != BOSSummaryTypeHeader)
    {
        CGFloat valueMaxWidth = tableView.frame.size.width-2*MARGIN-10.0-separatorRect.size.width-fieldMaxWidth;
        CGRect valueRect = [value boundingRectWithSize:CGSizeMake(valueMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
        
        return MARGIN+valueRect.size.height+MARGIN;
    }
    
    return MARGIN+fieldRect.size.height+MARGIN;
}

- (void)tableView:(UITableView *)tableView layoutCellWithForField:(NSString *)field andValue:(NSString *)value type:(BOSSummaryType)type
{
    //CGFloat rowHeight = [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:type];
    
    UIFont *font = (type == BOSSummaryTypeListing)? [UIFont systemFontOfSize:14.0]:[UIFont boldSystemFontOfSize:14.0];
    UIColor *textColor = (type == BOSSummaryTypeHeader)? [UIColor appDarkBlueColor]:[UIColor blackColor];
    
    CGFloat fieldMaxWidth = (type == BOSSummaryTypeListing)? MAX(80.0, tableView.frame.size.width/4.0):tableView.frame.size.width-2*MARGIN;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  font, NSFontAttributeName,
                                  nil];
    CGRect fieldRect = [field boundingRectWithSize:CGSizeMake(fieldMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    _fieldLabel.frame = CGRectMake(MARGIN, MARGIN, fieldRect.size.width, fieldRect.size.height);
    _fieldLabel.text = field;
    _fieldLabel.textColor = textColor;
    _fieldLabel.font = font;
    
    CGRect separatorRect = [SEPARATOR boundingRectWithSize:CGSizeMake(fieldMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    _separatorLabel.frame = (type == BOSSummaryTypeListing)? CGRectMake(MARGIN+fieldMaxWidth+5.0, MARGIN, separatorRect.size.width, fieldRect.size.height):CGRectMake(MARGIN+fieldRect.size.width+5.0, MARGIN, separatorRect.size.width, fieldRect.size.height);
    _separatorLabel.text = SEPARATOR;
    _separatorLabel.textColor = textColor;
    _separatorLabel.font = font;
    
    if (type != BOSSummaryTypeHeader)
    {
        CGFloat valueMaxWidth = tableView.frame.size.width-2*MARGIN-10.0-separatorRect.size.width-fieldMaxWidth;
        CGRect valueRect = [value boundingRectWithSize:CGSizeMake(valueMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
        
        _valueLabel.frame = CGRectMake(_separatorLabel.frame.origin.x+_separatorLabel.frame.size.width+5.0, MARGIN, valueRect.size.width, valueRect.size.height);
        _valueLabel.text = value;
        _valueLabel.textColor = textColor;
        _valueLabel.font = font;
    }
    else
    {
        _valueLabel.frame = CGRectZero;
        _valueLabel.text = @"";
        _valueLabel.textColor = textColor;
        _valueLabel.font = font;
    }
}

@end
