//
//  SearchViewController.m
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "SearchViewController.h"
#import "AsyncApi.h"
#import "AppUtil.h"

@interface SearchViewController () <AsyncApiDelegate, UISearchBarDelegate, UITextFieldDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource> {
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    
    UIView *_loadingView;
    
    UIScrollView *_mainView;
    UITableView *_tableView;
    
    UISearchBar *_searchBar;
    
    NSMutableArray *_searchArray;
    
    AsyncApi *_asyncApi;
    
    NSString *_string;
}

@end

@implementation SearchViewController

@synthesize delegate;

static NSString* const GET_SUGGESTION_CALL_ID_PREFIX = @"getsuggestion_";

static CGFloat const SECTION_HEIGHT = 30.0;
static CGFloat const CONTENT_MARGIN = 15.0;

- (id)initWithString:(NSString *)string
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        _string = string;
        
        [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                      [UIColor blackColor],
                                                                                                      NSForegroundColorAttributeName,
                                                                                                      nil]
                                                                                            forState:UIControlStateNormal];
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        //self.navigationItem.title = @"";
        
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 44.0)];
        _searchBar.delegate = self;
        _searchBar.placeholder = NSLocalizedString(@"search_placeholder", nil);
        _searchBar.tintColor = [UIColor whiteColor];
        if([_searchBar respondsToSelector:@selector(setBarTintColor:)]){
            _searchBar.barTintColor = [UIColor whiteColor];
        }
        _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
        _searchBar.showsCancelButton = YES;
        _searchBar.enablesReturnKeyAutomatically = NO;
        [_searchBar setValue:NSLocalizedString(@"cancel", nil) forKey:@"_cancelButtonText"];
        self.navigationItem.titleView = _searchBar;
        
        _searchBar.text = _string;
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
    }
    return self;
}

- (void)initPage
{
    _navBarHeight = 44.0;
    _tabBarHeight = 0.0;
    _statusBarHeight = 20.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_tabBarHeight-_statusBarHeight;
    
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _navBarHeight+_statusBarHeight, _screenWidth, _screenHeight)];
    _mainView.backgroundColor = [UIColor clearColor];
    _mainView.showsHorizontalScrollIndicator = NO;
    _mainView.bounces = NO;
    _mainView.delegate = self;
    _mainView.contentSize = _mainView.frame.size;
    [self.view addSubview:_mainView];
    
    float currentHeight = 0.0;
    //float sideGap = 10.0;
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, currentHeight, _mainView.frame.size.width, _mainView.frame.size.height-currentHeight) style:UITableViewStylePlain];
    //_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_mainView addSubview:_tableView];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)getSuggestion
{
    NSString *callId = [NSString stringWithFormat:@"%@%@", GET_SUGGESTION_CALL_ID_PREFIX, _searchBar.text];
    if (!_asyncApi)
        _asyncApi = [[AsyncApi alloc] initWithCaller:self];
    else
        [_asyncApi cancel];
    
    if (_searchBar.text.length)
        [_asyncApi bosSearchEmpNo:_searchBar.text :callId];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_searchBar becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UISearchBarDelegate

/*- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return NO;
}*/

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    
    /*[_searchBar setShowsCancelButton:YES animated:YES];
    
    if([theSearchBar.text length] > 0)
    {
        _searching = YES;
    }
    else
    {
        _searching = NO;
    }*/
    
    [_searchBar setShowsCancelButton:YES animated:YES];
    
    if (theSearchBar.text.length > 0)
        [self getSuggestion];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar;
{
    //[_searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    
    //OLD COMMENT//NSLog(@"search bar did change");
    
    //Remove all objects first.
    /*[_listOfItems removeAllObjects];
    
    if([searchText length] > 0)
    {
        _searching = YES;
        [self searchTableView];
    }
    else
    {
        _searching = NO;
    }
    
    [_tableView reloadData];*/
    
    if (searchText.length == 0)
    {
        _searchArray = nil;
        [_tableView reloadData];
        //_tableView.hidden = YES;
    }
    else
    {
        [self getSuggestion];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    //[_searchBar setShowsCancelButton:NO animated:YES];
    [_searchBar resignFirstResponder];
    
    for (id subview in [_searchBar subviews]) {
        if ([subview isKindOfClass:[UIButton class]]) {
            [subview setEnabled:TRUE];
        }
    }
    
    //[self startSearchingInCategory:@""];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    NSLog(@"searchBarCancelButtonClicked");
    /*[_listOfItems removeAllObjects];
    _searching = NO;
    _searchBar.text = nil;
    
    [_searchBar setShowsCancelButton:NO animated:YES];
    [_searchBar resignFirstResponder];
    
    [_tableView reloadData];*/
    
    //[_searchBar setShowsCancelButton:NO animated:YES];
    [_searchBar resignFirstResponder];
    
    //[self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/*- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == _searchTableView)
        return nil;
    
    switch (section) {
        case 0:
            return nil;
            break;
        case 1:
            return @"RECENT SEARCHES";
            break;
        case 2:
            return @"SAVED SEARCHES";
            break;
        default:
            return 0;
            break;
    }
}*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _searchArray? [_searchArray count]:0;
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _searchTableView)
        return _navBarHeight;
    
    if (indexPath.section == 1)
    {
        CGFloat sideGap = 5.0;
        CGFloat width = tableView.frame.size.width-2*sideGap;
        CGFloat scale = width/300.0;
        CGFloat height = 250.0/scale;
        
        return height+sideGap;
    }
    
    return [ListingTableViewCell tableView:tableView calculateRowHeightForDictionary:nil];
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    for (UIView *subview in cell.contentView.subviews)
        [subview removeFromSuperview];
    
    cell.detailTextLabel.textColor = [UIColor grayColor];
    
    NSDictionary *dict = [_searchArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [dict valueForKey:@"emp_no"];
    cell.detailTextLabel.text = [dict valueForKey:@"full_name"];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([delegate respondsToSelector:@selector(searchResultSelected:)])
        [delegate searchResultSelected:[_searchArray objectAtIndex:indexPath.row]];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //if (scrollView == _tableView || scrollView == _searchTableView)
    //    [_searchBar resignFirstResponder];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView == _tableView)
        [_searchBar resignFirstResponder];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

#pragma mark - UITextFieldDelegate

- (void)keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    //NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    NSLog(@"keyboardBounds: %f, %f, %f, %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.height, keyboardBounds.size.width);
    //[self collapseMainView:keyboardBounds.size.height];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    //[self expandMainView];
    
    //[self performSearch];
    
    return YES;
}

- (void)textFieldDidChanged:(NSNotification *)notification
{
    //_searchPlaceholder.hidden = _searchBar.text.length;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId rangeOfString:GET_SUGGESTION_CALL_ID_PREFIX].length)
    {
        //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSString *key = [callId stringByReplacingOccurrencesOfString:GET_SUGGESTION_CALL_ID_PREFIX withString:@""];
            //NSLog(@"key: %@", key);
            //NSLog(@"_searchBar.text: %@", _searchBar.text);
            if ([key isEqualToString:_searchBar.text])
            {
                NSMutableArray *content = [AppUtil removeNullElementFromObject:result.result];
                _searchArray = content;
                //NSLog(@"_tabArray: %@", _tabArray);
                
                [_tableView reloadData];            }
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
