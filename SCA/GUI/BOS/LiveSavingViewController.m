//
//  LiveSavingViewController.m
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "LiveSavingViewController.h"
#import "AsyncApi.h"
#import "RulesTableViewCell.h"
#import "BOSFeedbackViewController.h"
#import "AppUtil.h"

@interface LiveSavingViewController () <AsyncApiDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate> {
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    float _titleHeight;
    
    UIView *_loadingView;
    
    UIView *_navView;
    UILabel *_titleLabel;
    UIScrollView *_mainView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    
    NSInteger _selectedRow;
    
    NSMutableDictionary *_sessionDict;
}

@end

@implementation LiveSavingViewController

static NSString* const GET_PAGE_DATA_CALL_ID = @"getpagedata";

- (id)initWithDictionary:(NSMutableDictionary *)dict
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        _sessionDict = dict;
        _selectedRow = -1;
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        //self.navigationItem.title = @"";
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
    }
    return self;
}

- (void)initPage
{
    _statusBarHeight = 0.0;
    _navBarHeight = 120.0;
    _titleHeight = 95.0;
    _tabBarHeight = 0.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_statusBarHeight-_tabBarHeight;
    
    _navView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _screenWidth, _navBarHeight)];
    _navView.backgroundColor = [UIColor appBOSBlueColor];
    [self.view addSubview:_navView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30.0, 0.0, _screenWidth-2*30.0, _navView.frame.size.height)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.text = NSLocalizedString(@"BOS live saving title", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:35.0];
    _titleLabel.numberOfLines = 2;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(_navView.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _navView.frame.size.height/2.0-_titleLabel.frame.size.height/2.0, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [_navView addSubview:_titleLabel];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(8.0, 42.0, 35.0, _navBarHeight-2*42);
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];
    
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _navBarHeight+_statusBarHeight, _screenWidth, _screenHeight)];
    _mainView.backgroundColor = [UIColor clearColor];
    _mainView.showsHorizontalScrollIndicator = NO;
    _mainView.bounces = NO;
    _mainView.contentSize = _mainView.frame.size;
    [self.view addSubview:_mainView];
    
    CGFloat nextBtnHeight = 38.0;
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0.0, _mainView.frame.size.width, _mainView.frame.size.height-2*17.0-nextBtnHeight) style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_mainView addSubview:_tableView];
    
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(20.0, _mainView.frame.size.height-17.0-nextBtnHeight, _mainView.frame.size.width-2*20.0, nextBtnHeight);
    nextBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [nextBtn setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.2] forState:UIControlStateHighlighted];
    [nextBtn addTarget:self action:@selector(nextTapped:) forControlEvents:UIControlEventTouchUpInside];
    nextBtn.backgroundColor = [UIColor appGreenColor];
    nextBtn.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:19.0];
    nextBtn.layer.cornerRadius = nextBtnHeight/2.0;
    [_mainView addSubview:nextBtn];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nextTapped:(UIButton *)sender
{
    if (_selectedRow < 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"lsr_error", nil) message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSDictionary *dict = [_dataArray objectAtIndex:_selectedRow];
    [_sessionDict setObject:dict forKey:@"LSR"];
    
    BOSFeedbackViewController *feedbackVC = [[BOSFeedbackViewController alloc] initWithDictionary:_sessionDict];
    [self.navigationController pushViewController:feedbackVC animated:YES];
}

- (void)getPageData
{
    //[self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi bosGetLSRList:GET_PAGE_DATA_CALL_ID];
}

- (UIView *)findFirstResponder
{
    for (UIView *subview in _mainView.subviews) {
        if ([subview isFirstResponder])
            return subview;
    }
    
    return nil;
}

- (void)expandMainView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight);
    
    [UIView commitAnimations];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)collapseMainView:(float)keyboardHeight
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight-keyboardHeight+_tabBarHeight);
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self getPageData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate

- (void)keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    //NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    NSLog(@"keyboardBounds: %f, %f, %f, %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.height, keyboardBounds.size.width);
    //[self collapseMainView:keyboardBounds.size.height];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    //[self expandMainView];
    
    //[self performSearch];
    
    return YES;
}

- (void)textFieldDidChanged:(NSNotification *)notification
{
    //_searchPlaceholder.hidden = _searchBar.text.length;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _dataArray? [_dataArray count]:0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    NSString *name = [dict valueForKey:@"name"];
    return [RulesTableViewCell tableView:tableView calculateRowHeightWithString:name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    RulesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[RulesTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    cell.backgroundColor = (indexPath.row%2)? [UIColor colorWithWhite:0.9 alpha:1.0]:[UIColor whiteColor];
    
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    NSString *name = [dict valueForKey:@"name"];
    NSString *imageUrl = [dict valueForKey:@"icon_url"];
    [cell tableView:tableView layoutCellWithString:name imageUrl:imageUrl];
    [cell setOn:(indexPath.row == _selectedRow)];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //RulesTableViewCell *cell = (RulesTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (_selectedRow == indexPath.row)
    {
        _selectedRow = -1;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        NSInteger _oldRow = _selectedRow;
        _selectedRow = indexPath.row;
        
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:_oldRow inSection:0];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:oldIndexPath, indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_PAGE_DATA_CALL_ID])
    {
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableArray *content = [AppUtil removeNullElementFromObject:result.result];
            _dataArray = content;
            
            if ([_sessionDict objectForKey:@"LSR"])
            {
                NSDictionary *lsrDict = [_sessionDict objectForKey:@"LSR"];
                for (NSInteger i = 0; i < [_dataArray count]; i++)
                {
                    NSDictionary *dict = [_dataArray objectAtIndex:i];
                    
                    NSInteger anId = [[dict valueForKey:@"id"] integerValue];
                    NSInteger theId = [[lsrDict valueForKey:@"id"] integerValue];
                    
                    if (anId == theId)
                        _selectedRow = i;
                }
            }
            
            [_tableView reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
