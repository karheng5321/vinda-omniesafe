//
//  ActionListViewController.m
//  FourtitudeRecipes
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import "ActionListViewController.h"
#import "AppUtil.h"
#import "DynamicTableViewCell.h"
#import "AsyncApi.h"

@interface ActionListViewController () <UITableViewDataSource, UITableViewDelegate, AsyncApiDelegate> {
    
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    float _titleHeight;
    
    UIView *_loadingView;
    
    UIView *_navView;
    UILabel *_titleLabel;
    UIScrollView *_mainView;
    
    NSMutableArray *_dataArray;
    
    UITableView *_tableView;
    
    NSString *_string;
    NSInteger _selectedIndex;
}

@end

@implementation ActionListViewController

@synthesize delegate;

static NSString* const GET_PAGE_DATA_CALL_ID = @"getpagedata";

- (id)initWithString:(NSString *)string
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        _string = string;
        _selectedIndex = -1;
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        self.title = @"";
        self.navigationItem.title = @"Job Type";
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTapped:)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTapped:)];
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
        
        [self getPageData];
    }
    return self;
}

- (void)initPage
{
    _statusBarHeight = 0.0;
    _navBarHeight = 120.0;
    _titleHeight = 95.0;
    _tabBarHeight = 0.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_statusBarHeight-_tabBarHeight;
    
    _navView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _screenWidth, _navBarHeight)];
    _navView.backgroundColor = [UIColor appBOSBlueColor];
    [self.view addSubview:_navView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30.0, 0.0, _screenWidth-2*30.0, _navView.frame.size.height)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.text = NSLocalizedString(@"take_action", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:35.0];
    _titleLabel.numberOfLines = 2;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(_navView.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _navView.frame.size.height/2.0-_titleLabel.frame.size.height/2.0, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [_navView addSubview:_titleLabel];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(8.0, 42.0, 35.0, _navBarHeight-2*42);
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];

    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_tableView];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)backTapped:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)doneTapped:(UIBarButtonItem *)sender
{
    if ([delegate respondsToSelector:@selector(viewController:menuSelected:)])
    {
        NSMutableArray *array = (_selectedIndex < 0)? [[NSMutableArray alloc] init]:[NSMutableArray arrayWithObject:[_dataArray objectAtIndex:_selectedIndex]];
        [delegate viewController:self menuSelected:array];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)getPageData
{
    [self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi bosActionList:GET_PAGE_DATA_CALL_ID];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray? [_dataArray count]:0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    NSString *name = [dict valueForKey:@"text"];
    return [DynamicTableViewCell tableView:tableView calculateRowHeightWithString:name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Call";
    DynamicTableViewCell *cell = (DynamicTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[DynamicTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    NSString *name = [dict valueForKey:@"text"];
    
    [cell tableView:tableView layoutCellWithString:name];
    cell.textLabel.textColor = [UIColor blackColor];
    
    cell.accessoryType = (_selectedIndex == indexPath.row)? UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}
/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade]
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}
*/
/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    /*if (indexPath.row == _selectedIndex)
        _selectedIndex = -1;
    else
        _selectedIndex = indexPath.row;*/
    
    _selectedIndex = indexPath.row;
    
    [_tableView reloadData];
    
    if ([delegate respondsToSelector:@selector(viewController:menuSelected:)])
        [delegate viewController:self menuSelected:[NSMutableArray arrayWithObject:[_dataArray objectAtIndex:_selectedIndex]]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_PAGE_DATA_CALL_ID])
    {
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableArray *content = [AppUtil removeNullElementFromObject:result.result];
            _dataArray = content;
            
            NSDictionary *defaultDict = [NSDictionary dictionaryWithObject:NSLocalizedString(@"others", nil) forKey:@"text"];
            [_dataArray addObject:defaultDict];
            
            _selectedIndex = -1;
            for (NSInteger i = 0; i < [_dataArray count]; i++)
            {
                NSDictionary *dict = [_dataArray objectAtIndex:i];
                NSString *name = [dict valueForKey:@"text"];
                
                if ([name isEqualToString:_string])
                {
                    _selectedIndex = i;
                    break;
                }
            }
            
            [_tableView reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
