//
//  ActionListViewController.h
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionListViewControllerDelegate <NSObject>

@optional

- (void)viewController:(UIViewController *)vc menuSelected:(NSMutableArray *)array;

@end

@interface ActionListViewController : UIViewController

@property (nonatomic, retain) id <ActionListViewControllerDelegate> delegate;

- (id)initWithString:(NSString *)string;

@end
