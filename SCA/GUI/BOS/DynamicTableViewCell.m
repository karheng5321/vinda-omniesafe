//
//  DynamicTableViewCell.m
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import "DynamicTableViewCell.h"

@interface DynamicTableViewCell () {
	UILabel *_textLabel;
}

@end

@implementation DynamicTableViewCell

@synthesize textLabel = _textLabel;

static float const MARGIN = 15.0;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		
        _textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textColor = [UIColor blackColor];
        _textLabel.text = @"";
        _textLabel.textAlignment = NSTextAlignmentLeft;
        _textLabel.font = [UIFont systemFontOfSize:14.0];
        _textLabel.numberOfLines = 0;
        [self.contentView addSubview:_textLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightWithString:(NSString *)string
{
    CGFloat textMaxWidth = tableView.frame.size.width-MARGIN-3*MARGIN;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont systemFontOfSize:14.0], NSFontAttributeName,
                                 nil];
    CGRect textRect = [string boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    return MARGIN+textRect.size.height+MARGIN;
}

- (void)tableView:(UITableView *)tableView layoutCellWithString:(NSString *)string
{
    CGFloat textMaxWidth = tableView.frame.size.width-MARGIN-3*MARGIN;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont systemFontOfSize:14.0], NSFontAttributeName,
                                  nil];
    CGRect textRect = [string boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    _textLabel.frame = CGRectMake(MARGIN, MARGIN, textRect.size.width, textRect.size.height);
    _textLabel.text = string;
}

@end
