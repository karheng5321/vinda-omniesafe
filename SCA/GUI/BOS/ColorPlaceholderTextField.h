//
//  ColorPlaceholderTextField.h
//
//  Created by Siti Norain Ishak on 1/22/15.
//

#import <UIKit/UIKit.h>

@interface ColorPlaceholderTextField : UITextField {
    UIColor *_placeholderColor;
}

@property (nonatomic, retain) UIColor *placeholderColor;

- (void)setPlaceholderColor:(UIColor *)placeholderColor;

@end
