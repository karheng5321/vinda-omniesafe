//
//  BOSReportListViewController.m
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BOSReportListViewController.h"
#import "AsyncApi.h"
#import "ListingTableViewCell.h"
#import "BOSDetailsViewController.h"
#import "AppUtil.h"

@interface BOSReportListViewController () <AsyncApiDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate> {
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    float _titleHeight;
    
    UIView *_loadingView;
    
    UIView *_navView;
    UILabel *_titleLabel;
    UIScrollView *_mainView;
    UISegmentedControl *_segmentControl;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    
    NSString *_type;
    NSMutableDictionary *_dataDict;
}

@end

@implementation BOSReportListViewController

static NSString* const GET_PAGE_DATA_CALL_ID = @"getpagedata";

- (id)initWithType:(NSString *)type
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        _type = type;
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        //self.navigationItem.title = @"";
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
    }
    return self;
}

- (void)initPage
{
    _statusBarHeight = 0.0;
    _navBarHeight = 120.0;
    _titleHeight = 95.0;
    _tabBarHeight = 0.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_statusBarHeight-_tabBarHeight;
    
    _navView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _screenWidth, _navBarHeight)];
    _navView.backgroundColor = [UIColor appBOSBlueColor];
    [self.view addSubview:_navView];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30.0, 0.0, _screenWidth-2*30.0, _navView.frame.size.height)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.text = ([_type isEqualToString:@"own"])? NSLocalizedString(@"BOS history title", nil): isAdmin? NSLocalizedString(@"all_history", nil):NSLocalizedString(@"BOS department title", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:35.0];
    _titleLabel.numberOfLines = 2;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(_navView.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _navView.frame.size.height/2.0-_titleLabel.frame.size.height/2.0, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [_navView addSubview:_titleLabel];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(8.0, 42.0, 35.0, _navBarHeight-2*42);
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];
    
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _navBarHeight+_statusBarHeight, _screenWidth, _screenHeight)];
    _mainView.backgroundColor = [UIColor clearColor];
    _mainView.showsHorizontalScrollIndicator = NO;
    _mainView.bounces = NO;
    _mainView.contentSize = _mainView.frame.size;
    [self.view addSubview:_mainView];
    
    CGFloat currentHeight = 0.0;
    
    if ([_type isEqualToString:@"all"] && isAdmin)
    {
        currentHeight += 5.0;
        
        _segmentControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:[NSLocalizedString(@"all_department", nil) uppercaseString], [NSLocalizedString(@"my_department", nil) uppercaseString], nil]];
        _segmentControl.frame = CGRectMake(5.0, currentHeight, _mainView.frame.size.width-10.0, 40.0);
        _segmentControl.tintColor = _navView.backgroundColor;
        _segmentControl.momentary = NO;
        _segmentControl.selectedSegmentIndex = 0;
        [_segmentControl addTarget:self action:@selector(segmentedControlChanged:) forControlEvents:UIControlEventValueChanged];
        [_mainView addSubview:_segmentControl];
        
        currentHeight += _segmentControl.frame.size.height+5.0;
    }
    
    UIView *columnHeader = [[UIView alloc] initWithFrame:CGRectMake(0.0, currentHeight, _mainView.frame.size.width, 40.0)];
    [_mainView addSubview:columnHeader];
    
    CGFloat col1Width = (columnHeader.frame.size.width-4*10.0)*0.3;
    CGFloat col2Width = (columnHeader.frame.size.width-4*10.0)*0.3;
    CGFloat col3Width = (columnHeader.frame.size.width-4*10.0)*0.4;
    
    UILabel *column1 = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, col1Width, columnHeader.frame.size.height)];
    column1.backgroundColor = [UIColor clearColor];
    column1.textColor = [UIColor blackColor];
    column1.text = NSLocalizedString(@"date", nil);
    column1.textAlignment = NSTextAlignmentLeft;
    column1.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    column1.numberOfLines = 2;
    column1.lineBreakMode = NSLineBreakByWordWrapping;
    [columnHeader addSubview:column1];
    
    UILabel *column2 = [[UILabel alloc] initWithFrame:CGRectMake(10.0+col1Width+10.0, 0.0, col2Width, columnHeader.frame.size.height)];
    column2.backgroundColor = [UIColor clearColor];
    column2.textColor = [UIColor blackColor];
    column2.text = NSLocalizedString(@"report_no", nil);
    column2.textAlignment = NSTextAlignmentLeft;
    column2.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    column2.numberOfLines = 2;
    column2.lineBreakMode = NSLineBreakByWordWrapping;
    [columnHeader addSubview:column2];
    
    UILabel *column3 = [[UILabel alloc] initWithFrame:CGRectMake(10.0+col1Width+10.0+col2Width+10.0, 0.0, col3Width, columnHeader.frame.size.height)];
    column3.backgroundColor = [UIColor clearColor];
    column3.textColor = [UIColor blackColor];
    column3.text = NSLocalizedString(@"lsr_type", nil);
    column3.textAlignment = NSTextAlignmentLeft;
    column3.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    column3.numberOfLines = 2;
    column3.lineBreakMode = NSLineBreakByWordWrapping;
    [columnHeader addSubview:column3];
    
    currentHeight += columnHeader.frame.size.height;
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, currentHeight, _mainView.frame.size.width, _mainView.frame.size.height-currentHeight) style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_mainView addSubview:_tableView];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)segmentedControlChanged:(UISegmentedControl *)sender
{
    _dataArray = (_segmentControl.selectedSegmentIndex == 0)? [_dataDict objectForKey:@"admin"]:[_dataDict objectForKey:@"supervisor"];
    [_tableView reloadData];
}

- (void)getPageData
{
    [self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    
    if ([_type isEqualToString:@"own"])
        [asyncApi bosUserReportHistory:GET_PAGE_DATA_CALL_ID];
    else
    {
        [asyncApi bosAllReportHistory:GET_PAGE_DATA_CALL_ID];
    }
}

- (UIView *)findFirstResponder
{
    for (UIView *subview in _mainView.subviews) {
        if ([subview isFirstResponder])
            return subview;
    }
    
    return nil;
}

- (void)expandMainView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight);
    
    [UIView commitAnimations];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)collapseMainView:(float)keyboardHeight
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight-keyboardHeight+_tabBarHeight);
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self getPageData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate

- (void)keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    //NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    NSLog(@"keyboardBounds: %f, %f, %f, %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.height, keyboardBounds.size.width);
    //[self collapseMainView:keyboardBounds.size.height];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    //[self expandMainView];
    
    //[self performSearch];
    
    return YES;
}

- (void)textFieldDidChanged:(NSNotification *)notification
{
    //_searchPlaceholder.hidden = _searchBar.text.length;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _dataArray? [_dataArray count]:0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    return [ListingTableViewCell tableView:tableView calculateRowHeightWithDictionary:dict];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ListingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ListingTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    cell.backgroundColor = (indexPath.row%2)? [UIColor colorWithWhite:0.8 alpha:1.0]:[UIColor colorWithWhite:0.9 alpha:1.0];
    
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    [cell tableView:tableView layoutCellWithDictionary:dict];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = [_dataArray objectAtIndex:indexPath.row];
    NSInteger ID = [[dict valueForKey: API_REPORT_ID_KEY] integerValue];
    
    BOSDetailsViewController *detailsVC = [[BOSDetailsViewController alloc] initWithId:ID fromNotification:[_type isEqualToString:@"own"] sameSite:NO];
    [self.navigationController pushViewController:detailsVC animated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_PAGE_DATA_CALL_ID])
    {
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            if ([_type isEqualToString:@"own"])
            {
                NSMutableArray *content = [AppUtil removeNullElementFromObject:result.result];
                _dataArray = content;
            }
            else
            {
                NSMutableDictionary *content = [AppUtil removeNullElementFromObject:result.result];
                _dataDict = content;
                if (_segmentControl)
                {
                    _dataArray = (_segmentControl.selectedSegmentIndex == 0)? [_dataDict objectForKey:@"admin"]:[_dataDict objectForKey:@"supervisor"];
                }
                else
                {
                    _dataArray = [_dataDict objectForKey:@"supervisor"];
                }
                
                //_dataArray = [_dataDict objectForKey:@"admin"];
            }
            
            [_tableView reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
