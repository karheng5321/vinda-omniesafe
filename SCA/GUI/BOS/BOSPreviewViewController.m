//
//  BOSPreviewViewController.m
//  SCA
//
//  Created by Siti on 8/5/15.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BOSPreviewViewController.h"
#import "AsyncApi.h"
#import "BOSSummaryTableViewCell.h"
#import "RulesTableViewCell.h"
#import "AppUtil.h"
#import "AppDelegate.h"

@interface BOSPreviewViewController () <AsyncApiDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate> {
    float _screenWidth;
    float _screenHeight;
    float _navBarHeight;
    float _tabBarHeight;
    float _statusBarHeight;
    float _titleHeight;
    
    UIView *_loadingView;
    
    UIView *_navView;
    UILabel *_titleLabel;
    UIScrollView *_mainView;
    UITableView *_tableView;
    NSMutableArray *_dataArray;
    
    NSString *_dateString;
    //UITextField *_empNoField;
    NSString *_empNoString;
    NSString *_empNameString;
    NSString *_empDeptString;
    NSString *_empPosString;
    NSString *_superNoString;
    NSString *_superNameString;
    NSString *_superDeptString;
    
    NSMutableDictionary *_sessionDict;
}

@end

@implementation BOSPreviewViewController

static NSString* const SUBMIT_REPORT_CALL_ID = @"SUBMIT_REPORT_CALL_ID";

- (id)initWithDictionary:(NSMutableDictionary *)dict
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        _sessionDict = dict;
        NSLog(@"_sessionDict: %@", _sessionDict);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        _dateString = [dateFormatter stringFromDate:[NSDate date]];
        
        _empNoString = [_sessionDict valueForKey:@"emp_no"];
        _empNameString = [_sessionDict valueForKey:@"full_name"];
        _empDeptString = [_sessionDict valueForKey:@"department"];
        _empPosString = [_sessionDict valueForKey:@"designation"];
        if ([[_sessionDict objectForKey:@"supervisor"] isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *supervisor = [_sessionDict objectForKey:@"supervisor"];
            _superNoString = [supervisor valueForKey:@"emp_no"];
            _superNameString = [supervisor valueForKey:@"full_name"];
            _superDeptString = [supervisor valueForKey:@"department_name"];
        }
        else
        {
            _superNoString = @"";
            _superNameString = @"";
            _superDeptString = @"";
        }
        
        self.view.backgroundColor = [UIColor whiteColor];
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
        //self.navigationItem.hidesBackButton = YES;
        
        //self.navigationItem.title = @"";
        
        [self initPage];
        
        //UITapGestureRecognizer *singleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        //[self.view addGestureRecognizer:singleTapped];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _loadingView.center = CGPointMake(_screenWidth/2.0, _screenHeight/2.0+_statusBarHeight+_navBarHeight);
        _loadingView.layer.cornerRadius = 10.0;
        _loadingView.hidden = YES;
        [self.view addSubview:_loadingView];
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityIndicator.hidesWhenStopped = YES;
        activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
        [_loadingView addSubview:activityIndicator];
        [activityIndicator startAnimating];
    }
    return self;
}

- (void)initPage
{
    _statusBarHeight = 0.0;
    _navBarHeight = 120.0;
    _titleHeight = 95.0;
    _tabBarHeight = 0.0;
    _screenWidth = self.view.bounds.size.width;
    _screenHeight = self.view.bounds.size.height-_navBarHeight-_statusBarHeight-_tabBarHeight;
    
    _navView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _screenWidth, _navBarHeight)];
    _navView.backgroundColor = [UIColor appBOSBlueColor];
    [self.view addSubview:_navView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, _screenWidth-2*10.0, _navView.frame.size.height)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.text = NSLocalizedString(@"BOS preview title", nil);
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:35.0];
    _titleLabel.numberOfLines = 2;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_titleLabel sizeToFit];
    _titleLabel.frame = CGRectMake(_navView.frame.size.width/2.0-_titleLabel.frame.size.width/2.0, _navView.frame.size.height/2.0-_titleLabel.frame.size.height/2.0, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    [_navView addSubview:_titleLabel];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(8.0, 42.0, 35.0, _navBarHeight-2*42);
    backBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:backBtn];
    
    UILabel *descLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, _navView.frame.size.height-30.0, _screenWidth-2*15.0, 30.0)];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.textColor = [UIColor whiteColor];
    descLabel.text = NSLocalizedString(@"preview_description", nil);
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
    descLabel.numberOfLines = 1;
    descLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_navView addSubview:descLabel];
    
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 40.0, 50.0)];
    btnView.backgroundColor = [UIColor clearColor];
    btnView.opaque = NO;
    
    UIImage *iconImage = [UIImage imageNamed:@"ic_save_white.png"];
    float scale = iconImage.size.height/30.0;
    float iconWidth = iconImage.size.width/scale;
    float iconHeight = iconImage.size.height/scale;
    
    UIImageView *btnIcon = [[UIImageView alloc] initWithFrame:CGRectMake(btnView.frame.size.width/2.0-iconWidth/2.0, 0.0, iconWidth, iconHeight)];
    btnIcon.image = iconImage;
    [btnView addSubview:btnIcon];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 30.0, btnView.frame.size.width, 20.0)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.text = @"SAVE";
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    [btnView addSubview:label];
    
    UIImage *saveImage = [AppUtil imageFromUIView:btnView];
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(_navView.frame.size.width-10.0-40.0, _navView.frame.size.height/2.0-50.0/2.0, 40.0, 50.0);
    saveBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [saveBtn setImage:saveImage forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_navView addSubview:saveBtn];
    
    _mainView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _navBarHeight+_statusBarHeight, _screenWidth, _screenHeight)];
    _mainView.backgroundColor = [UIColor clearColor];
    _mainView.showsHorizontalScrollIndicator = NO;
    _mainView.bounces = NO;
    _mainView.contentSize = _mainView.frame.size;
    [self.view addSubview:_mainView];
    
    /*CGFloat valueMaxWidth = _mainView.frame.size.width-15.0-10.0-10.0-3.0-MAX(80.0, _mainView.frame.size.width/4.0);
    _empNoField = [[UITextField alloc] initWithFrame:CGRectMake(0.0, 0.0, valueMaxWidth, 30.0)];
    _empNoField.text = @"";
    _empNoField.placeholder = @"";
    _empNoField.delegate = self;
    _empNoField.borderStyle = UITextBorderStyleRoundedRect;
    _empNoField.textColor = [UIColor blackColor];
    _empNoField.font = [UIFont systemFontOfSize:14];
    _empNoField.keyboardType = UIKeyboardTypeDefault;
    _empNoField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _empNoField.textAlignment = NSTextAlignmentLeft;
    _empNoField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    _empNoField.returnKeyType = UIReturnKeyDone;*/
    
    CGFloat nextBtnHeight = 38.0;
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0.0, _mainView.frame.size.width, _mainView.frame.size.height-2*17.0-nextBtnHeight) style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.allowsSelection = NO;
    [_mainView addSubview:_tableView];
    
    NSString *text1 = [NSString stringWithFormat:@"%@:", [NSLocalizedString(@"feedback details", nil) uppercaseString]];
    NSString *descStg = [NSString stringWithFormat:@"%@", text1];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:descStg];
    [attrText addAttribute:NSFontAttributeName
                     value:[UIFont fontWithName:@"Roboto-Medium" size:14.0]
                     range:NSMakeRange(0, text1.length)];
    CGRect descRect = [attrText boundingRectWithSize:CGSizeMake(_tableView.frame.size.width-2*20.0, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil];
    
    NSString *text2 = [_sessionDict valueForKey:@"remark"];
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont fontWithName:@"Roboto-Regular" size:14.0], NSFontAttributeName,
                                  nil];
    CGRect textRect = [text2 boundingRectWithSize:CGSizeMake(_tableView.frame.size.width-2*20.0, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, _tableView.frame.size.width, descRect.size.height+textRect.size.height+50.0)];
    _tableView.tableFooterView = footerView;
    
    UIView *detailsBox = [[UIView alloc] initWithFrame:CGRectMake(10.0, 10.0, footerView.frame.size.width-2*10.0, footerView.frame.size.height-20.0)];
    detailsBox.layer.cornerRadius = 10.0;
    detailsBox.layer.borderColor = [[UIColor blackColor] CGColor];
    detailsBox.layer.borderWidth = 1.0;
    [footerView addSubview:detailsBox];
    
    UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0, descRect.size.width, descRect.size.height)];
    tagLabel.backgroundColor = [UIColor clearColor];
    tagLabel.textColor = [UIColor blackColor];
    tagLabel.attributedText = attrText;
    tagLabel.textAlignment = NSTextAlignmentLeft;
    tagLabel.lineBreakMode = NSLineBreakByWordWrapping;
    tagLabel.numberOfLines = 0;
    [detailsBox addSubview:tagLabel];
    
    UILabel *detailsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0+descRect.size.height+10.0, textRect.size.width, textRect.size.height)];
    detailsLabel.backgroundColor = [UIColor clearColor];
    detailsLabel.textColor = [UIColor blackColor];
    detailsLabel.text = text2;
    detailsLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14.0];
    detailsLabel.textAlignment = NSTextAlignmentLeft;
    detailsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    detailsLabel.numberOfLines = 0;
    [detailsBox addSubview:detailsLabel];
    
    UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nextBtn.frame = CGRectMake(20.0, _mainView.frame.size.height-17.0-nextBtnHeight, _mainView.frame.size.width-2*20.0, nextBtnHeight);
    nextBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [nextBtn setTitle:NSLocalizedString(@"submit", nil) forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.2] forState:UIControlStateHighlighted];
    [nextBtn addTarget:self action:@selector(nextTapped:) forControlEvents:UIControlEventTouchUpInside];
    nextBtn.backgroundColor = [UIColor appGreenColor];
    nextBtn.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:19.0];
    nextBtn.layer.cornerRadius = nextBtnHeight/2.0;
    [_mainView addSubview:nextBtn];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nextTapped:(UIButton *)sender
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDel isInternetAvailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"no_internet", nil)
                                                        message:NSLocalizedString(@"retry", nil)
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [self setLoadingHidden:NO];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    NSString *staffId = [NSString stringWithFormat:@"%@", [_sessionDict objectForKey:USER_ID_KEY]];
    
    NSDictionary *dict = [_sessionDict objectForKey:@"LSR"];
    NSString *lsrId = [NSString stringWithFormat:@"%@", [dict objectForKey:@"id"]];
    
    NSString *behaviour = [_sessionDict valueForKey:@"behaviour"];
    NSString *remark = [_sessionDict valueForKey:@"remark"];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi user:userId reportBos:staffId lsr:lsrId behaviour:behaviour remark:remark :SUBMIT_REPORT_CALL_ID];
}

- (void)saveTapped:(UIButton *)sender
{
    //NSString *timestampString = [dict valueForKey:@"reported_timestamp"];
    //NSLog(@"timestampString: %@", timestampString);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *timestampString = [dateFormatter stringFromDate:[NSDate date]];
    [_sessionDict setValue:timestampString forKey:@"reported_timestamp"];
    
    NSString *uuid = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, CFUUIDCreate(NULL)));
    [_sessionDict setValue:uuid forKey:@"cached_id"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    
    NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_BOS] mutableCopy];
    if (!reportDict)
        reportDict = [NSMutableDictionary new];
    NSMutableArray *reportArray = [[reportDict objectForKey:userId] mutableCopy];
    if (!reportArray)
        reportArray = [NSMutableArray new];
    
    [reportArray addObject:_sessionDict];
    [reportDict setObject:reportArray forKey:userId];
    [defaults setObject:reportDict forKey:NSUserDefaultKey_SAVED_BOS];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"report_saved", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }];
    
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (UIView *)findFirstResponder
{
    for (UIView *subview in _mainView.subviews) {
        if ([subview isFirstResponder])
            return subview;
    }
    
    return nil;
}

- (void)expandMainView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight);
    
    [UIView commitAnimations];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)collapseMainView:(float)keyboardHeight
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _mainView.frame = CGRectMake(0, _statusBarHeight+_navBarHeight, _screenWidth, _screenHeight-keyboardHeight+_tabBarHeight);
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate

- (void)keyboardWillShow:(NSNotification *)note
{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    //NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    //NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    NSLog(@"keyboardBounds: %f, %f, %f, %f", keyboardBounds.origin.x, keyboardBounds.origin.y, keyboardBounds.size.height, keyboardBounds.size.width);
    //[self collapseMainView:keyboardBounds.size.height];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    //[self expandMainView];
    
    //[self performSearch];
    
    return YES;
}

- (void)textFieldDidChanged:(NSNotification *)notification
{
    //_searchPlaceholder.hidden = _searchBar.text.length;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 5;
            break;
        case 2:
            return 4;
            break;
        case 3:
            return 1;
            break;
        case 4:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:[NSLocalizedString(@"date", nil) uppercaseString] andValue:_dateString type:BOSSummaryTypeDate];
        }
            break;
        case 1:
        {
            if (indexPath.row == 0)
                return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:[NSLocalizedString(@"feedback to", nil) uppercaseString] andValue:@"" type:BOSSummaryTypeHeader];
            else
            {
                NSString *field = @"";
                NSString *value = @"";
                switch (indexPath.row) {
                    case 1:
                        field = NSLocalizedString(@"emp_no", nil);
                        value = _empNoString;
                        return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:BOSSummaryTypeListing];
                        break;
                    case 2:
                        field = NSLocalizedString(@"name", nil);
                        value = _empNameString;
                        return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:BOSSummaryTypeListing];
                        break;
                    case 3:
                        field = NSLocalizedString(@"department", nil);
                        value = _empDeptString;
                        return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:BOSSummaryTypeListing];
                        break;
                    case 4:
                        field = NSLocalizedString(@"designation", nil);
                        value = _empPosString;
                        return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:BOSSummaryTypeListing];
                        break;
                    default:
                        return 0.0;
                        break;
                }
            }
        }
            break;
        case 2:
        {
            if (indexPath.row == 0)
                return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:[NSLocalizedString(@"immediate supervisor", nil) uppercaseString] andValue:@"" type:BOSSummaryTypeHeader];
            else
            {
                NSString *field = @"";
                NSString *value = @"";
                switch (indexPath.row) {
                    case 1:
                        field = NSLocalizedString(@"emp_no", nil);
                        value = _superNoString;
                        return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:BOSSummaryTypeListing];
                        break;
                    case 2:
                        field = NSLocalizedString(@"name", nil);
                        value = _superNameString;
                        return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:BOSSummaryTypeListing];
                        break;
                    case 3:
                        field = NSLocalizedString(@"department", nil);
                        value = _superDeptString;
                        return [BOSSummaryTableViewCell tableView:tableView calculateRowHeightForField:field andValue:value type:BOSSummaryTypeListing];
                        break;
                    default:
                        return 0.0;
                        break;
                }
            }
        }
            break;
        case 3:
        {
            return [RulesTableViewCell tableView:tableView calculateConditionRowHeightWithString:@"Follow all safety rules. Nothing is worth getting hurt for."];
        }
            break;
        case 4:
        {
            return [RulesTableViewCell tableView:tableView calculateConditionRowHeightWithString:@"UNSAFE"];
        }
            break;
        default:
            return 0.0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell_%ld_%ld", indexPath.section, indexPath.row];
    if (indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2)
    {
        BOSSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[BOSSummaryTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        // Configure the cell...
        
        cell.accessoryView = nil;
        
        switch (indexPath.section) {
            case 0:
            {
                [cell tableView:tableView layoutCellWithForField:[NSLocalizedString(@"date", nil) uppercaseString] andValue:_dateString type:BOSSummaryTypeDate];
            }
                break;
            case 1:
            {
                if (indexPath.row == 0)
                    [cell tableView:tableView layoutCellWithForField:[NSLocalizedString(@"feedback to", nil) uppercaseString] andValue:@"" type:BOSSummaryTypeHeader];
                else
                {
                    NSString *field = @"";
                    NSString *value = @"";
                    switch (indexPath.row) {
                        case 1:
                            field = NSLocalizedString(@"emp_no", nil);
                            value = _empNoString;
                            [cell tableView:tableView layoutCellWithForField:field andValue:value type:BOSSummaryTypeListing];
                            break;
                        case 2:
                            field = NSLocalizedString(@"name", nil);
                            value = _empNameString;
                            [cell tableView:tableView layoutCellWithForField:field andValue:value type:BOSSummaryTypeListing];
                            break;
                        case 3:
                            field = NSLocalizedString(@"department", nil);
                            value = _empDeptString;
                            [cell tableView:tableView layoutCellWithForField:field andValue:value type:BOSSummaryTypeListing];
                            break;
                        case 4:
                            field = NSLocalizedString(@"designation", nil);
                            value = _empPosString;
                            [cell tableView:tableView layoutCellWithForField:field andValue:value type:BOSSummaryTypeListing];
                            break;
                        default:
                            break;
                    }
                }
            }
                break;
            case 2:
            {
                if (indexPath.row == 0)
                    [cell tableView:tableView layoutCellWithForField:[NSLocalizedString(@"immediate supervisor", nil) uppercaseString] andValue:@"" type:BOSSummaryTypeHeader];
                else
                {
                    NSString *field = @"";
                    NSString *value = @"";
                    switch (indexPath.row) {
                        case 1:
                            field = NSLocalizedString(@"emp_no", nil);
                            value = _superNoString;
                            [cell tableView:tableView layoutCellWithForField:field andValue:value type:BOSSummaryTypeListing];
                            break;
                        case 2:
                            field = NSLocalizedString(@"name", nil);
                            value = _superNameString;
                            [cell tableView:tableView layoutCellWithForField:field andValue:value type:BOSSummaryTypeListing];
                            break;
                        case 3:
                            field = NSLocalizedString(@"department", nil);
                            value = _superDeptString;
                            [cell tableView:tableView layoutCellWithForField:field andValue:value type:BOSSummaryTypeListing];
                            break;
                        default:
                            break;
                    }
                }
            }
                break;
            default:
                break;
        }
        
        return cell;
    }
    else
    {
        RulesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[RulesTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        // Configure the cell...
        
        cell.backgroundColor = [UIColor whiteColor];
        
        if (indexPath.section == 3)
        {
            NSDictionary *dict = [_sessionDict objectForKey:@"LSR"];
            NSData *imageData = [AppUtil dataFromUrl:[dict valueForKey:@"icon_url"]];
            UIImage *image = [UIImage imageWithData:imageData];
            NSString *text = [dict valueForKey:@"name"];
            [cell tableView:tableView layoutConditionCellWithString:text icon:image];
        }
        else
        {
            NSString *behaviour = [[_sessionDict valueForKey:@"behaviour"] uppercaseString];
            UIImage *image = ([behaviour isEqualToString:@"UNSAFE"])? [UIImage imageNamed:@"bos_icon_unsafe.png"]:[UIImage imageNamed:@"bos_icon_safe.png"];
            NSString *text = behaviour;
            [cell tableView:tableView layoutConditionCellWithString:text icon:image];
        }
        
        return cell;
    }
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:SUBMIT_REPORT_CALL_ID])
    {
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
            NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
            
            NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_BOS] mutableCopy];
            if (!reportDict)
                reportDict = [NSMutableDictionary new];
            NSMutableArray *dataArray = [[reportDict objectForKey:userId] mutableCopy];
            if (!dataArray)
                dataArray = [NSMutableArray new];
            
            for (NSInteger i = 0; i < [dataArray count]; i++)
            {
                NSDictionary *dict = [dataArray objectAtIndex:i];
                NSString *ID = [dict valueForKey:@"cached_id"];
                NSString *theID = [_sessionDict valueForKey:@"cached_id"];
                
                if ([ID isEqualToString:theID])
                {
                    [dataArray removeObjectAtIndex:i];
                    break;
                }
            }
            
            [reportDict setObject:dataArray forKey:userId];
            [defaults setObject:reportDict forKey:NSUserDefaultKey_SAVED_BOS];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];

            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
