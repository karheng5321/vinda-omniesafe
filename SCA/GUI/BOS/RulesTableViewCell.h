//
//  RulesTableViewCell.h
//  SCA
//
//  Created by Siti on 12/23/16.
//  Copyright © 2016 Siti Norain Ishak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RulesTableViewCell : UITableViewCell {
	
}

@property (nonatomic, readonly) UILabel *textLabel;
@property (nonatomic, readwrite) BOOL on;

+ (CGFloat)tableView:(UITableView *)tableView calculateRowHeightWithString:(NSString *)string;
- (void)tableView:(UITableView *)tableView layoutCellWithString:(NSString *)string imageUrl:(NSString *)imageUrl;

+ (CGFloat)tableView:(UITableView *)tableView calculateConditionRowHeightWithString:(NSString *)string;
- (void)tableView:(UITableView *)tableView layoutConditionCellWithString:(NSString *)string icon:(UIImage *)image;

@end
