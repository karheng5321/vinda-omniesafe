//
//  LoginViewController.m
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "LoginViewController.h"
#import "GeneralHelper.h"
#import "AsyncApi.h"
#import "AppUtil.h"
#import "TSLanguageManager.h"
#import "Language.h"
#import "SelectSiteViewController.h"
#import "AppDelegate.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface LoginViewController ()<UITextFieldDelegate, AsyncApiDelegate> {
    UIView *_loadingView;
    
    UILabel *_versionLabel;
}

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextView;
@property (weak, nonatomic) IBOutlet UIButton *remembermeButton;
@property (weak, nonatomic) IBOutlet UIButton *enButton;
@property (weak, nonatomic) IBOutlet UIButton *bmButton;
@property (weak, nonatomic) IBOutlet UIButton *znButton;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *usernameTFTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rememberTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *bgMoveView;
@property (weak, nonatomic) IBOutlet UILabel *copyrightLabel;

@end

@implementation LoginViewController

static NSString * const LOGIN_USER_CALL_ID = @"loginuser";

- (void) layoutSetup
{
    [self.usernameTextView setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordTextView setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    /*self.enButton.backgroundColor = [UIColor darkGrayColor];
    [self.enButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.bmButton.backgroundColor = [UIColor clearColor];
    [self.bmButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];*/
    
    [self.znButton setTitle:@"中" forState:UIControlStateNormal];
    
    [NSBundle setLanguage:[[NSUserDefaults standardUserDefaults] objectForKey:APP_LANGUAGE]];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:APP_LANGUAGE] isEqualToString:kLMEnglish])
    {
        self.enButton.backgroundColor = [UIColor darkGrayColor];
        [self.enButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.bmButton.backgroundColor = [UIColor clearColor];
        [self.bmButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        self.znButton.backgroundColor = [UIColor clearColor];
        [self.znButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:APP_LANGUAGE] isEqualToString:kLMMalay])
    {
        self.bmButton.backgroundColor = [UIColor darkGrayColor];
        [self.bmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.enButton.backgroundColor = [UIColor clearColor];
        [self.enButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        self.znButton.backgroundColor = [UIColor clearColor];
        [self.znButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:APP_LANGUAGE] isEqualToString:kLMChinese])
    {
        self.znButton.backgroundColor = [UIColor darkGrayColor];
        [self.znButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.bmButton.backgroundColor = [UIColor clearColor];
        [self.bmButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        self.enButton.backgroundColor = [UIColor clearColor];
        [self.enButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    self.bgMoveView.backgroundColor = [UIColor clearColor];
    
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.logoTopConstraint.constant = 25;
        self.usernameTFTopConstraint.constant = 15;
        self.rememberTopConstraint.constant = 10;
    }
    else if ([GeneralHelper isDeviceiPhone6plus])
    {
        self.usernameTFTopConstraint.constant = 35;
        self.rememberTopConstraint.constant = 25;
    }
    // Logo Image Size
    self.logoWidthConstraint.constant = [UIScreen mainScreen].bounds.size.width-135;
    self.logoHeightConstraint.constant = (self.logoWidthConstraint.constant*234)/240;
    
    self.enButton.layer.borderWidth = 1.0f;
    self.enButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.enButton.layer.cornerRadius = self.enButton.frame.size.width/2;
    self.enButton.layer.masksToBounds = YES;
    
    self.bmButton.layer.borderWidth = 1.0f;
    self.bmButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.bmButton.layer.cornerRadius = self.bmButton.frame.size.width/2;
    self.bmButton.layer.masksToBounds = YES;
    
    self.znButton.layer.borderWidth = 1.0f;
    self.znButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.znButton.layer.cornerRadius = self.znButton.frame.size.width/2;
    self.znButton.layer.masksToBounds = YES;
    
    UIView *paddingView_left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
    UIView *paddingView_right = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
    self.usernameTextView.leftView = paddingView_left;
    self.usernameTextView.leftViewMode = UITextFieldViewModeAlways;
    self.usernameTextView.rightView = paddingView_right;
    self.usernameTextView.rightViewMode = UITextFieldViewModeAlways;
    self.usernameTextView.backgroundColor = [UIColor whiteColor];
    
    self.usernameTextView.layer.borderColor = [UIColor colorWithRed:11.0f/255.0f green:47.0f/255.0f blue:143.0f/255.0f alpha:1.0].CGColor;
    self.usernameTextView.layer.borderWidth = 1.0f;
    self.usernameTextView.layer.cornerRadius = 15.0f;
    self.usernameTextView.layer.masksToBounds = YES;
    self.usernameTextView.backgroundColor = [UIColor whiteColor];
    
    self.passwordTextView.layer.borderColor = [UIColor colorWithRed:11.0f/255.0f green:47.0f/255.0f blue:143.0f/255.0f alpha:1.0].CGColor;
    self.passwordTextView.layer.borderWidth = 1.0f;
    self.passwordTextView.layer.cornerRadius = 15.0f;
    self.passwordTextView.layer.masksToBounds = YES;
    self.passwordTextView.backgroundColor = [UIColor whiteColor];
    
    paddingView_left = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
    paddingView_right = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
    self.passwordTextView.leftView = paddingView_left;
    self.passwordTextView.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTextView.rightView = paddingView_right;
    self.passwordTextView.rightViewMode = UITextFieldViewModeAlways;
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    _loadingView.center = self.view.center;//CGPointMake(_screenWidth/2.0, _screenHeight/2.0);
    _loadingView.layer.cornerRadius = 10.0;
    _loadingView.hidden = YES;
    [self.view addSubview:_loadingView];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
    [_loadingView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    [self refreshPage];
}

- (void)viewDidLoad
{
    NSLog(@"LoginViewController viewDidLoad");
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self roundButton: self.signinButton];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
//                                   initWithTarget:self
//                                   action:@selector(dismissKeyboard)];
//    
//    [self.view addGestureRecognizer:tap];
    
    self.usernameTextView.delegate = self;
    self.passwordTextView.delegate = self;
    [self layoutSetup];
    [self rememberCheckBox];
    [self refreshUsername];
    
    if (!_versionLabel)
    {
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        
        _versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, self.view.frame.size.height-15.0, self.view.frame.size.width-10.0, 15.0)];
        _versionLabel.backgroundColor = [UIColor clearColor];
        _versionLabel.textColor = [UIColor blackColor];
        _versionLabel.text = [NSString stringWithFormat:@"Version %@", version];
        _versionLabel.numberOfLines = 1;
        _versionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _versionLabel.textAlignment = NSTextAlignmentRight;
        _versionLabel.font = [UIFont fontWithName:@"Roboto-Light" size:10.0];
        [self.view addSubview:_versionLabel];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"LoginViewController viewWillAppear");
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewMovingUp:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewMovingDown)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:NSUserDefaultKey_USER])
        [self proceedNext];
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"LoginViewController viewDidAppear");
    [super viewDidAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:NSUserDefaultKey_USER])
        [self proceedNext];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDel setSiteIndicator:@""];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

#pragma mark - UIButtons Actions
- (IBAction)SignInPressed:(id)sender
{
    [self.view endEditing:YES];
    
    if (!self.usernameTextView.text.length || !self.passwordTextView.text.length)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"All_fields_are_mandatory", nil) message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
        [alert show];
        return;
    }
    [self setLoadingHidden:NO];
    
    NSString *language = ([[self.enButton titleColorForState:UIControlStateNormal] isEqual:[UIColor whiteColor]])? @"EN":@"MY";
    NSLog(@"language: %@", language);
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi loginUser:self.usernameTextView.text password:self.passwordTextView.text language:language :LOGIN_USER_CALL_ID];
}

- (IBAction)enPressed:(id)sender
{
    // Change to English
    self.enButton.backgroundColor = [UIColor darkGrayColor];
    [self.enButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.bmButton.backgroundColor = [UIColor clearColor];
    [self.bmButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    self.znButton.backgroundColor = [UIColor clearColor];
    [self.znButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    [NSBundle setLanguage:kLMEnglish];
    
    [self refreshPage];
}

- (IBAction)bmPressed:(id)sender
{
    // Change to BM
    self.bmButton.backgroundColor = [UIColor darkGrayColor];
    [self.bmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.enButton.backgroundColor = [UIColor clearColor];
    [self.enButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    self.znButton.backgroundColor = [UIColor clearColor];
    [self.znButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    [NSBundle setLanguage:kLMMalay];
    
    [self refreshPage];
}

- (IBAction)znPressed:(id)sender
{
    // Change to ZH
    self.znButton.backgroundColor = [UIColor darkGrayColor];
    [self.znButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.enButton.backgroundColor = [UIColor clearColor];
    [self.enButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    self.bmButton.backgroundColor = [UIColor clearColor];
    [self.bmButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    [NSBundle setLanguage:kLMChinese];
    
    [self refreshPage];
}

- (void)refreshPage
{
    self.copyrightLabel.text = NSLocalizedString(@"copyright", nil);
    self.usernameTextView.placeholder = NSLocalizedString(@"username", nil);
    self.passwordTextView.placeholder = NSLocalizedString(@"password", nil);
    [self.signinButton setTitle:NSLocalizedString(@"signin", nil) forState:UIControlStateNormal];
    [self.remembermeButton setTitle:NSLocalizedString(@"remember_me", nil) forState:UIControlStateNormal];
}

- (IBAction)rememberPressed:(id)sender
{
    BOOL isRemember = [[NSUserDefaults standardUserDefaults] boolForKey:NSUSERDEFAULT_REMEMBER_ME_KEY];
    [[NSUserDefaults standardUserDefaults] setBool:!isRemember forKey:NSUSERDEFAULT_REMEMBER_ME_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self rememberCheckBox];
}

- (void) rememberCheckBox
{
    BOOL isRemember = [[NSUserDefaults standardUserDefaults] boolForKey:NSUSERDEFAULT_REMEMBER_ME_KEY];
    
    if (isRemember)
    {
        [self.remembermeButton setImage:[UIImage imageNamed:@"remember_checkbox.png"] forState:UIControlStateNormal];
    }
    else
    {
        [self.remembermeButton setImage:[UIImage imageNamed:@"remember_uncheckbox.png"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:SAVED_USERNAME_KEY];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:SAVED_PASSWORD_KEY];
    }
}

- (void)refreshUsername
{
    self.usernameTextView.text = [[NSUserDefaults standardUserDefaults] valueForKey:SAVED_USERNAME_KEY];
    self.passwordTextView.text = [[NSUserDefaults standardUserDefaults] valueForKey:SAVED_PASSWORD_KEY];
}

- (void)proceedNext
{
    [NSBundle setLanguage:[[NSUserDefaults standardUserDefaults] objectForKey:APP_LANGUAGE]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSLog(@"user: %@", user);
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    //BOOL isUser = [[roles objectForKey:ROLES_USER_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    BOOL isBosSuper = [[roles objectForKey:ROLES_BOS_SUPERVISOR_KEY] boolValue];
    BOOL isBosUser = [[roles objectForKey:ROLES_BOS_USER_KEY] boolValue];
    BOOL isSite1 = [[roles objectForKey:ROLES_SITE1_KEY] boolValue];
    BOOL isSite2 = [[roles objectForKey:ROLES_SITE2_KEY] boolValue];
    
    if (isSite1 || isSite2)
    {
        SelectSiteViewController *selectVC = [[SelectSiteViewController alloc] init];
        //UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:selectVC];
        
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [UIView transitionWithView:appDelegate.window duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void)
         {
             BOOL oldState = [UIView areAnimationsEnabled];
             [UIView setAnimationsEnabled:NO];
             
             [appDelegate.window setRootViewController:selectVC];
             [appDelegate.window makeKeyAndVisible];
             
             [UIView setAnimationsEnabled:oldState];
             
         }completion:nil];
    }
    else if (isBosSuper || isBosUser || isAdmin)
    {
        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (!(isSite1 || isSite2))
            appDel.API_SITE_URL = API_LOCATION_PREFIX;
        else
            appDel.API_SITE_URL = isSite1? SITE1_API_LOCATION_PREFIX:SITE2_API_LOCATION_PREFIX;
        
        [self switchToDashboard:@"NewMenuView"];
    }
    else if (isOwner || isMonitor)
    {
        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (!(isSite1 || isSite2))
            appDel.API_SITE_URL = API_LOCATION_PREFIX;
        else
            appDel.API_SITE_URL = isSite1? SITE1_API_LOCATION_PREFIX:SITE2_API_LOCATION_PREFIX;
        
        [self switchToDashboard:@"AdminMainMenuView"];
    }
    else
    {
        AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (!(isSite1 || isSite2))
            appDel.API_SITE_URL = API_LOCATION_PREFIX;
        else
            appDel.API_SITE_URL = isSite1? SITE1_API_LOCATION_PREFIX:SITE2_API_LOCATION_PREFIX;
        
        [self switchToDashboard:@"UserMainMenuView"];
    }
    
    BOOL isRemember = [[NSUserDefaults standardUserDefaults] boolForKey:NSUSERDEFAULT_REMEMBER_ME_KEY];
    if (isRemember)
    {
        [defaults setValue:self.usernameTextView.text forKey:SAVED_USERNAME_KEY];
        [defaults setValue:self.passwordTextView.text forKey:SAVED_PASSWORD_KEY];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}

- (void) viewMovingUp:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    CGRect frame = self.view.frame;
    frame.origin.y =  keyboardFrameBeginRect.size.height - 280;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];
}

- (void) viewMovingDown
{
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];
}

-(void)dismissKeyboard
{
    [self.usernameTextView resignFirstResponder];
    [self.passwordTextView resignFirstResponder];
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:LOGIN_USER_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableDictionary *user = (NSMutableDictionary *)[AppUtil removeNullElementFromObject:result.result];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:user forKey:NSUserDefaultKey_USER];
            
            [self proceedNext];
            
            /*user = [defaults objectForKey:NSUserDefaultKey_USER];
             NSDictionary *profile = [user objectForKey:API_PROFILE_KEY];
             NSString *picUrl = [profile objectForKey:USER_PROFILE_PIC_KEY];
             
             if (picUrl.length)
             [self downloadProfilePicture];
             else
             [self getContactList];*/
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
