//
//  RemarkViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "RemarkViewController.h"
#import "InfoViewController.h"

@interface RemarkViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *remarkTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remarkTextViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewTopConstraint;

@end

@implementation RemarkViewController

UILabel *_placeholderLabel;

- (void) setupLayout
{
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.remarkTextViewHeightConstraint.constant = 200;
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        self.textViewTopConstraint.constant =  30;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.textViewTopConstraint.constant =  40;
        self.remarkTextViewHeightConstraint.constant = 222;        
    }
    else if([GeneralHelper isDeviceiPhone6plus])
    {
        self.textViewTopConstraint.constant =  40;
    }
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTapped:)];
    [self.view addGestureRecognizer:singleTap];
    
    _placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 10.0, 100.0, 30.0)];
    _placeholderLabel.backgroundColor = [UIColor clearColor];
    _placeholderLabel.textColor = [UIColor lightGrayColor];
    _placeholderLabel.text = NSLocalizedString(@"description_placeholder", nil);
    _placeholderLabel.textAlignment = NSTextAlignmentLeft;
    _placeholderLabel.font = self.remarkTextView.font;
    [_placeholderLabel sizeToFit];
    [self.remarkTextView addSubview:_placeholderLabel];
    
    self.remarkTextView.textColor = [UIColor blackColor];
    //self.remarkTextView.delegate = self;
}

- (void)viewDidTapped:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.remarkTextView.delegate = self;
    [self setTextViewLayout: self.remarkTextView];

    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    // register for keyboard notifications
    if ([GeneralHelper isDeviceiPhone4] || [GeneralHelper isDeviceiPhone5])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(viewMovingUp:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(viewMovingDown)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textViewDidChange:)
                                                 name:UITextViewTextDidChangeNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextView Delegate

/*- (void) viewMovingUp:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    CGRect frame = self.view.frame;
    frame.origin.y =  keyboardFrameBeginRect.size.height - 280;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];
}

- (void) viewMovingDown
{
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];
}*/

- (void) viewMovingUp:(NSNotification *)notification
{
    //NSDictionary* keyboardInfo = [notification userInfo];
    //NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    //CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    //CGRect frame = self.remarkTextView.frame;
    //frame.origin.y =  keyboardFrameBeginRect.size.height - 100;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.remarkTextViewHeightConstraint.constant = self.remarkTextView.frame.size.height-100.0;
        //self.remarkTextView.frame = CGRectMake(self.remarkTextView.frame.origin.x, self.remarkTextView.frame.origin.y, self.remarkTextView.frame.size.width, self.remarkTextView.frame.size.height-200.0);
    }];
}

- (void) viewMovingDown
{
    /*CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];*/
    
    [UIView animateWithDuration:1.0 animations:^{
        self.remarkTextViewHeightConstraint.constant = self.remarkTextView.frame.size.height+100.0;
        //self.remarkTextView.frame = CGRectMake(self.remarkTextView.frame.origin.x, self.remarkTextView.frame.origin.y, self.remarkTextView.frame.size.width, self.remarkTextView.frame.size.height-200.0);
    }];
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    //self.remarkTextView.text = @"";
    //self.remarkTextView.textColor = [UIColor blackColor];
    
    //if (textView.keyboardType == UIKeyboardTypeNumberPad || textView.keyboardType == UIKeyboardTypeNamePhonePad || textView.keyboardType == UIKeyboardTypePhonePad || textView.keyboardType == UIKeyboardTypeNumbersAndPunctuation || textView.keyboardType == UIKeyboardTypeDecimalPad)
    //{
        UIView *keyboardAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 30.0)];
        keyboardAccessoryView.backgroundColor = [UIColor colorWithWhite:0.85 alpha:1.0];
        
        UIButton *okBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        okBtn.frame = CGRectMake(self.view.frame.size.width-70.0, 0.0, 70.0, 30.0);
        [okBtn setTitle:@"Done" forState:UIControlStateNormal];
        [okBtn setTitle:@"Done" forState:UIControlStateHighlighted];
        [okBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [okBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [okBtn addTarget:self action:@selector(doneTapped:) forControlEvents:UIControlEventTouchUpInside];
        okBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
        [keyboardAccessoryView addSubview:okBtn];
        
        textView.inputAccessoryView = keyboardAccessoryView;
    //}
    
    return YES;
}

- (void)doneTapped:(UIButton *)sender
{
    [self.remarkTextView resignFirstResponder];
}

-(void) textViewDidChange:(UITextView *)textView
{
    _placeholderLabel.hidden = self.remarkTextView.text.length;
    
    if(self.remarkTextView.text.length == 0)
    {
        //self.remarkTextView.textColor = [UIColor lightGrayColor];
        //self.remarkTextView.text = @"Type your remark here...";
        //[self.remarkTextView resignFirstResponder];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_REMARK];
    }
    else
    {
        [ReportDetailsModel saveValue: self.remarkTextView.text key:NSUSERDEFAULT_KEY_REMARK];
    }
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    /*if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }*/
    
    return YES;
}


-(void)dismissKeyboard
{
    [self.remarkTextView resignFirstResponder];
}
@end
