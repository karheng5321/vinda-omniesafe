//
//  PhotoVideoViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "PhotoVideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreMedia/CoreMedia.h>
#import "AppUtil.h"

@interface PhotoVideoViewController () <UIImagePickerControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictureButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictureButtonHeightConstraint;

@property (weak, nonatomic) IBOutlet UIButton *pictureButton;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;

@end

@implementation PhotoVideoViewController

//NO is select from gallery
bool isTakingPhoto;
bool isTakingVideo;
NSMutableArray *picVidArray;
NSData *cameraImage;
NSData *cameraVideo;

- (void) setupLayout
{
    CGFloat buttonSizeWidth;
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.buttonTopConstraint.constant = 10;
        buttonSizeWidth = 100;
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        buttonSizeWidth = 130;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.buttonTopConstraint.constant = 40;
        buttonSizeWidth = 150;
    }
    else if ([GeneralHelper isDeviceiPhone6plus])
    {
        self.buttonTopConstraint.constant = 55;
        buttonSizeWidth = 170;
    }
    
    self.pictureButtonWidthConstraint.constant = buttonSizeWidth;
    self.pictureButtonHeightConstraint.constant = buttonSizeWidth;
    
    self.pictureButton.layer.cornerRadius = buttonSizeWidth/2.0;
    self.pictureButton.clipsToBounds = YES;
    self.pictureButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.videoButton.layer.cornerRadius = buttonSizeWidth/2.0;
    self.videoButton.clipsToBounds = YES;
    self.videoButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
    
    picVidArray = [NSMutableArray new];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (IBAction)pictureTapped:(id)sender
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: NSLocalizedString(@"take_photo", nil), NSLocalizedString(@"choose_existing", nil), nil];
    actionSheet.tag = ACTIONSHEET_TAG_PHOTO;
    [actionSheet showInView:self.view];
}

- (IBAction)videoTapped:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: NSLocalizedString(@"take_video", nil), NSLocalizedString(@"choose_video", nil), nil];
    actionSheet.tag = ACTIONSHEET_TAG_VIDEO;
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == ACTIONSHEET_TAG_PHOTO)
    {
        switch (buttonIndex) {
            case 0:
                isTakingPhoto = YES;
                [self startTakingPicture];
                break;
            case 1:
                isTakingPhoto = NO;
                [self startTakingPicture];
                break;
            default:
                break;
        }
    }
    
    else if (actionSheet.tag == ACTIONSHEET_TAG_VIDEO)
    {
        switch (buttonIndex) {
            case 0:
                isTakingVideo = YES;
                [self startTakingVideo];
                break;
            case 1:
                isTakingVideo = NO;
                [self startTakingVideo];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Picture and Video Action

- (void) startTakingPicture {
    
    UIImagePickerController *picturePicker = [[UIImagePickerController alloc] init];
    
    if (isTakingPhoto == YES)
    {
        picturePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if (isTakingPhoto == NO)
    {
        picturePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    picturePicker.editing = YES;
    picturePicker.delegate = (id)self;
    [self presentViewController:picturePicker animated:YES completion:nil];
}


- (BOOL)startTakingVideo
{
    
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    
    if (isTakingVideo == YES)
    {
        videoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if (isTakingVideo == NO)
    {
        videoPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    videoPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    videoPicker.allowsEditing = NO;
    videoPicker.delegate = (id)self;
    videoPicker.videoMaximumDuration = VIDEO_DURATION;
    [self presentViewController:videoPicker animated:YES completion:nil];
    return YES;
}

#pragma mark - Image picker delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"%@", info);
    [self dismissViewControllerAnimated:TRUE completion:nil];
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    // Handle a movie capture
    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo)
    {
        
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSError *attributesError;
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[videoURL path] error:&attributesError];
        NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
        long long fileSize = [fileSizeNumber longLongValue];
        //NSLog(@"fileSize: %lld", fileSize);
        if (fileSize > FILE_UPLOAD_LIMIT)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"big_video_warning", nil) message:NSLocalizedString(@"big_video_message", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        [ReportDetailsModel saveValue: [videoURL absoluteString] key:NSUSERDEFAULT_KEY_VIDEOURL];
        cameraVideo = [NSData dataWithContentsOfURL:videoURL];
        //[picVidArray addObject:cameraVideo];
        
        //If video is taken, save to gallery path];
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([videoURL path]) && (isTakingVideo == YES))
        {
            UISaveVideoAtPathToSavedPhotosAlbum([videoURL path], self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        }
        
        UIImage *image = [AppUtil thumbnailImageForVideo:videoURL atTime:0.0];
        image = [AppUtil resizeImageForThumbnail:image];
        NSData *thumbData = UIImageJPEGRepresentation(image, 0.5);
        [_videoButton setImage:image forState:UIControlStateNormal];
        [ReportDetailsModel saveValue: thumbData key:NSUSERDEFAULT_KEY_VIDEO_THUMB];
        [ReportDetailsModel saveValue: cameraVideo key:NSUSERDEFAULT_KEY_VIDEO_DATA];
        
        //[picVidArray addObject:thumbData];
        [picVidArray addObject:@"YES"];
    }
    // Handle a picture capture
    else {
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        cameraImage = UIImageJPEGRepresentation([AppUtil resizeImageForUpload:image], 0.5);
        [ReportDetailsModel saveValue: cameraImage key:NSUSERDEFAULT_KEY_PIC_DATA];
        
        UIImage *thumb = [AppUtil resizeImageForThumbnail:image];
        NSData *thumbData = UIImageJPEGRepresentation(thumb, 0.5);
        [ReportDetailsModel saveValue: thumbData key:NSUSERDEFAULT_KEY_PIC_THUMB];
        
        //[picVidArray addObject: cameraImage];
        //[picVidArray addObject: thumbData];
        [picVidArray addObject: @"YES"];
        
        //If picture is taken, save to gallery
        if (isTakingPhoto == YES) {
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        }
        
        [_pictureButton setImage:thumb forState:UIControlStateNormal];
    }
    
    [ReportDetailsModel saveArray:picVidArray key:NSUSERDEFAULT_KEY_PICVIDFILE];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"media_taken", nil) message:NSLocalizedString(@"press_next", nil)
                                                   delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

-(void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    if (error)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Media Failed to Save into Gallery"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:true completion: nil];
}

+ (UIImage *)thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time
{
    MPMoviePlayerViewController *video = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    UIImage *image = [video.moviePlayer thumbnailImageAtTime:time timeOption:MPMovieTimeOptionNearestKeyFrame];
    
    return image;
}

@end
