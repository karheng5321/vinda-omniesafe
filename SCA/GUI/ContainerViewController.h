//
//  ContainerViewController.h
//  SCA
//
//  Created by kyTang on 28/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerViewController : UIViewController

- (void)gotoPhotoVideo;
- (void)gotoRemark;
- (void)gotoHazardType;
- (void)gotoAreaLocationType;
- (void)gotoCriticallyType;

@end