//
//  MainMenuViewController.m
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "MainMenuViewController.h"
#import "LoginViewController.h"
#import "ReportDetailsModel.h"
#import "GeneralHelper.h"
#import "Constant.h"
#import "AsyncApi.h"
#import "AppDelegate.h"
#import "SelectSiteViewController.h"

@interface MainMenuViewController ()

@property (weak, nonatomic) IBOutlet UIView *reportdangerButton;
@property (weak, nonatomic) IBOutlet UIView *reporthistoryButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *historyTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *reportDangerLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportHistoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reportButtonTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *unsentButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation MainMenuViewController

- (void) setupLayout
{
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
        self.historyTopConstraint.constant = 10;
    }
    
    CGFloat prefixWidth = [[UIScreen mainScreen] bounds].size.width-(70*2);
    
    if ([GeneralHelper isDeviceiPhone6plus])
    {
        self.reportButtonTopConstraint.constant = 50;
        prefixWidth = [[UIScreen mainScreen] bounds].size.width-(110*2);
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.reportButtonTopConstraint.constant = 40;
        prefixWidth = [[UIScreen mainScreen] bounds].size.width-(95*2);
    }
    
    self.buttonWidthConstraint.constant = prefixWidth;
    self.buttonHeightConstraint.constant = prefixWidth;
    
    self.reportdangerButton.layer.cornerRadius = prefixWidth/2;
    self.reportdangerButton.layer.masksToBounds = YES;
    
    self.reporthistoryButton.layer.cornerRadius = prefixWidth/2;
    self.reporthistoryButton.layer.masksToBounds = YES;
    
    self.reportDangerLabel.text = NSLocalizedString(@"report_unsafe_condition", nil);
    self.reportHistoryLabel.text = NSLocalizedString(@"report_history", nil);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    self.usernameLabel.text = [user objectForKey:@"ism_user_fullname"];
    
    [self refreshView];
    
    //if ([self.navigationController.viewControllers count] <= 1)
    //{
        UIImage *btnImage = [UIImage imageNamed:@"logout_icon.png"];
        float scale = btnImage.size.width/20.0;
        float btnWidth = btnImage.size.width/scale;
        float btnHeight = btnImage.size.height/scale;
        
        UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        logoutBtn.frame = CGRectMake(self.view.frame.size.width-10.0-btnWidth, self.view.frame.size.height-10.0-btnHeight, btnWidth, btnHeight);
        logoutBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [logoutBtn setImage:btnImage forState:UIControlStateNormal];
        [logoutBtn addTarget:self action:@selector(logoutTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:logoutBtn];
    //}
}

- (void)refreshView
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    
    NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_REPORT] mutableCopy];
    if (!reportDict)
        reportDict = [NSMutableDictionary new];
    NSMutableArray *dataArray = [[reportDict objectForKey:userId] mutableCopy];
    if (!dataArray)
        dataArray = [NSMutableArray new];
    
    self.unsentButton.hidden = ([dataArray count] == 0);
    
    self.backButton.hidden = ([self.navigationController.viewControllers count] <= 1);

    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isSite1 = [[roles objectForKey:ROLES_SITE1_KEY] boolValue];
    BOOL isSite2 = [[roles objectForKey:ROLES_SITE2_KEY] boolValue];
    if (isSite1 || isSite2)
        self.backButton.hidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (void)logoutTapped:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"logout_alert", nil) message:NSLocalizedString(@"logout_message", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self logoutFromServer];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:NSUserDefaultKey_USER];
        [ReportDetailsModel clearAllReportDetailKey];
        [self switchToDashboard:@"LoginViewController"];
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)logoutFromServer
{
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:nil];
    [asyncApi logoutFromServer:nil];
}

- (IBAction)unsentPressed:(id)sender
{
    [self performSegueWithIdentifier:@"gotoUnsentReport" sender:nil];
}

- (IBAction)reportDangerPressed:(id)sender
{
    /*AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDel isInternetAvailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet connection not available"
                                                        message:@"Please connect to the internet to proceed"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }*/
    
    [ReportDetailsModel clearAllReportDetailKey];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"NO" forKey:NSUSERDEFAULT_KEY_UPDATING_REPORT];
    
    [self performSegueWithIdentifier:@"gotoReportDanger" sender:self];
}

- (IBAction)reportHistoryPressed:(id)sender
{
    [self performSegueWithIdentifier:@"gotoReportHistpry" sender:self];
}

- (IBAction)backPressed:(id)sender
{
    NSLog(@"MainMenuViewController backPressed: %@", self.navigationController.viewControllers);
    
    if ([self.navigationController.viewControllers count] <= 1)
    {
        SelectSiteViewController *selectVC = [[SelectSiteViewController alloc] init];
        //UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:selectVC];
        
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [UIView transitionWithView:appDelegate.window duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void)
         {
             BOOL oldState = [UIView areAnimationsEnabled];
             [UIView setAnimationsEnabled:NO];
             
             [appDelegate.window setRootViewController:selectVC];
             [appDelegate.window makeKeyAndVisible];
             
             [UIView setAnimationsEnabled:oldState];
             
         }completion:nil];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)logoutPressed:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"logout_alert", nil)
                                                    message:NSLocalizedString(@"logout_message", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"yes", nil), nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
        {
            [self logoutFromServer];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults removeObjectForKey:NSUserDefaultKey_USER];
            [ReportDetailsModel clearAllReportDetailKey];
            [self switchToDashboard:@"LoginViewController"];
        }
            break;
    }
}

/*#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if ([segue.identifier isEqualToString:@"gotoSubmitView"])
    {
        [ReportDetailsModel saveValue:[GeneralHelper getCurrentDateTime] key:NSUSERDEFAULT_KEY_DATE_TIME];
    }
}*/

@end
