//
//  UnsentViewController.m
//  SCA
//
//  Created by kyTang on 22/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "UnsentViewController.h"
#import "ReportHistoryTableViewCell.h"
#import "ReportDetailsViewController.h"
#import "ReportDetailsModel.h"
#import "AsyncApi.h"
#import "AppUtil.h"
#import "SubmitViewController.h"

@interface UnsentViewController () <AsyncApiDelegate> {
    UIView *_loadingView;
}

@property (nonatomic, strong) NSMutableArray *reportArray;
@property (strong, nonatomic) NSMutableDictionary *offscreenCells;
@property (weak, nonatomic) IBOutlet UITableView *reporthistoryTable;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (nonatomic) NSInteger selectedIndex;

@end

@implementation UnsentViewController

static NSString * const GET_REPORT_CALL_ID = @"getreport";

- (void) setupLayout
{
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.topBarHeightConstraint.constant = 120;
    }
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    _loadingView.center = self.view.center;//CGPointMake(_screenWidth/2.0, _screenHeight/2.0);
    _loadingView.layer.cornerRadius = 10.0;
    _loadingView.hidden = YES;
    [self.view addSubview:_loadingView];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
    [_loadingView addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupLayout];
    
    [self createData];
    [self.reporthistoryTable reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //[self downloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

- (void)createData
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    
    NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_REPORT] mutableCopy];
    if (!reportDict)
        reportDict = [NSMutableDictionary new];
    NSMutableArray *dataArray = [[reportDict objectForKey:userId] mutableCopy];
    if (!dataArray)
        dataArray = [NSMutableArray new];
    
    self.reportArray = [NSMutableArray new];
    
    for (NSInteger i = 0; i < [dataArray count]; i++) {
        NSDictionary *dict = [dataArray objectAtIndex:i];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSDate *reportDate = [dict objectForKey:NSUSERDEFAULT_KEY_DATE_TIME];
        [dateFormatter setDateFormat:@"dd/MM/yyyy, hh:mm aa"];
        NSString *submitDateString = [dateFormatter stringFromDate:reportDate];
        NSLog(@"submitDateString: %@", submitDateString);
        
        ReportDetailsModel *report = [ReportDetailsModel new];
        report.dateTime = submitDateString;
        report.hazardType = [dict objectForKey:NSUSERDEFAULT_KEY_HAZARDTYPE];
        report.criticallyType = [dict objectForKey:NSUSERDEFAULT_KEY_AREA_LOCATION];
        
        [self.reportArray addObject:report];
    }
}

#pragma mark - UIButtons Actions

- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = NSStringFromClass([ReportHistoryTableViewCell class]);
    
    ReportHistoryTableViewCell *cell = [self.offscreenCells objectForKey:reuseIdentifier];
    if(cell == nil)
    {
        cell = (ReportHistoryTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    [cell updateDisplay:self.reportArray[indexPath.row]];
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGFloat height = 0.0;
    height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    height += 1;
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.reportArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReportHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ReportHistoryTableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    [cell updateDisplay:self.reportArray[indexPath.row]];
    [self.offscreenCells setObject:cell forKey:@"ReportHistoryTableViewCell"];
    
    cell.rightarrowImage.hidden = YES;
    
    [cell.deleteBtn removeFromSuperview];
    
    UIImage *icon = [UIImage imageNamed:@"ic_delete_forever.png"];
    float scale = icon.size.height/30.0;
    float iconWidth = icon.size.width/scale;
    float iconHeight = icon.size.height/scale;
    
    UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame = CGRectMake(tableView.frame.size.width-5.0-iconWidth, 5.0, iconWidth, iconHeight);
    deleteBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [deleteBtn setBackgroundImage:icon forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteTapped:) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.tag = indexPath.row+1;
    deleteBtn.center = CGPointMake(deleteBtn.center.x, cell.dateLabel.center.y);
    [cell.contentView addSubview:deleteBtn];
    
    cell.deleteBtn = deleteBtn;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setGreyWhiteCellColor:cell row:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //ReportDetailsModel *report = self.reportArray[indexPath.row];
    //NSLog(@"report: %@", report.dateTime);
    self.selectedIndex = indexPath.row;
    [self performSegueWithIdentifier:@"gotoSubmitReport" sender:nil];
}

- (void)deleteTapped:(UIButton *)sender
{
    NSLog(@"deleteTapped");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    
    NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_REPORT] mutableCopy];
    if (!reportDict)
        reportDict = [NSMutableDictionary new];
    NSMutableArray *dataArray = [[reportDict objectForKey:userId] mutableCopy];
    if (!dataArray)
        dataArray = [NSMutableArray new];
    
    NSInteger index = sender.tag-1;
    if (index < [dataArray count])
        [dataArray removeObjectAtIndex:index];
    
    [reportDict setObject:dataArray forKey:userId];
    [defaults setObject:reportDict forKey:NSUserDefaultKey_SAVED_REPORT];
    
    [self createData];
    [self.reporthistoryTable reloadData];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue");
    if ([segue.identifier isEqualToString:@"gotoSubmitReport"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
        NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
        
        NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_REPORT] mutableCopy];
        if (!reportDict)
            reportDict = [NSMutableDictionary new];
        NSMutableArray *dataArray = [[reportDict objectForKey:userId] mutableCopy];
        if (!dataArray)
            dataArray = [NSMutableArray new];
        
        NSDictionary *dict = [dataArray objectAtIndex:self.selectedIndex];
        //NSLog(@"dict: %@", dict);
        
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_HAZARDTYPE] forKey:NSUSERDEFAULT_KEY_HAZARDTYPE];
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_AREA_LOCATION] forKey:NSUSERDEFAULT_KEY_AREA_LOCATION];
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_AREA_OWNER] forKey:NSUSERDEFAULT_KEY_AREA_OWNER];
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE] forKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_REMARK] forKey:NSUSERDEFAULT_KEY_REMARK];
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB] forKey:NSUSERDEFAULT_KEY_PIC_THUMB];
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB] forKey:NSUSERDEFAULT_KEY_PIC_THUMB];
        
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_HAZARDID] forKey:NSUSERDEFAULT_KEY_HAZARDID];
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_AREA_ID] forKey:NSUSERDEFAULT_KEY_AREA_ID];
        [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_CRITICALLY_ID] forKey:NSUSERDEFAULT_KEY_CRITICALLY_ID];
        
        if ([dict objectForKey:NSUSERDEFAULT_KEY_REPORTNO])
        {
            [defaults setObject:[dict objectForKey:NSUSERDEFAULT_KEY_REPORTNO] forKey:NSUSERDEFAULT_KEY_REPORTNO];
            [defaults setObject:@"YES" forKey:NSUSERDEFAULT_KEY_UPDATING_REPORT];
        }
        else
            [defaults setObject:@"NO" forKey:NSUSERDEFAULT_KEY_UPDATING_REPORT];
        
        SubmitViewController *destinationVC = segue.destinationViewController;
        destinationVC.cachedIndex = [NSString stringWithFormat:@"%ld", self.selectedIndex];
    }
}

#pragma mark - Private



#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_REPORT_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
