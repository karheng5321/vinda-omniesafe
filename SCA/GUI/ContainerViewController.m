//
//  ContainerViewController.m
//  SCA
//
//  Created by kyTang on 28/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "ContainerViewController.h"
#import "PhotoVideoViewController.h"
#import "RemarkViewController.h"
#import "HazardTypeViewController.h"
#import "AreaLocationViewController.h"
#import "CriticallyTypeViewController.h"

#define SegueIdentifierPhotoVideo @"embedPhotoViedeo"
#define SegueIdentifierRemark @"embedRemark"
#define SegueIdentifierHazardType @"embedHazardType"
#define SegueIdentifierAreaLocationType @"embedAreaLocationType"
#define SegueIdentifierCriticallyType @"embedCriticallyType"

@interface ContainerViewController ()

@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (strong, nonatomic) PhotoVideoViewController *photoVideoViewController;
@property (strong, nonatomic) RemarkViewController *remarkViewController;
@property (strong, nonatomic) HazardTypeViewController *hazardTypeViewController;
@property (strong, nonatomic) AreaLocationViewController *areaLocationTypeViewController;
@property (strong, nonatomic) CriticallyTypeViewController *criticallyTypeViewController;
@property (assign, nonatomic) BOOL transitionInProgress;

@end

@implementation ContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.transitionInProgress = NO;
    self.currentSegueIdentifier = SegueIdentifierPhotoVideo;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Instead of creating new VCs on each seque we want to hang on to existing
    // instances if we have it. Remove the second condition of the following
    // two if statements to get new VC instances instead.
    if ([segue.identifier isEqualToString:SegueIdentifierPhotoVideo])
    {
        self.photoVideoViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueIdentifierRemark])
    {
        self.remarkViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueIdentifierHazardType])
    {
        self.hazardTypeViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueIdentifierAreaLocationType])
    {
        self.areaLocationTypeViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueIdentifierCriticallyType])
    {
        self.criticallyTypeViewController = segue.destinationViewController;
    }
    
    // If we're going to the first view controller.
    if ([segue.identifier isEqualToString:SegueIdentifierPhotoVideo])
    {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.photoVideoViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    // By definition the second view controller will always be swapped with the
    // first one.
    else if ([segue.identifier isEqualToString:SegueIdentifierRemark])
    {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.remarkViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierHazardType])
    {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.hazardTypeViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierAreaLocationType])
    {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.areaLocationTypeViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierCriticallyType])
    {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.criticallyTypeViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0.1 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        self.transitionInProgress = NO;
    }];
}

- (void)gotoPhotoVideo
{
    self.currentSegueIdentifier = SegueIdentifierPhotoVideo;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)gotoRemark
{
    self.currentSegueIdentifier = SegueIdentifierRemark;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)gotoHazardType
{
    self.currentSegueIdentifier = SegueIdentifierHazardType;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)gotoAreaLocationType
{
    self.currentSegueIdentifier = SegueIdentifierAreaLocationType;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)gotoCriticallyType
{
    self.currentSegueIdentifier = SegueIdentifierCriticallyType;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

@end