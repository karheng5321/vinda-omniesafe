//
//  AdminMenuSelectionViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 10/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AdminMenuSelectionViewController.h"
#import "AdminMainMenuViewController.h"
#import "Constant.h"
#import "AsyncApi.h"
#import "AppDelegate.h"
#import "AppUtil.h"
#import "SelectSiteViewController.h"

@interface AdminMenuSelectionViewController () {
    UILabel *_ucBadgeLabel;
}

@property (weak, nonatomic) IBOutlet UIView *reportdangerButton;
@property (weak, nonatomic) IBOutlet UIView *reporthistoryButton;
@property (weak, nonatomic) IBOutlet UIView *allhistoryButton;
@property (weak, nonatomic) IBOutlet UILabel *mainTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportDangerLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportHistoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaHistoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reportDangerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reportHistoryTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reportButtonCenter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *historyButtonCenter;
@property (weak, nonatomic) IBOutlet UIButton *unsentButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIButton *allhistoryBtn;

@end

@implementation AdminMenuSelectionViewController

static CGFloat const  badgeMinSize = 40.0;
static NSString* const GET_PAGE_DATA_CALL_ID = @"getpagedata";

NSString *role;

- (void) setupLayout
{
    CGFloat prefixWidth = [[UIScreen mainScreen] bounds].size.width-(80*2);
    
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.reportDangerTopConstraint.constant = 45;
        
        prefixWidth = [[UIScreen mainScreen] bounds].size.width-(105*2);
        self.buttonWidth.constant = prefixWidth;
        self.buttonHeight.constant = prefixWidth;
        
        self.reportButtonCenter.constant = -([[UIScreen mainScreen] bounds].size.width/4);
        self.historyButtonCenter.constant = ([[UIScreen mainScreen] bounds].size.width/4);
    }
    else if ([GeneralHelper isDeviceiPhone6plus])
    {
        self.reportDangerTopConstraint.constant = 60;
        prefixWidth = [[UIScreen mainScreen] bounds].size.width-(122*2);
        self.buttonWidth.constant = prefixWidth;
        self.buttonHeight.constant = prefixWidth;
        self.reportButtonCenter.constant = -([[UIScreen mainScreen] bounds].size.width/4);
        self.historyButtonCenter.constant = ([[UIScreen mainScreen] bounds].size.width/4);
    }
    
    
    self.reportdangerButton.layer.cornerRadius = self.reportdangerButton.frame.size.width/2;
    self.reportdangerButton.layer.masksToBounds = YES;
    
    self.reporthistoryButton.layer.cornerRadius = self.reporthistoryButton.frame.size.width/2;
    self.reporthistoryButton.layer.masksToBounds = YES;
    
    self.allhistoryButton.layer.cornerRadius = self.allhistoryButton.frame.size.width/2;
    self.allhistoryButton.layer.masksToBounds = YES;
    
    self.reportDangerLabel.text = NSLocalizedString(@"report_unsafe_condition", nil);
    self.reportHistoryLabel.text = NSLocalizedString(@"report_history", nil);
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    //BOOL isUser = [[roles objectForKey:ROLES_USER_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    //BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    
    if (isAdmin)
    {
        self.areaHistoryLabel.text = NSLocalizedString(@"all_history", nil);
    }
    else
    {
        self.areaHistoryLabel.text = NSLocalizedString(@"area_history", nil);
    }
    
    self.usernameLabel.text = [user objectForKey:@"ism_user_fullname"];
    
    [self refreshView];
    
    //if ([self.navigationController.viewControllers count] <= 1)
    //{
        UIImage *btnImage = [UIImage imageNamed:@"logout_icon.png"];
        float scale = btnImage.size.width/20.0;
        float btnWidth = btnImage.size.width/scale;
        float btnHeight = btnImage.size.height/scale;
        
        UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        logoutBtn.frame = CGRectMake(self.view.frame.size.width-10.0-btnWidth, self.view.frame.size.height-10.0-btnHeight, btnWidth, btnHeight);
        logoutBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [logoutBtn setImage:btnImage forState:UIControlStateNormal];
        [logoutBtn addTarget:self action:@selector(logoutTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:logoutBtn];
    //}
    
    if (isOwner || isAdmin)
    {
        _ucBadgeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _ucBadgeLabel.textAlignment = NSTextAlignmentCenter;
        _ucBadgeLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:16.0];
        _ucBadgeLabel.backgroundColor = [UIColor redColor];
        _ucBadgeLabel.textColor = [UIColor whiteColor];
        _ucBadgeLabel.layer.cornerRadius = badgeMinSize/2.0;
        _ucBadgeLabel.clipsToBounds = YES;
        [self.allhistoryButton.superview addSubview:_ucBadgeLabel];
    }
    
    if (appDel.ON_DIFFERENT_SITE)
    {
        _ucBadgeLabel.hidden = YES;
        self.allhistoryButton.hidden = YES;
        self.allhistoryBtn.hidden = YES;
        self.reporthistoryButton.hidden = YES;
        
        self.reporthistoryButton.frame = CGRectMake(self.reportdangerButton.frame.origin.x, self.reporthistoryButton.frame.origin.y, self.reportdangerButton.frame.size.width, self.reportdangerButton.frame.size.height);
    }
    
    [self getPageData];
}

- (void)refreshView
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    
    NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_REPORT] mutableCopy];
    if (!reportDict)
        reportDict = [NSMutableDictionary new];
    NSMutableArray *dataArray = [[reportDict objectForKey:userId] mutableCopy];
    if (!dataArray)
        dataArray = [NSMutableArray new];
    
    self.unsentButton.hidden = ([dataArray count] == 0);
    
    self.backButton.hidden = ([self.navigationController.viewControllers count] <= 1);
    
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isSite1 = [[roles objectForKey:ROLES_SITE1_KEY] boolValue];
    BOOL isSite2 = [[roles objectForKey:ROLES_SITE2_KEY] boolValue];
    if (isSite1 || isSite2)
        self.backButton.hidden = NO;
}

- (void)logoutTapped:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"logout_alert", nil) message:NSLocalizedString(@"logout_message", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self logoutFromServer];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults removeObjectForKey:NSUserDefaultKey_USER];
        [ReportDetailsModel clearAllReportDetailKey];
        [self switchToDashboard:@"LoginViewController"];
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)getPageData
{
    NSLog(@"bosGetBadgeNumber");
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi bosGetBadgeNumber:GET_PAGE_DATA_CALL_ID];
}

- (void)refreshBadge:(NSMutableDictionary *)badge
{
    NSLog(@"BOSMenuViewController refreshBadge");
    NSString *ucBadge = [NSString stringWithFormat:@"%@", [badge valueForKey:@"uc"]];
    //NSString *bosBadge = [NSString stringWithFormat:@"%@", [badge valueForKey:@"bos"]];
    
    _ucBadgeLabel.text = ucBadge;
    //_bosBadgeLabel.text = bosBadge;

    CGFloat textMaxWidth = self.view.frame.size.width/4.0;
    NSDictionary *textAttrDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _ucBadgeLabel.font, NSFontAttributeName,
                                  nil];
    CGRect ucRect = [ucBadge boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    //CGRect bosRect = [bosBadge boundingRectWithSize:CGSizeMake(textMaxWidth, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:textAttrDict context:nil];
    
    CGFloat ucWidth = MAX(ucRect.size.width+20.0, badgeMinSize);
    _ucBadgeLabel.frame = CGRectMake(self.allhistoryButton.frame.origin.x+self.allhistoryButton.frame.size.width*0.75, self.allhistoryButton.frame.origin.y, ucWidth, badgeMinSize);
    
    //CGFloat bosWidth = MAX(bosRect.size.width+20.0, badgeMinSize);
    //_bosBadgeLabel.frame = CGRectMake(_deptBtn.frame.origin.x+_deptBtn.frame.size.width*0.75, _deptBtn.frame.origin.y, bosWidth, badgeMinSize);
    
    _ucBadgeLabel.hidden = ([ucBadge integerValue] == 0);
    //_bosBadgeLabel.hidden = ([bosBadge integerValue] == 0);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    role = [self getUserRole];
    
    [self setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDel.ON_DIFFERENT_SITE)
    {
        self.allhistoryButton.userInteractionEnabled = NO;
        self.reporthistoryButton.hidden = NO;
        
        self.reporthistoryButton.center = CGPointMake(self.reportdangerButton.center.x, self.reporthistoryButton.center.y);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (IBAction)unsentPressed:(id)sender
{
    [self performSegueWithIdentifier:@"gotoUnsentReport" sender:nil];
}

- (IBAction)reportDangerPressed:(id)sender
{
    
    /*if ([[self getUserRole] isEqualToString: @"admin"]){
        [self performSegueWithIdentifier:@"gotoReportDangerAreaAdmin" sender:self];
    }
    else if ([[self getUserRole] isEqualToString: @"area"]) {
        [self performSegueWithIdentifier:@"gotoReportDangerArea" sender:self];
    }*/
    
    /*AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDel isInternetAvailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet connection not available"
                                                        message:@"Please connect to the internet to proceed"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }*/
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"NO" forKey:NSUSERDEFAULT_KEY_UPDATING_REPORT];
    
    [self performSegueWithIdentifier:@"gotoReportDanger" sender:self];
}

- (IBAction)reportHistoryPressed:(id)sender
{
    [self performSegueWithIdentifier:@"gotoReportHistoryByAreaAdmin" sender:self];
}

- (IBAction)areaHistoryPressed:(id)sender
{
    [self performSegueWithIdentifier:@"gotoAreaAllHistoryByAreaAdmin" sender:self];
}

- (IBAction)backPressed:(id)sender
{
    if ([self.navigationController.viewControllers count] <= 1)
    {
        SelectSiteViewController *selectVC = [[SelectSiteViewController alloc] init];
        //UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:selectVC];
        
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [UIView transitionWithView:appDelegate.window duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void)
         {
             BOOL oldState = [UIView areAnimationsEnabled];
             [UIView setAnimationsEnabled:NO];
             
             [appDelegate.window setRootViewController:selectVC];
             [appDelegate.window makeKeyAndVisible];
             
             [UIView setAnimationsEnabled:oldState];
             
         }completion:nil];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
    
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)logoutPressed:(id)sender
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"logout_alert", nil)
                                                    message:NSLocalizedString(@"logout_message", nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"cancel", nil)
                                          otherButtonTitles:NSLocalizedString(@"yes", nil), nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
        {
            [self logoutFromServer];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            [userDefaults removeObjectForKey:NSUserDefaultKey_USER];
            [ReportDetailsModel clearAllReportDetailKey];
            [self switchToDashboard:@"LoginViewController"];
        }
            break;
    }
}

- (void)logoutFromServer
{
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:nil];
    [asyncApi logoutFromServer:nil];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_PAGE_DATA_CALL_ID])
    {
        //[self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
            NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
            BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
            BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
            
            if (isOwner || isAdmin)
            {
                NSMutableDictionary *content = [AppUtil removeNullElementFromObject:result.result];
                [self refreshBadge:content];
            }
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            //[alert show];
            //return;
        }
    }
}

@end
