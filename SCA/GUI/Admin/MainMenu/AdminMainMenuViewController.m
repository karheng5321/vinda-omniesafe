//
//  AdminMainMenuViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 03/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AdminMainMenuViewController.h"
#import "AdminMainMenuTableViewCell.h"
#import "AdminDetailsViewController.h"
#import "LoginViewController.h"
#import "ReportDetailsViewController.h"
#import "AsyncApi.h"
#import "AppUtil.h"

@interface AdminMainMenuViewController () <AsyncApiDelegate> {
    UIView *_loadingView;
    
    NSInteger _selectedRow;
    
    UISegmentedControl *_segmentControl;
}

@property (weak, nonatomic) IBOutlet UITableView *reportTable;
@property (weak, nonatomic) IBOutlet UIButton *areaButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableTitleBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSectionHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation AdminMainMenuViewController

static NSString * const GET_REPORT_CALL_ID = @"getreport";

NSMutableArray *adminReportArray;

- (void) setupLayout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    float extraSpace = isAdmin? 50.0:0.0;
    
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
        self.tableTitleBarHeightConstraint.constant = 50;
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        self.tableTitleBarHeightConstraint.constant = 50;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.topBarHeightConstraint.constant = 120;
        self.tableTitleBarHeightConstraint.constant = 30;
    }
    
    self.topSectionHeightConstraint.constant += extraSpace;
    
    if (isAdmin)
    {
        self.titleLabel.text = NSLocalizedString(@"all_history", nil);
        
        _segmentControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"all_area", nil), NSLocalizedString(@"my_area", nil), nil]];
        _segmentControl.frame = CGRectMake(5.0, self.headerView.frame.origin.y+5.0, self.headerView.frame.size.width-10.0, 40.0);
        _segmentControl.tintColor = self.titleLabel.superview.backgroundColor;
        _segmentControl.momentary = NO;
        _segmentControl.selectedSegmentIndex = 0;
        [_segmentControl addTarget:self action:@selector(segmentedControlChanged:) forControlEvents:UIControlEventValueChanged];
        [self.headerView.superview addSubview:_segmentControl];
        
    }
    else
    {
        self.titleLabel.text = NSLocalizedString(@"area_history", nil);
    }
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    _loadingView.center = self.view.center;//CGPointMake(_screenWidth/2.0, _screenHeight/2.0);
    _loadingView.layer.cornerRadius = 10.0;
    _loadingView.hidden = YES;
    [self.view addSubview:_loadingView];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
    [_loadingView addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self downloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

#pragma mark - UIButtons Actions

- (IBAction)backPressed:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)areaPressed:(id)sender
{
    NSLog(@"area pressed");
}

- (void)segmentedControlChanged:(UISegmentedControl *)sender
{
    [self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    if (sender.selectedSegmentIndex == 0)
        [asyncApi getAdminReportHistory:GET_REPORT_CALL_ID];
    else
        [asyncApi getAreaReportHistory:GET_REPORT_CALL_ID];
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Some calculation to determine height of row/cell.
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return adminReportArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AdminMainMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdminReportHistoryCell"];
    
    [cell updateDisplay:adminReportArray[indexPath.row]];
    
    ReportDetailsModel *model = adminReportArray[indexPath.row];
    
    cell.rightarrowImage.hidden = YES;
    
    UIView *view = [cell.contentView viewWithTag:999];
    if (view)
        [view removeFromSuperview];
    //NSLog(@"model.status: %@", model.status);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
    label.backgroundColor = ([model.status isEqualToString:NSLocalizedString(@"new", nil)])? [UIColor blueColor]: ([model.status rangeOfString:NSLocalizedString(@"overdue", nil)].length)? [UIColor redColor]:[UIColor orangeColor];
    label.textColor = [UIColor whiteColor];
    label.text = ([model.status isEqualToString:NSLocalizedString(@"new", nil)])? [model.status uppercaseString]:model.status;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont italicSystemFontOfSize:12.0];
    [label sizeToFit];
    label.frame = CGRectMake(tableView.frame.size.width-5.0-label.frame.size.width, 5.0, label.frame.size.width, label.frame.size.height);
    label.tag = 999;
    label.font = [UIFont italicSystemFontOfSize:10.0];
    [cell.contentView addSubview:label];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self setGreyWhiteCellColor:cell row:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self performSegueWithIdentifier:@"gotoAdminReportDetailsView" sender:nil];
    _selectedRow = indexPath.row;
    
    [self performSegueWithIdentifier:@"gotoReportDetailsView" sender:nil];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*if ([segue.identifier isEqualToString:@"gotoAdminReportDetailsView"])
    {
        AdminDetailsViewController *destinationVC = segue.destinationViewController;
        NSIndexPath *myIndexPath = [self.reportTable indexPathForSelectedRow];
        int row = (int)[myIndexPath row];
        destinationVC.detailsModel = [adminReportArray objectAtIndex: row];
    }*/
    
    if ([segue.identifier isEqualToString:@"gotoReportDetailsView"])
    {
        ReportDetailsViewController *destinationVC = segue.destinationViewController;
        ReportDetailsModel *report = [adminReportArray objectAtIndex:_selectedRow];
        destinationVC.reportNo = report.reportNo;
        destinationVC.ownerId = report.areaOwner;
        destinationVC.fromNotification = NO;
        destinationVC.sameSite = NO;
    }
}

#pragma mark - Private

- (void)downloadData
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    //BOOL isUser = [[roles objectForKey:ROLES_USER_KEY] boolValue];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    BOOL isMonitor = [[roles objectForKey:ROLES_MONITOR_KEY] boolValue];
    //BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    
    [self setLoadingHidden:NO];
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    if (isAdmin || isMonitor)
    {
        if (_segmentControl.selectedSegmentIndex == 0)
            [asyncApi getAdminReportHistory:GET_REPORT_CALL_ID];
        else
            [asyncApi getAreaReportHistory:GET_REPORT_CALL_ID];
    }
    else
        [asyncApi getAreaReportHistory:GET_REPORT_CALL_ID];
}

- (void)processData:(NSMutableArray *)array
{
    adminReportArray = [NSMutableArray new];
    
    for (NSInteger i = 0; i < [array count]; i++) {
        NSDictionary *dict = [array objectAtIndex:i];
        dict = [AppUtil removeNullElementFromObject:dict];
        
        NSString *timestampString = [dict valueForKey:@"reported_timestamp"];
        //NSArray *timestampArray = [timestampString componentsSeparatedByString:@" "];
        //NSString *dateString = [timestampArray objectAtIndex:0];
        //NSLog(@"dateString: <%@>", dateString);
        //NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //[dateFormatter setDateFormat:@"yyyy-MM-dd"];
        //NSDate *reportDate = [dateFormatter dateFromString:dateString];
        //NSLog(@"reportDate: %@", reportDate);
        //[dateFormatter setDateFormat:@"dd/MM/yyyy"];
        //NSString *submitDateString = [dateFormatter stringFromDate:reportDate];
        //NSLog(@"submitDateString: %@", submitDateString);
        
        ReportDetailsModel *report = [ReportDetailsModel new];
        report.reportNo = [dict valueForKey:@"id"];
        report.userName = [dict valueForKey:@"reporter_uacc_id"];
        report.submitterName = [dict valueForKey:@"reporter_uacc_id"];
        report.dateTime = timestampString;//submitDateString;
        report.hazardType = [dict valueForKey:@"hazard_name"];
        report.areaLocation = [dict valueForKey:@"area_name"];
        report.areaOwner = [dict valueForKey:@"assigned_uacc_id"];
        report.criticallyType = [dict valueForKey:@"critical_level"];
        report.remark = [dict valueForKey:@"status"];
        report.incidents = report.remark;
        
        NSInteger status = [[dict valueForKey:@"status"] integerValue];
        
        switch (status) {
            case 1:
                report.status = NSLocalizedString(@"new", nil);
                break;
            case 2:
                report.status = NSLocalizedString(@"overdue", nil);
                break;
            case 3:
                report.status = NSLocalizedString(@"pending", nil);
                break;
            case 4:
                report.status = NSLocalizedString(@"reopen", nil);
                break;
            case 5:
                report.status = NSLocalizedString(@"reopen_overdue", nil);
                break;
            case 6:
                report.status = NSLocalizedString(@"closed", nil);
                break;
            case 7:
                report.status = NSLocalizedString(@"reopen_closed", nil);
                break;
            default:
                break;
        }
        
        NSString *picUrl = [dict valueForKey:@"uploaded_pic_thumbnail_url"];
        NSString *videoUrl = [dict valueForKey:@"uploaded_vid_url"];
        
        report.uploadPicVidArray = [NSMutableArray new];
        if (picUrl.length)
            [report.uploadPicVidArray addObject:picUrl];
        if (videoUrl.length)
            [report.uploadPicVidArray addObject:videoUrl];
        
        report.afterActionPicVidArray = report.uploadPicVidArray;
        
        [adminReportArray addObject:report];
    }
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_REPORT_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            NSMutableArray *array = (NSMutableArray *)result.result;
            [self processData:array];
            [self.reportTable reloadData];
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
