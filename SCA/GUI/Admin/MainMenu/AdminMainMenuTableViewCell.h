//
//  AdminMainMenuTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 04/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportDetailsModel.h"


@interface AdminMainMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportnoLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightarrowImage;

- (void) updateDisplay:(ReportDetailsModel *)model;

@end
