//
//  AdminMainMenuTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 04/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AdminMainMenuTableViewCell.h"
#import "GeneralHelper.h"

@interface AdminMainMenuTableViewCell()

@end

@implementation AdminMainMenuTableViewCell


- (void) updateDisplay:(ReportDetailsModel *)model
{
    [self.dateLabel setText: model.dateTime];
    [self.reportnoLabel setText: model.reportNo];
    [self.usernameLabel setText: model.submitterName];
    
    self.dateLabel.font = [UIFont systemFontOfSize:13];
    self.reportnoLabel.font = [UIFont systemFontOfSize:13];
    self.usernameLabel.font = [UIFont systemFontOfSize:13];
    //self.dateLabel.numberOfLines = 0;
    //[self.dateLabel sizeToFit];
}


@end
