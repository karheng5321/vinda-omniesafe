//
//  AdminRemarkViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 03/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AdminRemarkViewController.h"
#import "AdminMainMenuViewController.h"
#import "ReportDetailsModel.h"

@interface AdminRemarkViewController ()

@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UILabel *logoutInstructionLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remarkTextViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remarkLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextView *remarkTextView;
@end

@implementation AdminRemarkViewController

- (void) setupLayout {
    
    if ([GeneralHelper isDeviceiPhone4]) {
        self.topBarHeightConstraint.constant = 110;
        self.remarkLabelTopConstraint.constant = 20;
        self.remarkTextViewHeightConstraint.constant = 155;
    }
    else if ([GeneralHelper isDeviceiPhone6]) {
        self.topBarHeightConstraint.constant = 120;
        self.remarkLabelTopConstraint.constant = 39;
        self.remarkTextViewHeightConstraint.constant = 185;
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    [self roundButton: self.logoutButton];
    [self.logoutInstructionLabel setText:SUBMIT_INSTRUCTION];
    [self setTextViewLayout: self.remarkTextView];
    self.remarkTextView.delegate = self;
    // Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    // register for keyboard notifications
    if ([GeneralHelper isDeviceiPhone4] || [GeneralHelper isDeviceiPhone5])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(viewMovingUp:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(viewMovingDown)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions
- (IBAction)backTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)logoutTapped:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"THANK YOU!"
                                                    message:@"Your report has successfully closed."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    //send to server? the remark is saved in NSDefault
    [ReportDetailsModel clearAllReportDetailKey];

}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // the user clicked OK
    if (buttonIndex == 0)
    {
        [self switchToDashboard:@"AdminMainMenuView"];
    }
}


#pragma mark - UITextView Delegate

- (void) viewMovingUp:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    CGRect frame = self.view.frame;
    frame.origin.y =  keyboardFrameBeginRect.size.height - 280;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];
}

- (void) viewMovingDown
{
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    self.remarkTextView.text = @"";
    self.remarkTextView.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(self.remarkTextView.text.length == 0)
    {
        self.remarkTextView.textColor = [UIColor lightGrayColor];
        self.remarkTextView.text = @"Type your description";
        [self.remarkTextView resignFirstResponder];
    }
    else {
        [ReportDetailsModel saveValue:self.remarkTextView.text key:NSUSERDEFAULT_KEY_REMARK];
    }
}

-(void)dismissKeyboard {
    [self.remarkTextView resignFirstResponder];
}
@end
