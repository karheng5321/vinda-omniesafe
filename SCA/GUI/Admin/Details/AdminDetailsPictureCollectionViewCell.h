//
//  AdminDetailsPictureCollectionViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 09/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdminDetailsPictureCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *picvidImage;

@end
