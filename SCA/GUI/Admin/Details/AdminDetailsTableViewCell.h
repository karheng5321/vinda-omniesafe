//
//  AdminDetailsTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 04/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdminDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *detailslhsLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsrhsLabel;

@end
