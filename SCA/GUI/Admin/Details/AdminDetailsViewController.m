//
//  AdminDetailsViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 03/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AdminDetailsViewController.h"
#import "AdminDetailsTableViewCell.h"
#import "AdminRemarkViewController.h"
#import "AdminDetailsPictureTableViewCell.h"
#import "AdminDetailsPictureCollectionViewCell.h"

@interface AdminDetailsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UITableView *detailsTable;
@property (weak, nonatomic) IBOutlet UIButton *closeReportButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;

@end

@implementation AdminDetailsViewController
NSArray *detailslhsArray;
NSArray *detailsrhsArray;

- (void) setupLayout
{
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
        [self.closeReportButton setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-50, 35)];
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        [self.closeReportButton setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-50, 40)];
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.topBarHeightConstraint.constant = 120;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
    [self roundButton: self.closeReportButton];
    [self.detailsLabel setText: self.detailsModel.reportNo];
    detailslhsArray = @[USERNAME_LABEL, DATETIME_LABEL, HAZARD_LABEL, AREALOCATION_LABEL, AREAOWNER_LABEL, INCIDENTS_LABEL, PICTUREVIDEO_LABEL, REMARK_LABEL, STATUS_LABEL, AFTERACTION_LABEL];
    detailsrhsArray = @[self.detailsModel.userName, self.detailsModel.dateTime, self.detailsModel.hazardType, self.detailsModel.areaLocation, self.detailsModel.areaOwner, self.detailsModel.incidents, self.detailsModel.uploadPicVidArray, self.detailsModel.remark, self.detailsModel.status, self.detailsModel.afterActionPicVidArray];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions
- (IBAction)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeReportTapped:(id)sender
{
    [self performSegueWithIdentifier:@"goToAdminRemark" sender:nil];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goToAdminRemark"])
    {
        AdminRemarkViewController *destinationVC = segue.destinationViewController;
    }
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Some calculation to determine height of row/cell.
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return detailslhsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == detailslhsArray.count - 4)
    {
        AdminDetailsPictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdminPicVidCell"];
        [cell.picvidLabel setText: PICTUREVIDEO_LABEL];
        [cell.picvidCollectionView setBackgroundColor:[UIColor clearColor]];
        return cell;
    }
    else if (indexPath.row == detailslhsArray.count - 1)
    {
        AdminDetailsPictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdminPicVidCell"];
        [cell.picvidLabel setText: AFTERACTION_LABEL];
        [cell.picvidCollectionView setBackgroundColor:[UIColor clearColor]];
        return cell;
    }
    else
    {
        AdminDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdminDetailsCell"];
        [cell.detailslhsLabel setText: [detailslhsArray objectAtIndex: indexPath.row]];
        [cell.detailsrhsLabel setText: [detailsrhsArray objectAtIndex: indexPath.row]];
        return cell;
    }
}

@end
