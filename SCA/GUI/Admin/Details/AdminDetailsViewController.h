//
//  AdminDetailsViewController.h
//  SCA
//
//  Created by Ngo Yen Sern on 03/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "BaseTableViewController.h"

@interface AdminDetailsViewController : BaseTableViewController
@property (nonatomic,strong) ReportDetailsModel *detailsModel;
@end
