//
//  AreaOwnerCriticallyTypeTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerCriticallyTypeTableViewCell.h"
#import "GeneralHelper.h"

@interface AreaOwnerCriticallyTypeTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *criticallytypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *booleanImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *booleanImageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *booleanImageWidthConstraint;


@end

@implementation AreaOwnerCriticallyTypeTableViewCell

- (void) updateDisplay:(CriticallyTypeModel *)model
{
    
    if ([GeneralHelper isDeviceiPhone4]) {
        self.booleanImageHeightConstraint.constant = 20;
        self.booleanImageWidthConstraint.constant = 20;
    }
    else if ([GeneralHelper isDeviceiPhone6]) {
        self.booleanImageHeightConstraint.constant = 30;
        self.booleanImageWidthConstraint.constant = 30;
    }
    
    [self.criticallytypeLabel setText: model.criticallyName];
    
    if (model.isChecked)
    {
        [self.booleanImage setImage: [UIImage imageNamed:@"radio_black"]];
    }
    else
    {
        [self.booleanImage setImage: [UIImage imageNamed:@"radio_empty"]];
    }
    
    
}

@end
