//
//  AreaOwnerCriticallyTypeTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CriticallyTypeModel.h"

@interface AreaOwnerCriticallyTypeTableViewCell : UITableViewCell

- (void) updateDisplay:(CriticallyTypeModel *)model;

@end
