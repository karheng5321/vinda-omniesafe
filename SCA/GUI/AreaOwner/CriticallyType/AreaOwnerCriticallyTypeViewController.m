//
//  AreaOwnerCriticallyTypeViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerCriticallyTypeViewController.h"
#import "AreaOwnerCriticallyTypeTableViewCell.h"
#import "CriticallyTypeModel.h"

@interface AreaOwnerCriticallyTypeViewController ()

@end

@implementation AreaOwnerCriticallyTypeViewController


NSMutableArray *areaOwnerCriticallyTypeArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    areaOwnerCriticallyTypeArray = [NSMutableArray new];
    
    CriticallyTypeModel *sample1 = [CriticallyTypeModel new];
    sample1.criticallyName = @"Medium";
    sample1.isChecked = NO;
    
    CriticallyTypeModel *sample2 = [CriticallyTypeModel new];
    sample2.criticallyName = @"High";
    sample2.isChecked = NO;
    
    [areaOwnerCriticallyTypeArray addObjectsFromArray:@[sample1, sample2]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Some calculation to determine height of row/cell.
    return 53;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return areaOwnerCriticallyTypeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AreaOwnerCriticallyTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaOwnerCriticallyTypeCell"];
    
    [cell updateDisplay:areaOwnerCriticallyTypeArray[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setGreyWhiteCellColor:cell row:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (CriticallyTypeModel *model in areaOwnerCriticallyTypeArray)
    {
        model.isChecked = NO;
    }
    
    CriticallyTypeModel *model = (CriticallyTypeModel *)areaOwnerCriticallyTypeArray[indexPath.row];
    model.isChecked = !model.isChecked;
    
    [ReportDetailsModel saveValue: model.criticallyName key:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
    [tableView reloadData];
    
}

@end
