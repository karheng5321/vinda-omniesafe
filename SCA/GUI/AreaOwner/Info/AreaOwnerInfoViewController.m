//
//  AreaOwnerInfoViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 08/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerInfoViewController.h"
#import "AreaOwnerContainerViewController.h"
#import "AreaOwnerSubmitViewController.h"


@interface AreaOwnerInfoViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *photovideoLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeofhazardLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeofCritically;
@property (nonatomic, weak) AreaOwnerContainerViewController *containerViewController;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomBarHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *photoVideoBulletView;
@property (weak, nonatomic) IBOutlet UIView *actionTakenBulletView;
@property (weak, nonatomic) IBOutlet UIView *hazardTypeBulletView;
@property (weak, nonatomic) IBOutlet UIView *criticallyTypeBulletView;

@property (weak, nonatomic) IBOutlet UIView *criticallyToPhotoLineView;
@property (weak, nonatomic) IBOutlet UIView *actionTakenToHazardLineView;
@property (weak, nonatomic) IBOutlet UIView *photoToActionLineView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoActionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoCriticallyConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *actionHazardConstraint;

@end

@implementation AreaOwnerInfoViewController

- (void) setupLayout
{
    CGFloat gapSpace = ([[UIScreen mainScreen] bounds].size.width-(4*20)-(2*20))/3;
    self.photoActionConstraint.constant = gapSpace;
    self.photoCriticallyConstraint.constant = gapSpace;
    self.actionHazardConstraint.constant = gapSpace;
    
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
        self.bottomBarHeightConstraint.constant = 45;
        [self.nextButton setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-50, 35)];
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        self.bottomBarHeightConstraint.constant = 55;
        [self.nextButton setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-50, 40)];
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.topBarHeightConstraint.constant = 120;
    }
    
    //Setting bullet layout
    [self.criticallyTypeBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
    
    self.criticallyTypeBulletView.layer.borderWidth = 1.0f;
    self.criticallyTypeBulletView.layer.borderColor = [UIColor blackColor].CGColor;
    self.criticallyTypeBulletView.layer.cornerRadius = self.criticallyTypeBulletView.frame.size.width/2;
    self.criticallyTypeBulletView.layer.masksToBounds = YES;
    
    self.photoVideoBulletView.layer.borderWidth = 1.0f;
    self.photoVideoBulletView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.photoVideoBulletView.layer.cornerRadius = self.photoVideoBulletView.frame.size.width/2;
    self.photoVideoBulletView.layer.masksToBounds = YES;
    
    self.hazardTypeBulletView.layer.borderWidth = 1.0f;
    self.hazardTypeBulletView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.hazardTypeBulletView.layer.cornerRadius = self.hazardTypeBulletView.frame.size.width/2;
    self.hazardTypeBulletView.layer.masksToBounds = YES;
    
    self.actionTakenBulletView.layer.borderWidth = 1.0f;
    self.actionTakenBulletView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.actionTakenBulletView.layer.cornerRadius = self.actionTakenBulletView.frame.size.width/2;
    self.actionTakenBulletView.layer.masksToBounds = YES;
    
    self.photoVideoBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.criticallyTypeBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.hazardTypeBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.actionTakenBulletView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    
    
    self.criticallyToPhotoLineView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.criticallyToPhotoLineView.layer.borderWidth = 0.5f;
    self.actionTakenToHazardLineView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.actionTakenToHazardLineView.layer.borderWidth = 0.5f;
    self.photoToActionLineView.layer.borderColor = [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX].CGColor;
    self.photoToActionLineView.layer.borderWidth = 0.5f;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
    [self roundButton: self.nextButton];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)photoPressed:(id)sender
{
    [self.containerViewController gotoPhotoVideo];
    [self.titleLabel setText: @"PHOTO / VIDEO"];
}

- (IBAction)actiontakenPressed:(id)sender
{
    [self.containerViewController gotoActionTaken];
    [self.titleLabel setText: @"ACTION TAKEN"];
}

- (IBAction)hazardPressed:(id)sender
{
    [self.containerViewController gotoHazardType];
    [self.titleLabel setText: @"TYPE OF HAZARD"];}

- (IBAction)criticallyPressed:(id)sender {
    
    [self.containerViewController gotoCriticallyType];
    [self.titleLabel setText: @"TYPE OF CRITICALLY"];
}

- (IBAction)nextPressed:(id)sender {
    
    ReportDetailsModel *submittingModel = [ReportDetailsModel new];
    submittingModel = [ReportDetailsModel getReportDetailInfo];
    
    NSLog(@"submitterName  -> %@",submittingModel.submitterName);
    NSLog(@"dateTime       -> %@",submittingModel.dateTime);
    NSLog(@"remark         -> %@",submittingModel.remark);
    NSLog(@"areaOwner      -> %@",submittingModel.areaOwner);
    NSLog(@"criticallyType -> %@",submittingModel.criticallyType);
    NSLog(@"hazardType     -> %@",submittingModel.hazardType);
    NSLog(@"image          -> %@",submittingModel.uploadPicVidArray);
    
    NSString *vcName = self.containerViewController.childViewControllers.firstObject.restorationIdentifier;
    if ([vcName isEqualToString: @"AreaOwnerCriticallyTypeViewController"]) {
        if ([ReportDetailsModel getValue:NSUSERDEFAULT_KEY_CRITICALLY_TYPE].length == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please select the critically."
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else {
            [self photoPressed: nil];
            [self.photoVideoBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.criticallyToPhotoLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
        }
    }
    else if ([vcName isEqualToString: @"AreaOwnerPhotoVideoViewController"]) {
    
        if ([ReportDetailsModel getMutableArray:NSUSERDEFAULT_KEY_AFTERPICVIDFILE].count == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Media Not Found" message:@"Are you sure to proceed?" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
        }
        else {
            [self actiontakenPressed: nil];
            [self.actionTakenBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.photoToActionLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
        }
    }
    else if ([vcName isEqualToString: @"AreaOwnerActionTakenViewController"]) {
        if ([ReportDetailsModel getValue:NSUSERDEFAULT_KEY_REMARK].length == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Action Taken Field is Empty" message:@"Please fill in the field."
                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else {
            [self hazardPressed: nil];
            [self.hazardTypeBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.actionTakenToHazardLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
        }
    }
    else if ([vcName isEqualToString: @"AreaOwnerHazardTypeViewController"]) {
        if ([ReportDetailsModel getValue:NSUSERDEFAULT_KEY_HAZARDTYPE].length == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hazard Empty" message:@"Please select the hazard type." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else {
            
            ReportDetailsModel *model = [ReportDetailsModel getReportDetailInfo];
            if (model.afterActionPicVidArray == nil || model.remark == nil || model.hazardType == nil || model.criticallyType == nil) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Insufficient Information" message:@"Please complete all the field." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }
            else {
                [self performSegueWithIdentifier:@"goToAreaOwnerSubmit" sender:nil];
            }
        }
    }
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if ([segue.identifier isEqualToString:@"embedAreaOwnerContainer"])
    {
        self.containerViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:@"goToAreaOwnerSubmit"]) {
        AreaOwnerSubmitViewController *destinationVC = segue.destinationViewController;
        
        [ReportDetailsModel saveValue:[GeneralHelper getCurrentDateTime] key:NSUSERDEFAULT_KEY_DATE_TIME];

        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
            [self actiontakenPressed: nil];
            [self.actionTakenBulletView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            [self.photoToActionLineView setBackgroundColor: [self colorWithHexString:BULLET_BG_FILLED_COLOR_HEX]];
            break;
    }
}

@end
