//
//  AreaOwnerContainerViewController.h
//  SCA
//
//  Created by Ngo Yen Sern on 08/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AreaOwnerContainerViewController : UIViewController

- (void)gotoPhotoVideo;
- (void)gotoActionTaken;
- (void)gotoHazardType;
- (void)gotoCriticallyType;

@end
