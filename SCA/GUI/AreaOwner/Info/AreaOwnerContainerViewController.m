//
//  AreaOwnerContainerViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 08/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerContainerViewController.h"
#import "AreaOwnerPhotoVideoViewController.h"
#import "AreaOwnerActionTakenViewController.h"
#import "AreaOwnerHazardTypeViewController.h"
#import "AreaOwnerCriticallyTypeViewController.h"

#define SegueIdentifierPhotoVideo @"embedAreaOwnerPhotoVideo"
#define SegueIdentifierActionTaken @"embedAreaOwnerActionTaken"
#define SegueIdentifierHazardType @"embedAreaOwnerHazardType"
#define SegueIdentifierCriticallyType @"embedAreaOwnerCriticallyType"

@interface AreaOwnerContainerViewController ()

@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (strong, nonatomic) AreaOwnerPhotoVideoViewController *AreaOwnerPhotoVideoViewController;
@property (strong, nonatomic) AreaOwnerActionTakenViewController *AreaOwnerActionTakenViewController;
@property (strong, nonatomic) AreaOwnerHazardTypeViewController *AreaOwnerHazardTypeViewController;
@property (strong, nonatomic) AreaOwnerCriticallyTypeViewController *AreaOwnerCriticallyTypeViewController;
@property (assign, nonatomic) BOOL transitionInProgress;


@end

@implementation AreaOwnerContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.transitionInProgress = NO;
    self.currentSegueIdentifier = SegueIdentifierCriticallyType;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // Instead of creating new VCs on each seque we want to hang on to existing
    // instances if we have it. Remove the second condition of the following
    // two if statements to get new VC instances instead.
    if ([segue.identifier isEqualToString:SegueIdentifierPhotoVideo])
    {
        self.AreaOwnerPhotoVideoViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueIdentifierActionTaken])
    {
        self.AreaOwnerActionTakenViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueIdentifierHazardType])
    {
        self.AreaOwnerHazardTypeViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:SegueIdentifierCriticallyType])
    {
        self.AreaOwnerCriticallyTypeViewController = segue.destinationViewController;
    }
    
    // If we're going to the first view controller.
    if ([segue.identifier isEqualToString:SegueIdentifierCriticallyType])
    {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.AreaOwnerCriticallyTypeViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    
    // By definition the second view controller will always be swapped with the first one.
    else if ([segue.identifier isEqualToString:SegueIdentifierActionTaken])
    {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.AreaOwnerActionTakenViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }

    }
    else if ([segue.identifier isEqualToString:SegueIdentifierHazardType])
    {
    
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.AreaOwnerHazardTypeViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }

    }
    else if ([segue.identifier isEqualToString:SegueIdentifierPhotoVideo])
    {// If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.AreaOwnerPhotoVideoViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0.1 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        self.transitionInProgress = NO;
    }];
}

- (void)gotoPhotoVideo
{
    self.currentSegueIdentifier = SegueIdentifierPhotoVideo;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)gotoActionTaken
{
    self.currentSegueIdentifier = SegueIdentifierActionTaken;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)gotoHazardType
{
    self.currentSegueIdentifier = SegueIdentifierHazardType;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}


- (void)gotoCriticallyType
{
    self.currentSegueIdentifier = SegueIdentifierCriticallyType;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

@end
