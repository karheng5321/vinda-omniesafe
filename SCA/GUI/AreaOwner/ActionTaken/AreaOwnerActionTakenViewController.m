//
//  AreaOwnerActionTakenViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerActionTakenViewController.h"
#import "ReportDetailsModel.h"

@interface AreaOwnerActionTakenViewController ()
@property (weak, nonatomic) IBOutlet UITextView *actiontakentextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *actionTakenTextViewHeightConstraint;

@end

@implementation AreaOwnerActionTakenViewController

- (void)setupLayout {
    if ([GeneralHelper isDeviceiPhone4]) {
        self.actionTakenTextViewHeightConstraint.constant = 155;
        
    }
    else if ([GeneralHelper isDeviceiPhone6]) {
        self.actionTakenTextViewHeightConstraint.constant = 185;
        
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTextViewLayout: self.actiontakentextView];
    self.actiontakentextView.text = @"Type your action taken here...";
    self.actiontakentextView.delegate = self;
    // Do any additional setup after loading the view.
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewMovingUp:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewMovingDown)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextView Delegate

- (void) viewMovingUp:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    CGRect frame = self.view.frame;
    frame.origin.y =  keyboardFrameBeginRect.size.height - 280;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];
}

- (void) viewMovingDown
{
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.view.frame = frame;
    }];
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    self.actiontakentextView.text = @"";
    self.actiontakentextView.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(self.actiontakentextView.text.length == 0)
    {
        self.actiontakentextView.textColor = [UIColor lightGrayColor];
        self.actiontakentextView.text = @"Type your action taken here...";
        [self.actiontakentextView resignFirstResponder];
    }
    else {
        [ReportDetailsModel saveValue: self.actiontakentextView.text key:NSUSERDEFAULT_KEY_REMARK];
    }
}

-(void)dismissKeyboard {
    [self.actiontakentextView resignFirstResponder];
}

@end
