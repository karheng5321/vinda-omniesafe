//
//  AreaOwnerMenuSelectionViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 10/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerMenuSelectionViewController.h"

@interface AreaOwnerMenuSelectionViewController ()
@property (weak, nonatomic) IBOutlet UIButton *reportdangerButton;
@property (weak, nonatomic) IBOutlet UIButton *reporthistoryButton;
@property (weak, nonatomic) IBOutlet UIButton *areahistoryButton;

@end

@implementation AreaOwnerMenuSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self roundButton: self.reportdangerButton];
    [self roundButton: self.reporthistoryButton];
    [self roundButton: self.areahistoryButton];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (IBAction)logoutPressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:@"Are you sure you want to log out from OMNISAFE?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
            [self switchToDashboard:@"LoginViewController"];
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
