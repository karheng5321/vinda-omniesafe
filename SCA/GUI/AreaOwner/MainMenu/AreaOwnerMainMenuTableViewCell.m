//
//  AreaOwnerMainMenuTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerMainMenuTableViewCell.h"

@interface AreaOwnerMainMenuTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportnoLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@end

@implementation AreaOwnerMainMenuTableViewCell


- (void) updateDisplay:(ReportDetailsModel *)model
{
    [self.dateLabel setText: model.dateTime];
    [self.reportnoLabel setText: model.reportNo];
    [self.usernameLabel setText: model.userName];
}



@end
