//
//  AreaOwnerMainMenuTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportDetailsModel.h"

@interface AreaOwnerMainMenuTableViewCell : UITableViewCell

- (void) updateDisplay:(ReportDetailsModel *)model;

@end
