//
//  AreaOwnerMainMenuViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerMainMenuViewController.h"
#import "AreaOwnerMainMenuTableViewCell.h"
#import "AreaOwnerReportViewController.h"

@interface AreaOwnerMainMenuViewController ()
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UITableView *reportTable;

@end

@implementation AreaOwnerMainMenuViewController

NSMutableArray *areaownerReportArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self roundButton: self.logoutButton];
    
    areaownerReportArray = [NSMutableArray new];
    
    ReportDetailsModel *sample1 = [ReportDetailsModel new];
    sample1.userName = @"Harry";
    sample1.submitterName = @"Harry";
    sample1.dateTime = @"12/12/12";
    sample1.reportNo = @"23123124";
    sample1.hazardType = @"Fire";
    sample1.areaLocation = @"KL";
    sample1.areaOwner = @"MBPJ";
    sample1.remark = @"Dirty air";
    sample1.incidents = @"Dirty air";
    sample1.criticallyType = @"high";
    sample1.status = @"Dirty air";
    sample1.remark = @"Dirty air";
    sample1.uploadPicVidArray = [NSMutableArray new];
    [sample1.uploadPicVidArray addObject: UIImagePNGRepresentation([UIImage imageNamed:@"down_arrow"])];
    
    
    ReportDetailsModel *sample2 = [ReportDetailsModel new];
    sample2.dateTime = @"1/1/2016";
    sample2.reportNo = @"Electrical";
    sample2.userName = @"Minor";
    
    ReportDetailsModel *sample3 = [ReportDetailsModel new];
    sample3.dateTime = @"24/12/2015";
    sample3.userName = @"Ergonomic";
    sample3.reportNo = @"Major";
    
    [areaownerReportArray addObjectsFromArray:@[sample1, sample2, sample3]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (IBAction)logoutPressed:(id)sender {
    [self switchToDashboard:@"LoginViewController"];
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Some calculation to determine height of row/cell.
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return areaownerReportArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AreaOwnerMainMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaOwnerMainMenuCell"];
    
    [cell updateDisplay:areaownerReportArray[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setGreyWhiteCellColor:cell row:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"goToAreaOwnerReportView" sender:nil];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goToAreaOwnerReportView"])
    {
        AreaOwnerReportViewController *destinationVC = segue.destinationViewController;
        NSIndexPath *myIndexPath = [self.reportTable indexPathForSelectedRow];
        int row = (int)[myIndexPath row];
        ReportDetailsModel *destModel = [areaownerReportArray objectAtIndex: row];
        
        [ReportDetailsModel saveValue:destModel.reportNo key:NSUSERDEFAULT_KEY_REPORTNO];
        [ReportDetailsModel saveValue:destModel.submitterName key:NSUSERDEFAULT_KEY_SUBMITTER_NAME];
        [ReportDetailsModel saveValue:destModel.userName key:NSUSERDEFAULT_KEY_USERNAME];
        [ReportDetailsModel saveValue:destModel.dateTime key:NSUSERDEFAULT_KEY_DATE_TIME];
        [ReportDetailsModel saveValue:destModel.hazardType key:NSUSERDEFAULT_KEY_HAZARDTYPE];
        [ReportDetailsModel saveValue:destModel.areaLocation key:NSUSERDEFAULT_KEY_AREA_LOCATION];
        [ReportDetailsModel saveValue:destModel.areaOwner key:NSUSERDEFAULT_KEY_AREA_OWNER];
        [ReportDetailsModel saveValue:destModel.criticallyType key:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
        [ReportDetailsModel saveValue:destModel.remark key:NSUSERDEFAULT_KEY_REMARK];
        [ReportDetailsModel saveValue:destModel.status key:NSUSERDEFAULT_KEY_STATUS];
        [ReportDetailsModel saveValue:destModel.incidents key:NSUSERDEFAULT_KEY_INCIDENTS];
        [ReportDetailsModel saveArray:destModel.uploadPicVidArray key: NSUSERDEFAULT_KEY_PICVIDFILE];
        
    
    }
}


@end
