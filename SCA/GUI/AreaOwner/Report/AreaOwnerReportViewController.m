//
//  AreaOwnerReportViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerReportViewController.h"
#import "AreaOwnerReportTableViewCell.h"
#import "AreaOwnerReportPictureTableViewCell.h"
#import "ReportDetailsModel.h"
#import "AreaOwnerReportCollectionViewCell.h"

@interface AreaOwnerReportViewController ()
@property (weak, nonatomic) IBOutlet UILabel *reportnoLabel;
@property (weak, nonatomic) IBOutlet UITableView *reportTable;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation AreaOwnerReportViewController

NSArray *areaOwnerReportlhsArray;
NSArray *areaOwnerReportrhsArray;
ReportDetailsModel *areaOwnerSubmittingModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self roundButton: self.nextButton];
    
    areaOwnerSubmittingModel = [ReportDetailsModel getReportDetailInfo];
    
    [self.reportnoLabel setText: areaOwnerSubmittingModel.reportNo];
    
    areaOwnerReportlhsArray = @[SUBMITTER_LABEL, DATETIME_LABEL, HAZARD_LABEL, AREALOCATION_LABEL, AREAOWNER_LABEL, CRITICALLY_LABEL, REMARK_LABEL, PICTUREVIDEO_LABEL];
    areaOwnerReportrhsArray = @[areaOwnerSubmittingModel.userName, areaOwnerSubmittingModel.dateTime, areaOwnerSubmittingModel.hazardType, areaOwnerSubmittingModel.areaLocation, areaOwnerSubmittingModel.areaOwner, areaOwnerSubmittingModel.criticallyType, areaOwnerSubmittingModel.remark, areaOwnerSubmittingModel.uploadPicVidArray];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return areaOwnerReportlhsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //IF last row, show picture
    if (indexPath.row == areaOwnerReportlhsArray.count - 1) {
        
        AreaOwnerReportPictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaOwnerReportPictureCell"];
        [cell.pictureLabel setText: PICTUREVIDEO_LABEL];
        [cell.picvidCollectionView setBackgroundColor:[UIColor clearColor]];
        return cell;
    }
    else {
        AreaOwnerReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaOwnerReportCell"];
        [cell.areaownerReportlhsLabel setText: [areaOwnerReportlhsArray objectAtIndex: indexPath.row]];
        [cell.areaownerReportrhsLabel setText: [areaOwnerReportrhsArray objectAtIndex: indexPath.row]];
        return cell;
    }
}


#pragma mark - UICollectionView Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return areaOwnerSubmittingModel.uploadPicVidArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AreaOwnerReportCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AreaOwnerReportPicVidCell" forIndexPath:indexPath];
    [cell.picvidImage setImage: [UIImage imageWithData:[areaOwnerSubmittingModel.uploadPicVidArray objectAtIndex:indexPath.row]]];
    return cell;
}


#pragma mark - UIButtons Actions
- (IBAction)backTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
