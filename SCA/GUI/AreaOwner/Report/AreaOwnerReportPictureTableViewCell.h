//
//  AreaOwnerReportPictureTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AreaOwnerReportPictureTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *pictureLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *picvidCollectionView;

@end
