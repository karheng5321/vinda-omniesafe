//
//  AreaOwnerReportCollectionViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AreaOwnerReportCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *picvidImage;

@end
