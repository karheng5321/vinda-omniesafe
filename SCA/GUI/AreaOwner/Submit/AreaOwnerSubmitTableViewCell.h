//
//  SubmitTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AreaOwnerSubmitTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftsideLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightsideLabel;

@end
