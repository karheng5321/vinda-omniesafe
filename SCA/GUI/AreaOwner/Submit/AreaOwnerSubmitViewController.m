//
//  AreaOwnerSubmitViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerSubmitViewController.h"
#import "AreaOwnerSubmitTableViewCell.h"
#import "AreaOwnerPictureTableViewCell.h"
#import "AreaOwnerSubmitCollectionViewCell.h"
#import "ReportDetailsModel.h"

@interface AreaOwnerSubmitViewController ()
@property (weak, nonatomic) IBOutlet UILabel *reportnoLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitTableBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitButtonBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitTableTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitButtonTopConstraint;

@end

@implementation AreaOwnerSubmitViewController

NSArray *areaOwnerSubmitlhsArray;
NSArray *areaOwnerSubmitrhsArray;
ReportDetailsModel *areaOwnerFinalSubmittingModel;;

- (void)setupLayout {
    
    if ([GeneralHelper isDeviceiPhone4]) {
        self.topBarHeightConstraint.constant = 110;
        self.submitTableBottomConstraint.constant = -80;
        self.submitTableTopConstraint.constant = 15;
        self.submitButtonHeightConstraint.constant = 35;
        self.submitButtonBottomConstraint.constant = 2;
        self.submitButtonTopConstraint.constant = 5;
        self.cancelButtonHeightConstraint.constant = 35;
    }
    else if ([GeneralHelper isDeviceiPhone5]) {
        self.submitButtonTopConstraint.constant = 8;
        self.submitTableBottomConstraint.constant = -100;
        self.submitButtonHeightConstraint.constant = 35;
        self.submitButtonBottomConstraint.constant = 10;
        self.submitButtonTopConstraint.constant = 10;
        self.cancelButtonHeightConstraint.constant = 35;
    }
    else if ([GeneralHelper isDeviceiPhone6]) {
        self.topBarHeightConstraint.constant = 120;
        self.submitTableTopConstraint.constant = 15;
        self.submitButtonHeightConstraint.constant = 40;
        self.cancelButtonHeightConstraint.constant = 40;
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
    [self roundButton: self.submitButton];
    [self roundButton: self.cancelButton];
    

    areaOwnerFinalSubmittingModel = [ReportDetailsModel getReportDetailInfo];
    
    [self.reportnoLabel setText: areaOwnerFinalSubmittingModel.reportNo];
    
    areaOwnerSubmitlhsArray = @[USERNAME_LABEL, DATETIME_LABEL, HAZARD_LABEL, AREALOCATION_LABEL, AREAOWNER_LABEL, INCIDENTS_LABEL, PICTUREVIDEO_LABEL, REMARK_LABEL, STATUS_LABEL, AFTERACTION_LABEL];
    areaOwnerSubmitrhsArray = @[areaOwnerFinalSubmittingModel.userName, areaOwnerFinalSubmittingModel.dateTime, areaOwnerFinalSubmittingModel.hazardType, areaOwnerFinalSubmittingModel.areaLocation, areaOwnerFinalSubmittingModel.areaOwner, areaOwnerFinalSubmittingModel.incidents, areaOwnerFinalSubmittingModel.uploadPicVidArray, areaOwnerFinalSubmittingModel.remark, areaOwnerFinalSubmittingModel.status, areaOwnerFinalSubmittingModel.afterActionPicVidArray];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (IBAction)submitTapped:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Report Submitted"
                                                    message:@"Thank you for reporting this hazard"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)cancelTapped:(id)sender
{
    [ReportDetailsModel clearAllReportDetailKey];
    [self switchToDashboard:@"AdminMainMenuView"];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // the user clicked OK
    if (buttonIndex == 0)
    {
        [ReportDetailsModel clearAllReportDetailKey];
        [self switchToDashboard:@"AdminMainMenuView"];
    }
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Some calculation to determine height of row/cell.
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return areaOwnerSubmitlhsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //IF last row, show picture
    if (indexPath.row == areaOwnerSubmitlhsArray.count - 4)
    {
        
        AreaOwnerPictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaOwnerPictureCell"];
        [cell.pictureLabel setText: PICTUREVIDEO_LABEL];
        [cell.picvidCollectionView setBackgroundColor:[UIColor clearColor]];
        return cell;
    }
    else if (indexPath.row == areaOwnerSubmitlhsArray.count - 1)
    {
        AreaOwnerPictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaOwnerPictureCell"];
        [cell.pictureLabel setText: AFTERACTION_LABEL];
        [cell.picvidCollectionView setBackgroundColor:[UIColor clearColor]];
        return cell;
    }
    else
    {
        AreaOwnerSubmitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaOwnerSubmitCell"];
        [cell.leftsideLabel setText: [areaOwnerSubmitlhsArray objectAtIndex: indexPath.row]];
        [cell.rightsideLabel setText: [areaOwnerSubmitrhsArray objectAtIndex: indexPath.row]];
        return cell;
    }
}


#pragma mark - UICollectionView Delegate

//FIXME, cancel and no cancel
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return areaOwnerFinalSubmittingModel.afterActionPicVidArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AreaOwnerSubmitCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AreaOwnerSubmitPicVidCell" forIndexPath:indexPath];
    
    NSData *cameraPicorVid = [areaOwnerFinalSubmittingModel.afterActionPicVidArray objectAtIndex:indexPath.row];
    
    if ([UIImage imageWithData: cameraPicorVid] != nil) {
        [cell.picvidImage setImage: [UIImage imageWithData:cameraPicorVid]];
    }
    else {
        [cell.picvidImage setImage: [GeneralHelper generateThumbImage: areaOwnerFinalSubmittingModel.videoURL]];
    }
    return cell;
}


@end
