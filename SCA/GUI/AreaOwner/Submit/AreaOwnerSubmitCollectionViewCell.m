//
//  AreaOwnerSubmitCollectionViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerSubmitCollectionViewCell.h"
#import "ReportDetailsModel.h"

@implementation AreaOwnerSubmitCollectionViewCell
- (IBAction)cancelTapped:(id)sender {
    [ReportDetailsModel clearAfterActionPicArray];
    [self.picvidImage setImage: nil];
    [self.cancelButton setHidden: TRUE];
}

@end
