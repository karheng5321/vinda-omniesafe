//
//  AreaOwnerHazardTypeViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerHazardTypeViewController.h"
#import "HazardTypeModel.h"
#import "AreaOwnerHazardTypeTableViewCell.h"

@interface AreaOwnerHazardTypeViewController ()

@end

@implementation AreaOwnerHazardTypeViewController

NSMutableArray *areaOwnerHazardTypeArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    areaOwnerHazardTypeArray = [NSMutableArray new];
    
    HazardTypeModel *sample1 = [HazardTypeModel new];
    sample1.hazardName = @"Elimination";
    sample1.isChecked = NO;
    
    HazardTypeModel *sample2 = [HazardTypeModel new];
    sample2.hazardName = @"Reduction";
    sample2.isChecked = NO;
    
    HazardTypeModel *sample3 = [HazardTypeModel new];
    sample3.hazardName = @"Isolation";
    sample3.isChecked = NO;
    
    HazardTypeModel *sample4 = [HazardTypeModel new];
    sample4.hazardName = @"Control (administrative)";
    sample4.isChecked = NO;
    
    HazardTypeModel *sample5 = [HazardTypeModel new];
    sample5.hazardName = @"PPE";
    sample5.isChecked = NO;
    
    HazardTypeModel *sample6 = [HazardTypeModel new];
    sample6.hazardName = @"Discipline";
    sample6.isChecked = NO;
    
    [areaOwnerHazardTypeArray addObjectsFromArray:@[sample1, sample2, sample3, sample4, sample5, sample6]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Some calculation to determine height of row/cell.
    return CELL_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return areaOwnerHazardTypeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AreaOwnerHazardTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AreaOwnerHazardTypeCell"];
    
    [cell updateDisplay:areaOwnerHazardTypeArray[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView1 willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setGreyWhiteCellColor:cell row:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (HazardTypeModel *model in areaOwnerHazardTypeArray)
    {
        model.isChecked = NO;
    }
    
    HazardTypeModel *model = (HazardTypeModel *)areaOwnerHazardTypeArray[indexPath.row];
    model.isChecked = !model.isChecked;
    
    [ReportDetailsModel saveValue: model.hazardName key:NSUSERDEFAULT_KEY_HAZARDTYPE];
    [tableView reloadData];
}

@end
