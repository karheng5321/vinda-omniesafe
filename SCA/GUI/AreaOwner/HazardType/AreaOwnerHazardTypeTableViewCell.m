//
//  AreaOwnerHazardTypeTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerHazardTypeTableViewCell.h"
#import "GeneralHelper.h"

@interface AreaOwnerHazardTypeTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *booleanImage;
@property (weak, nonatomic) IBOutlet UILabel *hazardLabel;


@end

@implementation AreaOwnerHazardTypeTableViewCell

- (void) updateDisplay:(HazardTypeModel *)model
{
    [self.hazardLabel setText: model.hazardName];
    
    if (model.isChecked)
    {
        [self.booleanImage setImage: [UIImage imageNamed:@"radio_black"]];
    }
    else
    {
        [self.booleanImage setImage: [UIImage imageNamed:@"radio_empty"]];
    }
}
@end
