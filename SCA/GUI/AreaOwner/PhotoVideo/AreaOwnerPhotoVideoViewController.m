//
//  AreaOwnerPhotoVideoViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 05/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AreaOwnerPhotoVideoViewController.h"
#import "ReportDetailsModel.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface AreaOwnerPhotoVideoViewController ()<UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictureButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictureButtonWidthConstraint;

@end

@implementation AreaOwnerPhotoVideoViewController

//NO is select from gallery
bool areaOwnerIsTakingPhoto;
bool areaOwnerIsTakingVideo;
NSMutableArray *areaOwnerPicVidArray;
NSData *areaOwnerCameraImage;
NSData *areaOwnerCameraVideo;

- (void) setupLayout
{
    CGFloat buttonSizeWidth;
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.buttonTopConstraint.constant = 10;
        buttonSizeWidth = 100;
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        buttonSizeWidth = 130;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.buttonTopConstraint.constant = 40;
        buttonSizeWidth = 150;
    }
    else if ([GeneralHelper isDeviceiPhone6plus])
    {
        self.buttonTopConstraint.constant = 55;
        buttonSizeWidth = 170;
    }
    
    self.pictureButtonWidthConstraint.constant = buttonSizeWidth;
    self.pictureButtonHeightConstraint.constant = buttonSizeWidth;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
    areaOwnerPicVidArray = [NSMutableArray new];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIButtons Actions

- (IBAction)pictureTapped:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Take photo", @"Choose from existing", nil];
    actionSheet.tag = ACTIONSHEET_TAG_PHOTO;
    [actionSheet showInView:self.view];
}

- (IBAction)videoTapped:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Take video", @"Choose from existing", nil];
    actionSheet.tag = ACTIONSHEET_TAG_VIDEO;
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == ACTIONSHEET_TAG_PHOTO)
    {
        switch (buttonIndex) {
            case 0:
                areaOwnerIsTakingPhoto = YES;
                [self startTakingPicture];
                break;
            case 1:
                areaOwnerIsTakingPhoto = NO;
                [self startTakingPicture];
                break;
            default:
                break;
        }
    }
    
    else if (actionSheet.tag == ACTIONSHEET_TAG_VIDEO)
    {
        switch (buttonIndex) {
            case 0:
                areaOwnerIsTakingVideo = YES;
                [self startTakingVideo];
                break;
            case 1:
                areaOwnerIsTakingVideo = NO;
                [self startTakingVideo];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Picture and Video Action

- (void) startTakingPicture {
    
    UIImagePickerController *picturePicker = [[UIImagePickerController alloc] init];
    
    if (areaOwnerIsTakingPhoto == YES)
    {
        picturePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if (areaOwnerIsTakingPhoto == NO)
    {
        picturePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    picturePicker.editing = YES;
    picturePicker.delegate = (id)self;
    [self presentViewController:picturePicker animated:YES completion:nil];
}


- (BOOL)startTakingVideo
{
    
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    
    if (areaOwnerIsTakingVideo == YES)
    {
        videoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if (areaOwnerIsTakingVideo == NO)
    {
        videoPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    videoPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
    videoPicker.allowsEditing = NO;
    videoPicker.delegate = (id)self;
    videoPicker.videoMaximumDuration = VIDEO_DURATION;
    [self presentViewController:videoPicker animated:YES completion:nil];
    return YES;
}

#pragma mark - Image picker delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"%@", info);
    [self dismissViewControllerAnimated:TRUE completion:nil];
    
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    // Handle a movie capture
    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo)
    {
        
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        [ReportDetailsModel saveValue: [videoURL absoluteString] key:NSUSERDEFAULT_KEY_VIDEOURL];
        areaOwnerCameraVideo = [NSData dataWithContentsOfURL:videoURL];
        [areaOwnerPicVidArray addObject:areaOwnerCameraVideo];
        
        //If video is taken, save to gallery] path];
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([videoURL path]) && (areaOwnerIsTakingVideo == YES))
        {
            UISaveVideoAtPathToSavedPhotosAlbum([videoURL path], self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        }
    }
    // Handle a picture capture
    else {
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        areaOwnerCameraImage = UIImageJPEGRepresentation(image, 0.5);
        [areaOwnerPicVidArray addObject: areaOwnerCameraImage];
        
        //If picture is taken, save to gallery
        if (areaOwnerIsTakingPhoto == YES) {
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
        }
        
    }
    
    [ReportDetailsModel saveArray:areaOwnerPicVidArray key: NSUSERDEFAULT_KEY_AFTERPICVIDFILE];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Media Taken" message:@"Press Next to Submit"
                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}


-(void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    if (error)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Media Failed to Save into Gallery"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:true completion: nil];
}


@end
