//
//  PictureCollectionViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UpdateMediaNotification @"UpdateMediaNotification"
#define SelectMediaNotification @"SelectMediaNotification"

@interface PictureCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *picvidImage;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *uploadButton;

- (void) updateDisplay:(UIImage *)image;

@end
