//
//  SubmitViewController.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "SubmitViewController.h"
#import "SubmitTableViewCell.h"
#import "PictureTableViewCell.h"
#import "PictureCollectionViewCell.h"
#import "GeneralHelper.h"
#import "Constant.h"
#import "AsyncApi.h"
#import "AppUtil.h"
#import "AppDelegate.h"
#import "MainMenuViewController.h"
#import "AdminMenuSelectionViewController.h"

@interface SubmitViewController () <AsyncApiDelegate, UIActionSheetDelegate> {
    UIView *_loadingView;
}

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *submitTableView;

@property (nonatomic, strong) NSMutableArray *reviewArray;
@property (strong, nonatomic) NSMutableDictionary *offscreenCells;

@end

@implementation SubmitViewController

static NSString * const SUBMIT_REPORT_CALL_ID = @"submitreport";

ReportDetailsModel *submittingModel;

- (void)setupLayout
{
    if ([GeneralHelper isDeviceiPhone4])
    {
        self.topBarHeightConstraint.constant = 110;
    }
    else if ([GeneralHelper isDeviceiPhone5])
    {
        self.topBarHeightConstraint.constant = 120;
    }
    else if ([GeneralHelper isDeviceiPhone6])
    {
        self.topBarHeightConstraint.constant = 120;
    }
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    _loadingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    _loadingView.center = self.view.center;//CGPointMake(_screenWidth/2.0, _screenHeight/2.0);
    _loadingView.layer.cornerRadius = 10.0;
    _loadingView.hidden = YES;
    [self.view addSubview:_loadingView];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.center = CGPointMake(_loadingView.frame.size.width/2.0, _loadingView.frame.size.height/2.0);
    [_loadingView addSubview:activityIndicator];
    [activityIndicator startAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupLayout];
    [self roundButton: self.submitButton];
    [self roundButton: self.cancelButton];
    
    
    //submittingModel = [ReportDetailsModel getReportDetailInfo];
    //submittingModel.areaOwner = @"MBPJ";
    
//    NSLog(@"submitterName  -> %@",submittingModel.submitterName);
//    NSLog(@"dateTime       -> %@",submittingModel.dateTime);
//    NSLog(@"remark         -> %@",submittingModel.remark);
//    NSLog(@"areaOwner      -> %@",submittingModel.areaOwner);
//    NSLog(@"criticallyType -> %@",submittingModel.criticallyType);
//    NSLog(@"hazardType     -> %@",submittingModel.hazardType);
//    NSLog(@"image          -> %@",submittingModel.uploadPicVidArray);

    [self createData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reportMediaUpdated:) name:UpdateMediaNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectMedia:) name:SelectMediaNotification object:nil];
}

- (void)reportMediaUpdated:(NSNotification *)note
{
    [self createData];
}

- (void)selectMedia:(NSNotification *)note
{
    UIButton *sender = (UIButton *)[note object];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: NSLocalizedString(@"cancel", nil)
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (sender.tag == 1)
    {
        UIImage *btnImage = [sender backgroundImageForState:UIControlStateNormal];
        if (btnImage != nil)
        {
            [actionSheet addButtonWithTitle:NSLocalizedString(@"take_photo", nil)];
            [actionSheet addButtonWithTitle:NSLocalizedString(@"choose_photo", nil)];
            [actionSheet addButtonWithTitle:NSLocalizedString(@"take_video", nil)];
            [actionSheet addButtonWithTitle:NSLocalizedString(@"choose_video", nil)];
        }
        else if ([defaults objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB])
        {
            [actionSheet addButtonWithTitle:NSLocalizedString(@"take_photo", nil)];
            [actionSheet addButtonWithTitle:NSLocalizedString(@"choose_photo", nil)];
        }
        else
        {
            [actionSheet addButtonWithTitle:NSLocalizedString(@"take_video", nil)];
            [actionSheet addButtonWithTitle:NSLocalizedString(@"choose_video", nil)];
        }
    }
    else
    {
        UIImage *btnImage = [sender backgroundImageForState:UIControlStateNormal];
        if (btnImage == nil)
        {
            [actionSheet addButtonWithTitle:NSLocalizedString(@"take_video", nil)];
            [actionSheet addButtonWithTitle:NSLocalizedString(@"choose_video", nil)];
        }
        else if ([defaults objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB])
        {
            [actionSheet addButtonWithTitle:NSLocalizedString(@"take_video", nil)];
            [actionSheet addButtonWithTitle:NSLocalizedString(@"choose_video", nil)];
        }
        else
        {
            [actionSheet addButtonWithTitle:NSLocalizedString(@"take_photo", nil)];
            [actionSheet addButtonWithTitle:NSLocalizedString(@"choose_photo", nil)];
        }
    }
    
    actionSheet.tag = ACTIONSHEET_TAG_VIDEO;
    [actionSheet showInView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLoadingHidden:(BOOL)animated
{
    _loadingView.hidden = animated;
    self.view.userInteractionEnabled = animated;
}

#pragma mark - UIButtons Actions

- (IBAction)submitTapped:(id)sender
{
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDel isInternetAvailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"no_internet", nil)
                                                        message:NSLocalizedString(@"retry", nil)
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Report Submitted"
                                                    message:@"Thank you for reporting this hazard"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];*/
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [self setLoadingHidden:NO];
    
    BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
    if (isUpdate)
    {
        NSString *hazardId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_HAZARDID] integerValue]];
        NSString *reportId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_REPORTNO] integerValue]];
        NSLog(@"reportId: %@", reportId);
        AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
        [asyncApi report:reportId updateHazard:hazardId picture:[defaults objectForKey:NSUSERDEFAULT_KEY_PIC_DATA] video:[defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_DATA] thumbnail:[defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB] remark:[defaults objectForKey:NSUSERDEFAULT_KEY_REMARK] :SUBMIT_REPORT_CALL_ID];
    }
    else
    {
        NSString *hazardId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_HAZARDID] integerValue]];
        NSString *areaId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_AREA_ID] integerValue]];
        NSString *criticalityId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_CRITICALLY_ID] integerValue]];
        //NSLog(@"[defaults objectForKey:NSUSERDEFAULT_KEY_PIC_DATA]: %@", [defaults objectForKey:NSUSERDEFAULT_KEY_PIC_DATA]);
        AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
        [asyncApi submitReportHazard:hazardId area:areaId criticality:criticalityId picture:[defaults objectForKey:NSUSERDEFAULT_KEY_PIC_DATA] video:[defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_DATA] thumbnail:[defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB] remark:[defaults objectForKey:NSUSERDEFAULT_KEY_REMARK] :SUBMIT_REPORT_CALL_ID];
    }
}

- (IBAction)cancelTapped:(id)sender
{
    /*NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
    BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
    BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
    
    [ReportDetailsModel clearAllReportDetailKey];
    if (isAdmin || isOwner)
        [self switchToDashboard:@"AdminMainMenuView"];
    else
        [self switchToDashboard:@"UserMainMenuView"];*/
    
    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:0];
    if ([vc isKindOfClass:[MainMenuViewController class]] || [vc isKindOfClass:[AdminMenuSelectionViewController class]])
        [self.navigationController popToRootViewControllerAnimated:YES];
    else
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

- (IBAction)saveTapped:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
    
    NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_REPORT] mutableCopy];
    if (!reportDict)
        reportDict = [NSMutableDictionary new];
    NSMutableArray *reportArray = [[reportDict objectForKey:userId] mutableCopy];
    if (!reportArray)
        reportArray = [NSMutableArray new];
    
    BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
    NSMutableDictionary *report = [NSMutableDictionary new];
    if (isUpdate)
    {
        NSString *hazardId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_HAZARDID] integerValue]];
        NSString *reportId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_REPORTNO] integerValue]];
        NSData *picData = [defaults objectForKey:NSUSERDEFAULT_KEY_PIC_DATA];
        NSData *picThumb = [defaults objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB];
        NSData *vidData = [defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_DATA];
        NSData *thumbData = [defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB];
        NSString *remark = [defaults objectForKey:NSUSERDEFAULT_KEY_REMARK];
       
        [report setObject:hazardId forKey:NSUSERDEFAULT_KEY_HAZARDID];
        [report setObject:reportId forKey:NSUSERDEFAULT_KEY_REPORTNO];
        [report setObject:remark forKey:NSUSERDEFAULT_KEY_REMARK];
        [report setObject:[NSDate date] forKey:NSUSERDEFAULT_KEY_DATE_TIME];
        [report setObject:userId forKey:NSUSERDEFAULT_KEY_SUBMITTER_ID];
        if (picData)
            [report setObject:picData forKey:NSUSERDEFAULT_KEY_PIC_DATA];
        if (picThumb)
            [report setObject:picData forKey:NSUSERDEFAULT_KEY_PIC_THUMB];
        if (vidData)
            [report setObject:vidData forKey:NSUSERDEFAULT_KEY_VIDEO_DATA];
        if (thumbData)
            [report setObject:thumbData forKey:NSUSERDEFAULT_KEY_VIDEO_THUMB];
        if ([defaults objectForKey:NSUSERDEFAULT_KEY_PICVIDFILE])
            [report setObject:[defaults objectForKey:NSUSERDEFAULT_KEY_PICVIDFILE] forKey:NSUSERDEFAULT_KEY_PICVIDFILE];
        
        NSString *hazardName = [NSString stringWithFormat:@"%@", [defaults objectForKey:NSUSERDEFAULT_KEY_HAZARDTYPE]];
        NSString *locationName = [NSString stringWithFormat:@"%@", [defaults objectForKey:NSUSERDEFAULT_KEY_AREA_LOCATION]];
        NSString *ownerId = [NSString stringWithFormat:@"%@", [defaults objectForKey:NSUSERDEFAULT_KEY_AREA_OWNER]];
        NSString *criticalityName = [NSString stringWithFormat:@"%@", [defaults objectForKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE]];
        
        [report setObject:hazardName forKey:NSUSERDEFAULT_KEY_HAZARDTYPE];
        [report setObject:locationName forKey:NSUSERDEFAULT_KEY_AREA_LOCATION];
        [report setObject:ownerId forKey:NSUSERDEFAULT_KEY_AREA_OWNER];
        [report setObject:criticalityName forKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
        
    }
    else
    {
        NSString *hazardId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_HAZARDID] integerValue]];
        NSString *areaId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_AREA_ID] integerValue]];
        NSString *criticalityId = [NSString stringWithFormat:@"%ld", [[defaults objectForKey:NSUSERDEFAULT_KEY_CRITICALLY_ID] integerValue]];
        NSData *picData = [defaults objectForKey:NSUSERDEFAULT_KEY_PIC_DATA];
        NSData *picThumb = [defaults objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB];
        NSData *vidData = [defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_DATA];
        NSData *thumbData = [defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB];
        NSString *remark = [defaults objectForKey:NSUSERDEFAULT_KEY_REMARK];
        
        [report setObject:hazardId forKey:NSUSERDEFAULT_KEY_HAZARDID];
        [report setObject:areaId forKey:NSUSERDEFAULT_KEY_AREA_ID];
        [report setObject:criticalityId forKey:NSUSERDEFAULT_KEY_CRITICALLY_ID];
        [report setObject:remark forKey:NSUSERDEFAULT_KEY_REMARK];
        [report setObject:[NSDate date] forKey:NSUSERDEFAULT_KEY_DATE_TIME];
        [report setObject:userId forKey:NSUSERDEFAULT_KEY_SUBMITTER_ID];
        if (picData)
            [report setObject:picData forKey:NSUSERDEFAULT_KEY_PIC_DATA];
        if (picThumb)
            [report setObject:picData forKey:NSUSERDEFAULT_KEY_PIC_THUMB];
        if (vidData)
            [report setObject:vidData forKey:NSUSERDEFAULT_KEY_VIDEO_DATA];
        if (thumbData)
            [report setObject:thumbData forKey:NSUSERDEFAULT_KEY_VIDEO_THUMB];
        if ([defaults objectForKey:NSUSERDEFAULT_KEY_PICVIDFILE])
            [report setObject:[defaults objectForKey:NSUSERDEFAULT_KEY_PICVIDFILE] forKey:NSUSERDEFAULT_KEY_PICVIDFILE];
        
        NSString *hazardName = [NSString stringWithFormat:@"%@", [defaults objectForKey:NSUSERDEFAULT_KEY_HAZARDTYPE]];
        NSString *locationName = [NSString stringWithFormat:@"%@", [defaults objectForKey:NSUSERDEFAULT_KEY_AREA_LOCATION]];
        NSString *ownerId = [NSString stringWithFormat:@"%@", [defaults objectForKey:NSUSERDEFAULT_KEY_AREA_OWNER]];
        NSString *criticalityName = [NSString stringWithFormat:@"%@", [defaults objectForKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE]];
        
        [report setObject:hazardName forKey:NSUSERDEFAULT_KEY_HAZARDTYPE];
        [report setObject:locationName forKey:NSUSERDEFAULT_KEY_AREA_LOCATION];
        [report setObject:ownerId forKey:NSUSERDEFAULT_KEY_AREA_OWNER];
        [report setObject:criticalityName forKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
    }
    
    [reportArray addObject:report];
    [reportDict setObject:reportArray forKey:userId];
    [defaults setObject:reportDict forKey:NSUserDefaultKey_SAVED_REPORT];
    
    //NSLog(@"[defaults objectForKey:NSUserDefaultKey_SAVED_REPORT]: %@", [defaults objectForKey:NSUserDefaultKey_SAVED_REPORT]);
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"report_saved", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:0];
        if ([vc isKindOfClass:[MainMenuViewController class]] || [vc isKindOfClass:[AdminMenuSelectionViewController class]])
            [self.navigationController popToRootViewControllerAnimated:YES];
        else
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }];
    
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // the user clicked OK
    /*if (buttonIndex == 0)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
        NSDictionary *roles = [user objectForKey:USER_ROLES_KEY];
        BOOL isAdmin = [[roles objectForKey:ROLES_ADMIN_KEY] boolValue];
        BOOL isOwner = [[roles objectForKey:ROLES_OWNER_KEY] boolValue];
        
        [ReportDetailsModel clearAllReportDetailKey];
        if (isAdmin || isOwner)
            [self switchToDashboard:@"AdminMainMenuView"];
        else
            [self switchToDashboard:@"UserMainMenuView"];
    }*/
    
    UIViewController *vc = [self.navigationController.viewControllers objectAtIndex:0];
    if ([vc isKindOfClass:[MainMenuViewController class]] || [vc isKindOfClass:[AdminMenuSelectionViewController class]])
        [self.navigationController popToRootViewControllerAnimated:YES];
    else
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}


#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == ACTIONSHEET_TAG_VIDEO)
    {
        NSString *btnTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
        if ([btnTitle isEqualToString:NSLocalizedString(@"cancel", nil)])
            return;
        
        UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
        videoPicker.delegate = (id)self;
        
        if ([btnTitle isEqualToString:NSLocalizedString(@"take_photo", nil)])
        {
            videoPicker.allowsEditing = YES;
            videoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        else if ([btnTitle isEqualToString:NSLocalizedString(@"choose_photo", nil)])
        {
            videoPicker.allowsEditing = YES;
            videoPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
        else if ([btnTitle isEqualToString:NSLocalizedString(@"take_video", nil)])
        {
            videoPicker.allowsEditing = NO;
            videoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            videoPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
            videoPicker.videoMaximumDuration = VIDEO_DURATION;
        }
        else if ([btnTitle isEqualToString:NSLocalizedString(@"choose_video", nil)])
        {
            videoPicker.allowsEditing = NO;
            videoPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            videoPicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];
            videoPicker.videoMaximumDuration = VIDEO_DURATION;
        }
        
        [self presentViewController:videoPicker animated:YES completion:nil];
    }
}

#pragma mark - Image picker delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"%@", info);
    [self dismissViewControllerAnimated:TRUE completion:nil];
    
    NSMutableArray *picVidArray = [[[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_PICVIDFILE] mutableCopy];
    if (!picVidArray)
        picVidArray = [[NSMutableArray alloc] init];
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    // Handle a movie capture
    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo)
    {
        
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSError *attributesError;
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[videoURL path] error:&attributesError];
        NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
        long long fileSize = [fileSizeNumber longLongValue];
        //NSLog(@"fileSize: %lld", fileSize);
        if (fileSize > FILE_UPLOAD_LIMIT)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"big_video_warning", nil) message:NSLocalizedString(@"big_video_message", nil) delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        [ReportDetailsModel saveValue: [videoURL absoluteString] key:NSUSERDEFAULT_KEY_VIDEOURL];
        NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
        
        BOOL newlyAdd = ([[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB] == nil);
        
        UIImage *image = [AppUtil thumbnailImageForVideo:videoURL atTime:0.0];
        image = [AppUtil resizeImageForThumbnail:image];
        NSData *thumbData = UIImageJPEGRepresentation(image, 0.5);
        [ReportDetailsModel saveValue: thumbData key:NSUSERDEFAULT_KEY_VIDEO_THUMB];
        [ReportDetailsModel saveValue: videoData key:NSUSERDEFAULT_KEY_VIDEO_DATA];
        
        if (newlyAdd)
            [picVidArray addObject:@"YES"];
    }
    // Handle a picture capture
    else {
        
        BOOL newlyAdd = ([[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB] == nil);
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        NSData *imageData = UIImageJPEGRepresentation([AppUtil resizeImageForUpload:image], 0.5);
        [ReportDetailsModel saveValue: imageData key:NSUSERDEFAULT_KEY_PIC_DATA];
        
        UIImage *thumb = [AppUtil resizeImageForThumbnail:image];
        NSData *thumbData = UIImageJPEGRepresentation(thumb, 0.5);
        [ReportDetailsModel saveValue: thumbData key:NSUSERDEFAULT_KEY_PIC_THUMB];
        
        if (newlyAdd)
            [picVidArray addObject: @"YES"];
    }
    
    [ReportDetailsModel saveArray:picVidArray key:NSUSERDEFAULT_KEY_PICVIDFILE];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:UpdateMediaNotification object:nil userInfo:nil];
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    reviewUIModel *model = self.reviewArray[indexPath.row];
    
    switch (model.cellType)
    {
        case SubmitCellTypeText:
        {
            NSString *reuseIdentifier = NSStringFromClass([SubmitTableViewCell class]);
            
            SubmitTableViewCell *cell = [self.offscreenCells objectForKey:reuseIdentifier];
            if(cell == nil)
            {
                cell = (SubmitTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
            }
            [cell updateDisplay:self.reviewArray[indexPath.row]];
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            
            cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
            
            [cell setNeedsLayout];
            [cell layoutIfNeeded];
            
            CGFloat height = 0.0;
            height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            
            height += 1;
            return height;
        }
            break;
            
        case SubmitCellTypeMedia:
        {
            return 71;
        }
            break;
            
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.reviewArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubmitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SubmitTableViewCell class])];
    reviewUIModel *model = self.reviewArray[indexPath.row];
    
    switch (model.cellType)
    {
        case SubmitCellTypeText:
        {
            SubmitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SubmitTableViewCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            [cell updateDisplay:self.reviewArray[indexPath.row]];
            [self.offscreenCells setObject:cell forKey:@"SubmitTableViewCell"];
            
            return cell;
        }
            break;
            
        case SubmitCellTypeMedia:
        {
            PictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PictureTableViewCell class])];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            [cell updateDisplay:self.reviewArray[indexPath.row]];
            
            return cell;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - Private
- (void) createData
{
    self.reviewArray = [NSMutableArray new];
    
    NSString *todayDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy, HH:mm aa"];
    todayDate = [formatter stringFromDate:[NSDate date]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
    NSString *hazardTitle = isUpdate? NSLocalizedString(@"hazard_control", nil):NSLocalizedString(@"hazard_type", nil);
    
    //reviewUIModel *name = [reviewUIModel reviewUIModelWithTitle:@"Submitter Name" content:[user objectForKey:USER_FULLNAME_KEY] cellType:SubmitCellTypeText];
    reviewUIModel *dateTime = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"date_time", nil) content:todayDate cellType:SubmitCellTypeText];
    reviewUIModel *hazard = [reviewUIModel reviewUIModelWithTitle:hazardTitle content:[defaults objectForKey:NSUSERDEFAULT_KEY_HAZARDTYPE] cellType:SubmitCellTypeText];
    reviewUIModel *area = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"area_location", nil) content:[defaults objectForKey:NSUSERDEFAULT_KEY_AREA_LOCATION] cellType:SubmitCellTypeText];
    reviewUIModel *owner = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"area_owner_id", nil) content:[defaults objectForKey:NSUSERDEFAULT_KEY_AREA_OWNER] cellType:SubmitCellTypeText];
    reviewUIModel *critically = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"criticallity_type", nil) content:[defaults objectForKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE] cellType:SubmitCellTypeText];
    reviewUIModel *remark = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"description", nil) content:[defaults objectForKey:NSUSERDEFAULT_KEY_REMARK] cellType:SubmitCellTypeText];
    
    NSMutableArray *imageArray = [NSMutableArray new];
    if ([defaults objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB])
        [imageArray addObject:[UIImage imageWithData:[defaults objectForKey:NSUSERDEFAULT_KEY_PIC_THUMB]]];
    if ([defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB])
        [imageArray addObject:[UIImage imageWithData:[defaults objectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB]]];
    
    reviewUIModel *media = [reviewUIModel reviewUIModelWithTitle:NSLocalizedString(@"picture_video_uploaded", nil) media:imageArray cellType:SubmitCellTypeMedia];
    
    [self.reviewArray addObjectsFromArray:@[dateTime, hazard, area, owner, critically, remark, media]];
    [self.submitTableView reloadData];
}
//
//#pragma mark - UICollectionView Delegate
//
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    return submittingModel.uploadPicVidArray.count;
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    PictureCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PicVidCell" forIndexPath:indexPath];
//    
//    NSData *cameraPicorVid = [submittingModel.uploadPicVidArray objectAtIndex:indexPath.row];
//    
//    if ([UIImage imageWithData: cameraPicorVid] != nil) {
//        [cell.picvidImage setImage: [UIImage imageWithData:cameraPicorVid]];
//    }
//    else {
//        [cell.picvidImage setImage: [GeneralHelper generateThumbImage: submittingModel.videoURL]];
//    }
//    return cell;
//}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    //NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:SUBMIT_REPORT_CALL_ID])
    {
        NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
        [self setLoadingHidden:YES];
        
        if (result.status)
        {
            //NSMutableArray *array = (NSMutableArray *)result.result;
            //[self processData:array];
            //[self.reporthistoryTable reloadData];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            BOOL isUpdate = [[defaults valueForKey:NSUSERDEFAULT_KEY_UPDATING_REPORT] boolValue];
            NSString *title = isUpdate? NSLocalizedString(@"report_updated", nil):NSLocalizedString(@"report_submitted", nil);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                            message:NSLocalizedString(@"thank_you", nil)
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            if (self.cachedIndex)
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
                NSString *userId = [NSString stringWithFormat:@"%@", [user objectForKey:USER_ID_KEY]];
                
                NSMutableDictionary *reportDict = [[defaults objectForKey:NSUserDefaultKey_SAVED_REPORT] mutableCopy];
                if (!reportDict)
                    reportDict = [NSMutableDictionary new];
                NSMutableArray *dataArray = [[reportDict objectForKey:userId] mutableCopy];
                if (!dataArray)
                    dataArray = [NSMutableArray new];
                
                if ([self.cachedIndex integerValue] < [dataArray count])
                    [dataArray removeObjectAtIndex:[self.cachedIndex integerValue]];
                
                [reportDict setObject:dataArray forKey:userId];
                [defaults setObject:reportDict forKey:NSUserDefaultKey_SAVED_REPORT];
            }
            
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}


@end
