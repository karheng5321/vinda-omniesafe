//
//  PictureTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 02/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "PictureTableViewCell.h"
#import "PictureCollectionViewCell.h"

@interface PictureTableViewCell()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *pictureLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *picvidCollectionView;
@property (nonatomic, strong) NSArray *medias;

@end

@implementation PictureTableViewCell

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.pictureLabel.backgroundColor = [UIColor clearColor];
    self.picvidCollectionView.backgroundColor = [UIColor clearColor];
}

- (void) updateDisplay:(reviewUIModel *)model
{
    [self.picvidCollectionView registerNib:[UINib nibWithNibName:@"PictureCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PictureCollectionViewCell"];
    self.picvidCollectionView.dataSource = self;
    self.picvidCollectionView.delegate = self;
    
    self.pictureLabel.text = [NSString stringWithFormat:@"%@", model.title];
    self.medias = model.media;
    [self.picvidCollectionView reloadData];
}

#pragma mark - UICollectionView Data Source & Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.medias count]? 2:1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PictureCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PictureCollectionViewCell" forIndexPath:indexPath];
    cell.cancelButton.tag = indexPath.row+1;
    cell.uploadButton.tag = indexPath.row+1;
    [cell updateDisplay:(indexPath.row < [self.medias count])? self.medias[indexPath.row]:nil];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
