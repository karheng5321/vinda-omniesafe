//
//  SubmitTableViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "SubmitTableViewCell.h"

@interface SubmitTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *leftsideLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightsideLabel;

@end

@implementation SubmitTableViewCell

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.leftsideLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.leftsideLabel.frame);
    self.rightsideLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.rightsideLabel.frame);
    
    self.leftsideLabel.backgroundColor = [UIColor clearColor];
    self.rightsideLabel.backgroundColor = [UIColor clearColor];
}

- (void) updateDisplay:(reviewUIModel *)model
{
    self.leftsideLabel.text = [NSString stringWithFormat:@"%@", model.title];
    
    self.rightsideLabel.text = [NSString stringWithFormat:@"%@", model.content];
    if ([model.content length] == 0)
    {
        self.rightsideLabel.text = @"-";
    }
    
    
    [self.leftsideLabel sizeToFit];
}

@end
