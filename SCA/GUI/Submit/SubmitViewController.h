//
//  SubmitViewController.h
//  SCA
//
//  Created by Ngo Yen Sern on 25/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "InfoViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>

@interface SubmitViewController : InfoViewController

@property (nonatomic, retain) NSString *cachedIndex;

@end
