//
//  PictureCollectionViewCell.m
//  SCA
//
//  Created by Ngo Yen Sern on 06/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "PictureCollectionViewCell.h"
#import "ReportDetailsModel.h"

@interface PictureCollectionViewCell()

@end

@implementation PictureCollectionViewCell

//- (void) updateDisplay:(NSString *)mediaName
- (void) updateDisplay:(UIImage *)image
{
    self.picvidImage.image = image;//[UIImage imageNamed:mediaName];
    self.cancelButton.hidden = !self.picvidImage.image;
    
    UIImage *btnImage = (self.cancelButton.hidden)? [UIImage imageNamed:@"ic_photo_placeholder.png"]:nil;
    [self.uploadButton setBackgroundImage:btnImage forState:UIControlStateNormal];
}

- (IBAction)cancelTapped:(id)sender
{
    [ReportDetailsModel clearUploadPicArrayAtIndex:self.cancelButton.tag-1];
    
    [self.picvidImage setImage: nil];
    self.cancelButton.hidden = !self.picvidImage.image;
    
    UIImage *btnImage = (self.cancelButton.hidden)? [UIImage imageNamed:@"ic_photo_placeholder.png"]:nil;
    [self.uploadButton setBackgroundImage:btnImage forState:UIControlStateNormal];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:UpdateMediaNotification object:nil userInfo:nil];
}

- (IBAction)cameraBtnTapped:(id)sender
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:SelectMediaNotification object:self.uploadButton userInfo:nil];
}

@end
