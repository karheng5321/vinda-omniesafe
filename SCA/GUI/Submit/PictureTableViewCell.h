//
//  PictureTableViewCell.h
//  SCA
//
//  Created by Ngo Yen Sern on 02/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "reviewUIModel.h"

@interface PictureTableViewCell : UITableViewCell

- (void) updateDisplay:(reviewUIModel *)model;

@end
