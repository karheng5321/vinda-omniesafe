//
//  AppDelegate.h
//  SCA
//
//  Created by kyTang on 21/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (BOOL)isInternetAvailable;
- (void)proceedLogout;
- (void)setSiteIndicator:(NSString *)text;
//- (void)hideSiteIndicator:(BOOL)hide;

@property (nonatomic, retain) NSString *API_SITE_URL;
@property (nonatomic) BOOL ON_DIFFERENT_SITE;

@end

