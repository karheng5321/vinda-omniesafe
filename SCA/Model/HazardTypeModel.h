//
//  HazardTypeModel.h
//  SCA
//
//  Created by kyTang on 28/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HazardTypeModel : NSObject

@property (nonatomic, strong) NSString *hazardName;
@property (nonatomic) BOOL isChecked;
@property (nonatomic) NSInteger hazardId;

@end
