//
//  ReportDetailsModel.h
//  SCA
//
//  Created by Ngo Yen Sern on 29/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#define NSUSERDEFAULT_KEY_REPORTNO @"NSUSERDEFAULT_KEY_REPORTNO"
#define NSUSERDEFAULT_KEY_SUBMITTER_NAME @"NSUSERDEFAULT_KEY_SUBMITTER_NAME"
#define NSUSERDEFAULT_KEY_DATE_TIME @"NSUSERDEFAULT_KEY_DATE_TIME"
#define NSUSERDEFAULT_KEY_HAZARDTYPE @"NSUSERDEFAULT_KEY_HAZARDTYPE"
#define NSUSERDEFAULT_KEY_HAZARDID @"NSUSERDEFAULT_KEY_HAZARDID"
#define NSUSERDEFAULT_KEY_AREA_LOCATION @"NSUSERDEFAULT_KEY_AREA_LOCATION"
#define NSUSERDEFAULT_KEY_AREA_ID @"NSUSERDEFAULT_KEY_AREA_ID"
#define NSUSERDEFAULT_KEY_AREA_CATEGORY_ID @"NSUSERDEFAULT_KEY_AREA_CATEGORY_ID"
#define NSUSERDEFAULT_KEY_AREA_OWNER @"NSUSERDEFAULT_KEY_AREA_OWNER"
#define NSUSERDEFAULT_KEY_CRITICALLY_TYPE @"NSUSERDEFAULT_KEY_CRITICALLY_TYPE"
#define NSUSERDEFAULT_KEY_CRITICALLY_ID @"NSUSERDEFAULT_KEY_CRITICALLY_ID"
#define NSUSERDEFAULT_KEY_REMARK @"NSUSERDEFAULT_KEY_REMARK"
#define NSUSERDEFAULT_KEY_USERNAME @"NSUSERDEFAULT_KEY_USERNAME"
#define NSUSERDEFAULT_KEY_INCIDENTS @"NSUSERDEFAULT_KEY_INCIDENTS"
#define NSUSERDEFAULT_KEY_STATUS @"NSUSERDEFAULT_KEY_STATUS"
#define NSUSERDEFAULT_KEY_PICVIDFILE @"NSUSERDEFAULT_KEY_PICVIDFILE"
#define NSUSERDEFAULT_KEY_AFTERPICVIDFILE @"NSUSERDEFAULT_KEY_AFTERPICVIDFILE"
#define NSUSERDEFAULT_KEY_VIDEOURL @"NSUSERDEFAULT_KEY_VIDEOURL"
#define NSUSERDEFAULT_KEY_VIDEO_THUMB @"NSUSERDEFAULT_KEY_VIDEO_THUMB"
#define NSUSERDEFAULT_KEY_VIDEO_DATA @"NSUSERDEFAULT_KEY_VIDEO_DATA"
#define NSUSERDEFAULT_KEY_PIC_DATA @"NSUSERDEFAULT_KEY_PIC_DATA"
#define NSUSERDEFAULT_KEY_PIC_THUMB @"NSUSERDEFAULT_KEY_PIC_THUMB"

#define NSUSERDEFAULT_KEY_UPDATING_REPORT @"NSUSERDEFAULT_KEY_UPDATING_REPORT"
#define NSUSERDEFAULT_KEY_SUBMITTER_ID @"NSUSERDEFAULT_KEY_SUBMITTER_ID"

@interface ReportDetailsModel : NSObject

@property (nonatomic, strong) NSString *submitterName;
@property (nonatomic, strong) NSString *dateTime;
@property (nonatomic, strong) NSString *hazardType;
@property (nonatomic, strong) NSString *areaLocation;
@property (nonatomic, strong) NSString *areaOwner;
@property (nonatomic, strong) NSString *criticallyType;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, strong) NSString *reportNo;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *incidents;
@property (nonatomic, strong) NSMutableArray *uploadPicVidArray;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSMutableArray *afterActionPicVidArray;
@property (nonatomic, strong) NSURL *videoURL;


+ (ReportDetailsModel *) getReportDetailInfo;
+ (void) saveValue:(id)value key:(NSString *)key;
+ (void) saveArray:(NSMutableArray *)mutablearray key:(NSString *)key;
+ (void) clearAllReportDetailKey;
+ (void) clearUploadPicArray;
+ (void)clearUploadPicArrayAtIndex:(NSInteger)index;
+ (void) clearAfterActionPicArray;
+ (NSString*) getValue: (NSString *)key;
+ (NSMutableArray*) getMutableArray: (NSString *)key;

@end
