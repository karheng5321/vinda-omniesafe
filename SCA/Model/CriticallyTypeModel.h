//
//  CriticallyTypeModel.h
//  SCA
//
//  Created by Ngo Yen Sern on 28/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CriticallyTypeModel : NSObject

@property (nonatomic, strong) NSString *criticallyName;
@property (nonatomic) BOOL isChecked;
@property (nonatomic) NSInteger criticalityId;

@end
