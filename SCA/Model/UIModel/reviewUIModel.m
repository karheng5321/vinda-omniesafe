//
//  reviewUIModel.m
//  SCA
//
//  Created by kyTang on 16/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "reviewUIModel.h"

@implementation reviewUIModel

+ (id) reviewUIModelWithTitle:(NSString *)title content:(NSString *)content cellType:(SubmitCellType)cellType
{
    reviewUIModel *model = [reviewUIModel new];
    model.title = title;
    model.content = content;
    model.cellType = cellType;
    
    return model;
}

+ (id) reviewUIModelWithTitle:(NSString *)title media:(NSArray *)media cellType:(SubmitCellType)cellType
{
    reviewUIModel *model = [reviewUIModel new];
    model.title = title;
    model.media = media;
    model.cellType = cellType;
    
    return model;
}

@end
