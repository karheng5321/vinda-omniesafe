//
//  reviewUIModel.h
//  SCA
//
//  Created by kyTang on 16/05/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerEnum.h"

@interface reviewUIModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;
@property (nonatomic) SubmitCellType cellType;
@property (nonatomic, strong) NSArray *fileUrl;

+ (id) reviewUIModelWithTitle:(NSString *)title content:(NSString *)content cellType:(SubmitCellType)cellType;

@property (nonatomic, strong) NSArray *media;


+ (id) reviewUIModelWithTitle:(NSString *)title media:(NSArray *)media cellType:(SubmitCellType)cellType;

@end
