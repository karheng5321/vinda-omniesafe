//
//  ReportDetailsModel.m
//  SCA
//
//  Created by Ngo Yen Sern on 29/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "ReportDetailsModel.h"
#import "Constant.h"

@implementation ReportDetailsModel

+ (ReportDetailsModel *) getReportDetailInfo
{
    ReportDetailsModel *model = [ReportDetailsModel new];
    model.reportNo = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_REPORTNO];
    model.submitterName = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_SUBMITTER_NAME];
    model.dateTime = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_DATE_TIME];
    model.hazardType = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_HAZARDTYPE];
    model.areaLocation = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_AREA_LOCATION];
    model.areaOwner = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_AREA_OWNER];
    model.criticallyType = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
    model.remark = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_REMARK];
    model.userName = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_USERNAME];
    model.incidents = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_INCIDENTS];
    model.status = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_STATUS];
    model.uploadPicVidArray = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_PICVIDFILE];
    model.afterActionPicVidArray = [[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_AFTERPICVIDFILE];
    model.videoURL = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:NSUSERDEFAULT_KEY_VIDEOURL]];
    
    return model;
}

+ (void) saveValue:(id)value key:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) saveArray:(NSMutableArray *)mutablearray key:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:mutablearray forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*) getValue: (NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:key];
}

+ (NSMutableArray*) getMutableArray: (NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+ (void) clearAllReportDetailKey
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_REPORTNO];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_SUBMITTER_NAME];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_DATE_TIME];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_HAZARDTYPE];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_AREA_LOCATION];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_AREA_OWNER];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_CRITICALLY_TYPE];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_REMARK];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_USERNAME];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_INCIDENTS];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_STATUS];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_PICVIDFILE];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_AFTERPICVIDFILE];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEOURL];
    
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_HAZARDID];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_AREA_ID];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_AREA_CATEGORY_ID];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_CRITICALLY_ID];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEO_DATA];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_PIC_THUMB];
    [userDefaults removeObjectForKey:NSUSERDEFAULT_KEY_PIC_DATA];
    
    [userDefaults synchronize];
}

+ (void)clearUploadPicArrayAtIndex:(NSInteger)index {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *mediaArray = [[defaults objectForKey:NSUSERDEFAULT_KEY_PICVIDFILE] mutableCopy];
    NSLog(@"mediaArray: %@", mediaArray);
    if (index > 0)
    {
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB];
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEO_DATA];
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEOURL];
        [mediaArray removeLastObject];
    }
    else if ([mediaArray count] > 1)
    {
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_PIC_THUMB];
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_PIC_DATA];
        [mediaArray removeObjectAtIndex:0];
    }
    else
    {
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_PIC_THUMB];
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_PIC_DATA];
        
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB];
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEO_DATA];
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_VIDEOURL];
        [mediaArray removeObjectAtIndex:0];
    }
    
    if ([mediaArray count])
        [defaults setObject:mediaArray forKey:NSUSERDEFAULT_KEY_PICVIDFILE];
    else
        [defaults removeObjectForKey:NSUSERDEFAULT_KEY_PICVIDFILE];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) clearUploadPicArray {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_VIDEO_THUMB];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_VIDEO_DATA];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_VIDEOURL];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_PIC_THUMB];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_PIC_DATA];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_PICVIDFILE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*+ (void) clearUploadPicArray {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_PICVIDFILE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}*/

+ (void) clearAfterActionPicArray {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NSUSERDEFAULT_KEY_AFTERPICVIDFILE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

@end