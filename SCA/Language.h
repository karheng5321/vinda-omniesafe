//
//  Language.h
//  ChickenCode
//
//  Created by kyTang on 2/3/16.
//  Copyright © 2016 Apps Design Studio Sdn. Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Language : NSBundle

@end

@interface NSBundle (Language)
+(void)setLanguage:(NSString *)language;
@end
