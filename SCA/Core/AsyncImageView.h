//
//  AppUtil.h
//
//  Created by Siti Norain Ishak on 5/20/14.
//

#import <UIKit/UIKit.h>

@class AsyncImageView;

@protocol AsyncImageViewDelegate <NSObject>

@optional

- (void)imageDownloaded:(UIImage *)image forFromUrl:(NSString *)url;
- (void)imageView:(AsyncImageView *)imageView resizeToSize:(CGSize)size;

@end

@interface AsyncImageView : UIImageView {
    id <AsyncImageViewDelegate> delegate;
    
    NSString *_imageUrl;
    BOOL _resize;
}

@property (nonatomic, retain) id <AsyncImageViewDelegate> delegate;

//- (id)initWithFrame:(CGRect)frame imageUrl:(NSString *)imageUrl objectKey:(NSString *)key;
- (id)initWithFrame:(CGRect)frame imageUrl:(NSString *)imageUrl autoResize:(BOOL)resize downloadNew:(BOOL)redownload;
- (void)reloadFromUrl:(NSString *)imageUrl;

@end
