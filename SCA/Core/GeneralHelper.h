//
//  GeneralHelper.h
//  SCA
//
//  Created by kyTang on 21/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GeneralHelper : NSObject

+ (BOOL) isDeviceiPhone4;
+ (BOOL) isDeviceiPhone5;
+ (BOOL) isDeviceiPhone6;
+ (BOOL) isDeviceiPhone6plus;

+ (BOOL)hasConnection;

+ (NSString *) getCurrentDateTime;
+ (UIImage *)generateThumbImage : (NSURL *)videoURL;
@end
