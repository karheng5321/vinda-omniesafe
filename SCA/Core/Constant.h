//
//  Constant.h
//  Grabbit
//
//  Created by Siti on 12/23/14.
//  Copyright (c) 2014 Apps Design Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    CONNECTION_LOST = 404,
    API_INVALID_RESULT = 9999,
    NO_DATA_AVAILABLE = 1001,
    SUCCESS = 1000
} ErrorNo;
/*
typedef NS_ENUM (NSInteger, Shortcut) {
    CALL = 1,
    WHATSAPP,
    SHARE,
    EMAIL,
    FACEBOOK,
    MESSAGE,
    MAP,
    WECHAT
};
*/
#define APP_LANGUAGE @"APP_LANGUAGE"
#define APP_LANGUAGE_INDEX @"APP_LANGUAGE_INDEX"
#define NOTIFICATION_CHANGE_LANGUAGE @"NOTIFICATION_CHANGE_LANGUAGE"

//static NSString * const NewContactAddedNotification = @"NewContactAdded";

static NSString * const CONNECTION_LOST_ERROR_MSG = @"Internet connection disconnected or not stable. Please try again.";
static NSString * const API_INVALID_RESULT_ERROR_MSG = @"Unexpected error occured. Please contact developer.";
static NSString * const SUCCESS_MSG = @"SUCCESS";

static NSString * const API_LOCATION_PREFIX = @"http://scasafe.appsdesignstudio.com";
static NSString * const SITE1_API_LOCATION_PREFIX = @"http://scasafe.appsdesignstudio.com";
static NSString * const SITE2_API_LOCATION_PREFIX = @"http://site2.omniesafe.appsdesignstudio.com";


static NSString * const NSUserDefaultKey_USER = @"user";
static NSString * const NSUserDefaultKey_DEVICE_TOKEN = @"devicetoken";

static NSString * const NSUserDefaultKey_HAZARD_LIST = @"hazardtype";
static NSString * const NSUserDefaultKey_HAZARD_CONTROL = @"hazardcontrol";
static NSString * const NSUserDefaultKey_AREA_LIST = @"arealist";
static NSString * const NSUserDefaultKey_CRITICALITY_LEVEL = @"criticalitylevel";

static NSString * const NSUserDefaultKey_SAVED_REPORT = @"savedreport";
static NSString * const NSUserDefaultKey_SAVED_BOS = @"NSUserDefaultKey_SAVED_BOS";

static NSString * const USER_ID_KEY = @"uacc_id";
static NSString * const USER_MOBILE_KEY = @"hp_number";
static NSString * const USER_DEPARTMENT_KEY = @"ism_department";
static NSString * const USER_DESIGNATION_KEY = @"ism_designation";
static NSString * const USER_TOKEN_KEY = @"ism_mobile_token";
static NSString * const USER_FULLNAME_KEY = @"ism_user_fullname";
static NSString * const USER_ROLES_KEY = @"roles";
static NSString * const ROLES_ADMIN_KEY = @"admin";
static NSString * const ROLES_BOS_SUPERVISOR_KEY = @"bossupervisor";
static NSString * const ROLES_BOS_USER_KEY = @"bosuser";
static NSString * const ROLES_MONITOR_KEY = @"adminmonitor";
static NSString * const ROLES_OWNER_KEY = @"area_owner";
static NSString * const ROLES_USER_KEY = @"user";
static NSString * const ROLES_SITE1_KEY = @"site1";
static NSString * const ROLES_SITE2_KEY = @"site2";
static NSString * const USER_USERNAME_KEY = @"uacc_username";

static NSString * const API_USERNAME_KEY = @"username";
static NSString * const API_PASSWORD_KEY = @"password";
static NSString * const API_LANGUAGE_KEY = @"language";
static NSString * const API_DEVICE_TOKEN_KEY = @"device_token";
//static NSString * const API_UDID_KEY = @"ios_udid";
static NSString * const API_ACTIVE_DEVICE_KEY = @"active_device";

static NSString * const API_REPORT_ID_KEY = @"report_id";
static NSString * const API_EMP_NO_KEY = @"emp_no";
static NSString * const API_PICTURE_KEY = @"pic";
static NSString * const API_VIDEO_KEY = @"vid";
static NSString * const API_VIDEO_THUMBNAIL_KEY = @"vid_thumbnail";
static NSString * const API_TOKEN_KEY = @"token";
static NSString * const API_AREA_ID_KEY = @"area_id";
static NSString * const API_HAZARD_ID_KEY = @"hazard_id";
static NSString * const API_HAZARD_CONTROL_ID_KEY = @"hazard_control_id";
static NSString * const API_CRITICALITY_ID_KEY = @"criticality_id";
static NSString * const API_REMARK_KEY = @"remark";

// Set Language
#define APP_LANGUAGE @"APP_LANGUAGE"
#define APP_LANGUAGE_INDEX @"APP_LANGUAGE_INDEX"
#define NOTIFICATION_CHANGE_LANGUAGE @"NOTIFICATION_CHANGE_LANGUAGE"

static CGFloat const STATUS_BAR_HEIGHT = 20.0;

@interface UIColor (Custom)

+ (UIColor *)appBOSBlueColor;
+ (UIColor *)appGreenColor;
+ (UIColor *)appDarkBlueColor;
+ (UIColor *)appUnsafeBlueColor;
+ (UIColor *)appSiteGreyColor;
+ (UIColor *)appSiteTurqoiseColor;

@end

@implementation UIColor (Custom)

+ (UIColor *)appBOSBlueColor
{
    return [UIColor colorWithRed:0.0/255.0 green:149.0/255.0 blue:218.0/255.0 alpha:1.0];
}

+ (UIColor *)appGreenColor
{
    return [UIColor colorWithRed:77.0/255.0 green:176.0/255.0 blue:37.0/255.0 alpha:1.0];
}

+ (UIColor *)appDarkBlueColor
{
    return [UIColor colorWithRed:0.0/255.0 green:53.0/255.0 blue:103.0/255.0 alpha:1.0];
}

+ (UIColor *)appUnsafeBlueColor
{
    return [UIColor colorWithRed:11.0/255.0 green:47.0/255.0 blue:143.0/255.0 alpha:1.0];
}

+ (UIColor *)appSiteGreyColor
{
    return [UIColor colorWithRed:150.0/255.0 green:151.0/255.0 blue:154.0/255.0 alpha:1.0];
}

+ (UIColor *)appSiteTurqoiseColor
{
    return [UIColor colorWithRed:1.0/255.0 green:104.0/255.0 blue:152.0/255.0 alpha:1.0];
}

@end

@interface UIFont (Custom)

+ (UIFont *)appFontWithSize:(CGFloat)size;
+ (UIFont *)appLightFontWithSize:(CGFloat)size;

@end

@implementation UIFont (Custom)

+ (UIFont *)appLightFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Avenir-Light" size:size];
}

+ (UIFont *)appFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"Roboto-Medium" size:size];
}

@end
