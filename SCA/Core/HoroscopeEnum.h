//
//  HoroscopeEnum.h
//
//  Created by Siti Norain on 1/24/13.
//

#import <Foundation/Foundation.h>
/*
typedef enum {
	CAPRICORN,
	AQUARIUS,
	PISCES,
	ARIES,
	TAURUS,
	GEMINI,
	CANCER,
	LEO,
	VIRGO,
	LIBRA,
	SCORPIO,
	SAGITTARIUS
} Horoscope;
*/
@interface HoroscopeEnum : NSObject
/*
//get list of all horoscope
+ (NSArray *)getAllHoroscope;

//get horoscope description
+ (NSString *)getHoroscopeDescription:(Horoscope)horoscopeEnum;

//get horoscope start date string (format: MMdd)
//+ (NSString *)getHoroscopeDate:(Horoscope)horoscopeEnum;

//get horoscope enum from birthday
+ (Horoscope)getHoroscopeEnum:(NSDate *)birthday;
*/
@end
