//
//  AsyncApi.m
//  SmartHeart
//
//  Created by Siti on 3/27/14.
//  Copyright (c) 2014 Apps Design Studio. All rights reserved.
//

#import "AsyncApi.h"
#import "AppDelegate.h"
#import "ReportDetailsModel.h"

@implementation AsyncApi

@synthesize delegate;

static NSString * const API_STATUS_KEY = @"status";
static NSString * const API_ERROR_MSG_KEY = @"error_msg";
static NSString * const API_ERROR_NO_KEY = @"error_no";
static NSString * const API_OBJECT_KEY = @"object";

- (id)initWithCaller:(id)caller
{
    self = [super init];
    if (self) {
        // Initialization code
		
		self.delegate = caller;
        _isMedia = NO;
    }
    return self;
}

- (void)downloadMedia:(NSString *)mediaUrlString :(NSString *)callId
{
    mediaUrlString = [mediaUrlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    _callId = callId;
    _isMedia = YES;
    _mediaUrlString = mediaUrlString;
    
    NSString *urlString = [NSString stringWithFormat:@"%@", mediaUrlString];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

/*- (void)createUser:(NSString *)email password:(NSString *)password mobile:(NSString *)mobileNo firstname:(NSString *)firstname lastname:(NSString *)lastname country:(NSString *)countryCode firstContactId:(NSString *)firstContactId :(NSString *)callId
{
    _callId = callId;
    
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
    [postDict setObject:email forKey:USER_EMAIL_KEY];
    [postDict setObject:password forKey:USER_PASSWORD_KEY];
    [postDict setObject:mobileNo forKey:USER_MOBILE_KEY];
    [postDict setObject:firstname forKey:USER_FIRSTNAME_KEY];
    [postDict setObject:lastname forKey:USER_LASTNAME_KEY];
    [postDict setObject:firstContactId forKey:FIRST_CONTACT_ID];
    [postDict setObject:countryCode forKey:API_COUNTRY_CODE_KEY];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceToken = [defaults objectForKey:NSUserDefaultKey_DEVICE_TOKEN];
    [postDict setObject:deviceToken forKey:API_DEVICE_TOKEN_KEY];
    
    NSString *udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [postDict setObject:udid forKey:API_UDID_KEY];
    
    [postDict setObject:@"iphone" forKey:API_ACTIVE_DEVICE_KEY];
    
    NSLog(@"postDict: %@", postDict);
    
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postDict
                                                       options:0
                                                         error:&error];
    
    //SBJsonWriter *writer = [SBJsonWriter alloc];
	//NSString *jsonConvertedObj = [writer stringWithObject:postDict];
	//NSData *postData = [jsonConvertedObj dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlString = [NSString stringWithFormat:@"%@createuser", API_LOCATION_PREFIX];
	urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSLog(@"apiURL: %@", urlString);
	NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
	[request setHTTPMethod:@"POST"];
	[request setHTTPBody:postData];
	
	[self startRequestToApi:request];
}*/

- (void)loginUser:(NSString *)username password:(NSString *)password language:(NSString *)language :(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceToken = [defaults objectForKey:NSUserDefaultKey_DEVICE_TOKEN];
    if (!deviceToken)
        deviceToken = @"";
    
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/auth", API_LOCATION_PREFIX];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_USERNAME_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[username dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_PASSWORD_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[password dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_DEVICE_TOKEN_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[deviceToken dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_ACTIVE_DEVICE_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"iphone" dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"language: %@", language);
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_LANGUAGE_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[language dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)updateDeviceToken:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    NSString *deviceToken = [defaults objectForKey:NSUserDefaultKey_DEVICE_TOKEN];
    if (!deviceToken)
        deviceToken = @"";
    
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/updatedevicetoken", API_LOCATION_PREFIX];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_TOKEN_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[token dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_DEVICE_TOKEN_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[deviceToken dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)logoutFromServer:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    NSString *deviceToken = @"";
    
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/updatedevicetoken", API_LOCATION_PREFIX];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_TOKEN_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[token dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_DEVICE_TOKEN_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[deviceToken dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)submitReportHazard:(NSString *)hazardId area:(NSString *)areaId criticality:(NSString *)criticalityId picture:(NSData *)picData video:(NSData *)vidData thumbnail:(NSData *)thumbnailData remark:(NSString *)remark :(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/ucreport", appDel.API_SITE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/ucreport", API_LOCATION_PREFIX];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_TOKEN_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[token dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_HAZARD_ID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[hazardId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_AREA_ID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[areaId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_CRITICALITY_ID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[criticalityId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_REMARK_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[remark dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *filename = @"picture.jpg";
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", API_PICTURE_KEY, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:picData]];
    
    filename = @"video.mp4";
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", API_VIDEO_KEY, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:vidData]];
    
    filename = @"thumbnail.jpg";
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", API_VIDEO_THUMBNAIL_KEY, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:thumbnailData]];
    
    
    //closing//
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)report:(NSString *)reportId updateHazard:(NSString *)controlId picture:(NSData *)picData video:(NSData *)vidData thumbnail:(NSData *)thumbnailData remark:(NSString *)remark :(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/ucreportupdate", appDel.API_SITE_URL];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/ucreportupdate", API_LOCATION_PREFIX];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_TOKEN_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[token dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_REPORT_ID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[reportId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_HAZARD_CONTROL_ID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[controlId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_REMARK_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[remark dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *filename = @"picture.jpg";
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", API_PICTURE_KEY, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:picData]];
    
    filename = @"video.mp4";
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", API_VIDEO_KEY, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:vidData]];
    
    filename = @"thumbnail.jpg";
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", API_VIDEO_THUMBNAIL_KEY, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:thumbnailData]];
    
    
    //closing//
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)getAdminReportHistory:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/adminreport/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/adminreport/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)getAreaReportHistory:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/areaownerreport/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/areaownerreport/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)getUserReportHistory:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/userreporthistory/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/userreporthistory/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)report:(NSString *)reportId getDetails:(BOOL)sameSite :(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/reportdetails/%@/%@", sameSite? API_LOCATION_PREFIX:appDel.API_SITE_URL, token, reportId];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/reportdetails/%@/%@", API_LOCATION_PREFIX, token, reportId];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)report:(NSString *)reportId close:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/closereport/%@/%@", appDel.API_SITE_URL, token, reportId];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/closereport/%@/%@", API_LOCATION_PREFIX, token, reportId];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)getHazardList:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/hazardlist/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/hazardlist/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)getHazardControlList:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/hazardcontrollist/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/hazardcontrollist/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)getAreaList:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/arealist/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/arealist/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)getCriticalityList:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/criticalitylist/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/sca_api/criticalitylist/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)bosUserReportHistory:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/self_report/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/self_report/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    [self startRequestToApi:request];
}

- (void)bosAllReportHistory:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/all_report_list/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/all_report_list/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    [self startRequestToApi:request];
}

- (void)bosGetBadgeNumber:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/badge_number/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/badge_number/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)bosReport:(NSString *)reportId getDetails:(BOOL)sameSite :(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/report_details/%@", sameSite? API_LOCATION_PREFIX:appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/report_details/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_REPORT_ID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[reportId dataUsingEncoding:NSUTF8StringEncoding]];
    
    //closing//
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)bosSearchEmpNo:(NSString *)empNo :(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/search_employee/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/search_employee/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_EMP_NO_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[empNo dataUsingEncoding:NSUTF8StringEncoding]];
    
    //closing//
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)bosGetLSRList:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/lsr_list/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/lsr_list/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    [self startRequestToApi:request];
}

- (void)bosActionList:(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/action_text/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/action_text/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    
    [self startRequestToApi:request];
}

- (void)user:(NSString *)userId reportBos:(NSString *)staffId lsr:(NSString *)lsrId behaviour:(NSString *)behaviour remark:(NSString *)remark :(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/bos_report/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/bos_report/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"uacc_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[userId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"staff_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[staffId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"lsr_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[lsrId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"behaviour"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[behaviour dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"feedback"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[remark dataUsingEncoding:NSUTF8StringEncoding]];
    
    //closing//
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)report:(NSString *)reportId takeAction:(NSString *)action :(NSString *)callId
{
    _callId = callId;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *user = [defaults objectForKey:NSUserDefaultKey_USER];
    NSString *token = [user valueForKey:USER_TOKEN_KEY];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/take_action/%@", appDel.API_SITE_URL, token];
    //NSString *urlString = [NSString stringWithFormat:@"%@/bos_api/take_action/%@", API_LOCATION_PREFIX, token];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"apiURL: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:120.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", API_REPORT_ID_KEY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[reportId dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", @"take_action"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[action dataUsingEncoding:NSUTF8StringEncoding]];
    
    //closing//
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self startRequestToApi:request];
}

- (void)startRequestToApi:(NSURLRequest *)request
{
	_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
	[_connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
	[_connection start];
}

- (void)cancel
{
    [_connection unscheduleFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [_connection cancel];
    _connection = nil;
    _data = nil;
}

- (NSString *)getMD5:(NSString *)originalString {
	const char *cStr = [originalString UTF8String];
	unsigned char result[16];
	CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
	return [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]];
}

- (NSString *)makeDocumentFullPath:(NSString*)filename {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docDir = [paths objectAtIndex:0];
	NSString *fullPath = [NSString stringWithFormat:@"%@/%@", docDir, filename];
	return fullPath;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
	
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (BOOL)isFileExist:(NSString *)string
{
	if ([[NSFileManager defaultManager] fileExistsAtPath:string])
		return YES;
	return NO;
}

#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
	if (_data == nil)
		_data = [[NSMutableData alloc] initWithCapacity:2048];
	[_data appendData:incrementalData];
}

- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	_connection = nil;
	
	//NSLog(@"connectionDidFinishLoading");
    
    if (_isMedia)
	{
        ResultApi *resultApi = [[ResultApi alloc] initWithStatus:YES errorNumber:SUCCESS errorMessage:SUCCESS_MSG];
        resultApi.result = _data;
        [delegate apiResponseComplete:_callId withResult:resultApi];
    }
	else if (self.delegate)
	{
		NSString *string = [[NSString alloc] initWithData:_data encoding:NSUTF8StringEncoding];
        NSLog(@"raw string: %@", string);
		//id result = [string JSONValue];
        
        NSError *error;
        id result = [NSJSONSerialization JSONObjectWithData:_data
                                                    options:NSJSONReadingMutableContainers
                                                      error:&error];
		
		//NSLog(@"result: %@", result);
		
		if (result)
		{
            NSInteger errorCode = [[result objectForKey:API_ERROR_NO_KEY] integerValue];
            if (errorCode == 2001)
            {
                NSLog(@"Authentication failed");
                
                AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate proceedLogout];
                
                ResultApi *resultApi = [[ResultApi alloc] initWithStatus:[[result objectForKey:API_STATUS_KEY] boolValue] errorNumber:[[result objectForKey:API_ERROR_NO_KEY] integerValue] errorMessage:[result objectForKey:API_ERROR_MSG_KEY]];
                resultApi.result = [result objectForKey:API_OBJECT_KEY];
                [delegate apiResponseComplete:_callId withResult:resultApi];
            }
            
            else
            {
                ResultApi *resultApi = [[ResultApi alloc] initWithStatus:[[result objectForKey:API_STATUS_KEY] boolValue] errorNumber:[[result objectForKey:API_ERROR_NO_KEY] integerValue] errorMessage:[result objectForKey:API_ERROR_MSG_KEY]];
                resultApi.result = [result objectForKey:API_OBJECT_KEY];
                [delegate apiResponseComplete:_callId withResult:resultApi];
            }
		}
		else
		{
			ResultApi *resultApi = [[ResultApi alloc] initWithStatus:NO errorNumber:API_INVALID_RESULT errorMessage:API_INVALID_RESULT_ERROR_MSG];
			[delegate apiResponseComplete:_callId withResult:resultApi];
		}
	}
	
	_data = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	_connection = nil;
    NSLog(@"didFailWithError: %@", error);
	
	if (self.delegate)
	{
		//ResultApi *resultApi = [[ResultApi alloc] initWithStatus:NO errorNumber:CONNECTION_LOST errorMessage:CONNECTION_LOST_ERROR_MSG];
		//[delegate apiResponseComplete:_callId withResult:resultApi];
	}
	
	_data = nil;
}

@end
