//
//  AppUtil.m
//  SmartHeart
//
//  Created by Siti on 5/20/14.
//  Copyright (c) 2014 Apps Design Studio. All rights reserved.
//

#import "AppUtil.h"
#import "AppDelegate.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import <CommonCrypto/CommonDigest.h>
//#import "JSON.h"

@implementation AppUtil

/*static double const PENINSULAR_MALAYSIA_MIN_LAT = 1.263325;
static double const PENINSULAR_MALAYSIA_MAX_LAT = 6.723257;
static double const PENINSULAR_MALAYSIA_MIN_LONG = 100.123215;
static double const PENINSULAR_MALAYSIA_MAX_LONG = 104.284973;

static double const LANGKAWI_MIN_LAT = 6.155827;
static double const LANGKAWI_MAX_LAT = 6.474846;
static double const LANGKAWI_MIN_LONG = 99.64215;
static double const LANGKAWI_MAX_LONG = 99.946883;

static double const LABUAN_MIN_LAT = 5.239341;
static double const LABUAN_MAX_LAT = 5.391805;
static double const LABUAN_MIN_LONG = 115.157318;
static double const LABUAN_MAX_LONG = 115.269241;

static double const EAST_MALAYSIA_MIN_LAT = 0.856902;
static double const EAST_MALAYSIA_MAX_LAT = 7.359743;
static double const EAST_MALAYSIA_MIN_LONG = 109.542618;
static double const EAST_MALAYSIA_MAX_LONG = 119.262085;*/

+ (BOOL)isFileExist:(NSString *)string
{
	if ([[NSFileManager defaultManager] fileExistsAtPath:string])
		return YES;
	return NO;
}

+ (NSString *)makeDocumentFullPath:(NSString*)filename {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docDir = [paths objectAtIndex:0];
	NSString *fullPath = [NSString stringWithFormat:@"%@/%@", docDir, filename];
	return fullPath;
}

+ (BOOL)deleteFileAtPath:(NSString*)filePath {
	return [[NSFileManager defaultManager] removeItemAtPath:filePath error:NULL];
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
	
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

+ (BOOL)writeData:(NSData *)data toFile:(NSString *)location
{
    //NSLog(@"location: %@", location);
    BOOL status = [data writeToFile:location atomically:FALSE];
    //NSLog(@"status: %d", status);
    if (status && [self isFileExist:location])
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:location]];
    
    return status;
}

+ (BOOL)writeContent:(id)content toFile:(NSString *)location
{
    //NSLog(@"location: %@", location);
    BOOL status = [content writeToFile:location atomically:FALSE];
    //NSLog(@"status: %d", status);
    if (status && [self isFileExist:location])
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:location]];
    
    return status;
}

+ (id)contentFromFile:(NSString *)location
{
    if (location)
    {
        //NSLog(@"file exist? %@", [AppUtil isFileExist:location]? @"YES":@"NO");
        id object = [NSMutableArray arrayWithContentsOfFile:location];
        if (!object)
            object = [NSDictionary dictionaryWithContentsOfFile:location];
        //NSLog(@"object: %@", object);
        return object;
    }
    
    return nil;
}

+ (UIImage *)resizeImageForThumbnail:(UIImage*)image
{
	CGSize newSize = [self sizeForSize:image.size];
	UIGraphicsBeginImageContext(newSize);
	[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
	UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return scaledImage;
}

+ (CGSize)sizeForSize:(CGSize)size
{
	float scaleFactor = 1.0;
	float maxWidth = 300.0;
	float maxHeight = 300.0;
	
	if (size.width >= size.height)
		scaleFactor = size.width/maxWidth;
	else
		scaleFactor = size.height/maxHeight;
	
	return CGSizeMake(size.width/scaleFactor, size.height/scaleFactor);
}

+ (UIImage *)resizeImageForUpload:(UIImage*)image
{
    CGSize newSize = [self sizeForUpload:image.size];
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

+ (CGSize)sizeForUpload:(CGSize)size
{
    float scaleFactor = 1.0;
    float maxWidth = 1000.0;
    float maxHeight = 1000.0;
    
    if (size.width >= size.height)
        scaleFactor = size.width/maxWidth;
    else
        scaleFactor = size.height/maxHeight;
    
    return CGSizeMake(size.width/scaleFactor, size.height/scaleFactor);
}

+ (id)removeNullElementFromObject:(id)object
{
    //NSString *jsonStr = [object JSONRepresentation];
    //jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"null" withString:@"\"\""];
    //return [jsonStr JSONValue];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:0
                                                         error:&error];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"null" withString:@"\"\""];
    jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    id result = [NSJSONSerialization JSONObjectWithData:jsonData
                                                options:NSJSONReadingMutableContainers
                                                  error:&error];
    
    return result;
}

/*+ (BOOL)saveImageData:(NSData *)imageData fromUrl:(NSString *)imageUrl
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *filename = [defaults objectForKey:imageUrl];
    if (!filename)
    {
        CFUUIDRef udid = CFUUIDCreate(NULL);
        filename = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
        [defaults setObject:filename forKey:imageUrl];
    }
    
    NSString *fileLoc = [AppUtil makeDocumentFullPath:filename];
    return [AppUtil writeData:imageData toFile:fileLoc];
}

+ (NSData *)getImageDataFromUrl:(NSString *)imageUrl
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *filename = [defaults objectForKey:imageUrl];
    if (!filename)
        return nil;
    
    NSString *fileLoc = [AppUtil makeDocumentFullPath:filename];
    return [NSData dataWithContentsOfFile:fileLoc];
}*/

+ (BOOL)saveData:(NSData *)data fromUrl:(NSString *)url
{
    //NSLog(@"saveData: %@", data);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *fileName = [defaults objectForKey:url];
    
    if (!fileName)
    {
        CFUUIDRef udid = CFUUIDCreate(NULL);
        fileName = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
    }
    
    NSString *filePath = [AppUtil makeDocumentFullPath:fileName];
    [defaults setObject:fileName forKey:url];
    
    BOOL status = [AppUtil writeData:data toFile:filePath];
    
    return status;
}

+ (NSData *)dataFromUrl:(NSString *)url
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *fileName = [defaults objectForKey:url];
    NSString *filePath = [AppUtil makeDocumentFullPath:fileName];
    
    if (filePath)
        return [NSData dataWithContentsOfFile:filePath];
    
    return nil;
}

+ (BOOL)writeArray:(id)array forKey:(NSString *)key
{
    //NSLog(@"writeArray: %@", array);
    NSString *location = [AppUtil makeDocumentFullPath:key];
    //NSLog(@"location: %@", location);
    BOOL status = [array writeToFile:location atomically:YES];
    //NSLog(@"status: %d", status);
    if (status && [self isFileExist:location])
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:location]];
    
    return status;
}

+ (id)arrayForKey:(NSString *)key
{
    //NSLog(@"arrayForKey");
    NSString *location = [AppUtil makeDocumentFullPath:key];
    //NSLog(@"location: %@", location);
    
    if (location)
    {
        //NSLog(@"file exist? %@", [AppUtil isFileExist:location]? @"YES":@"NO");
        id object = [NSMutableArray arrayWithContentsOfFile:location];
        if (!object)
            object = [NSDictionary dictionaryWithContentsOfFile:location];
        //NSLog(@"object: %@", object);
        return object;
    }
    
    return nil;
}

+ (NSURL *)fileLocationFromUrl:(NSString *)url
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *fileName = [defaults objectForKey:url];
    
    if (!fileName)
    {
        CFUUIDRef udid = CFUUIDCreate(NULL);
        fileName = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
    }
    
    NSString *filePath = [AppUtil makeDocumentFullPath:fileName];
    if ([AppUtil isFileExist:filePath])
        return [NSURL fileURLWithPath:filePath];
    
    return nil;
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)imageFromUIView:(UIView*)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

/*+ (UIImage *)backButtonImage
{
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 60.0, 40.0)];
    backView.backgroundColor = [UIColor clearColor];
    backView.opaque = NO;
    
    UIImage *backImage = [UIImage imageNamed:@"icon_back.png"];
    float scale = 2.0;
    float backWidth = backImage.size.width/scale;
    float backHeight = backImage.size.height/scale;
    
    UIImageView *arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, backView.frame.size.height/2.0-backHeight/2.0, backWidth, backHeight)];
    arrowView.image = backImage;
    [backView addSubview:arrowView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 100.0, 30.0)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.text = @"BACK";
    label.textAlignment = NSTextAlignmentRight;
    label.font = [UIFont appFontWithSize:14.0];
    [label sizeToFit];
    label.frame = CGRectMake(backView.frame.size.width-label.frame.size.width, backView.frame.size.height/2.0-label.frame.size.height/2.0, label.frame.size.width, label.frame.size.height);
    [backView addSubview:label];
    
    UIImage *backArrow = [self imageFromUIView:backView];
    
    return backArrow;
}*/

/*+ (Region)getUserRegion
{
    if (![CLLocationManager locationServicesEnabled])
        return LOCATION_SERVIS_DISABLE;
    
    NSLog(@"[CLLocationManager authorizationStatus]: %d", [CLLocationManager authorizationStatus]);
    
    if (([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied))
        return LOCATION_SERVIS_DISABLE;
    
    AppDelegate *appDel = (AppDelegate *)(AppDelegate *)[[UIApplication sharedApplication] delegate];
    double latitude = appDel.latitude;
    double longitude =  appDel.longitude;
    
    if (latitude >= LANGKAWI_MIN_LAT && latitude <= LANGKAWI_MAX_LAT && longitude >= LANGKAWI_MIN_LONG && longitude <= LANGKAWI_MAX_LONG)
        return LANGKAWI;
    else if (latitude >= PENINSULAR_MALAYSIA_MIN_LAT && latitude <= PENINSULAR_MALAYSIA_MAX_LAT && longitude >= PENINSULAR_MALAYSIA_MIN_LONG && longitude <= PENINSULAR_MALAYSIA_MAX_LONG)
        return PENINSULAR_MALAYSIA;
    else if (latitude >= LABUAN_MIN_LAT && latitude <= LABUAN_MAX_LAT && longitude >= LABUAN_MIN_LONG && longitude <= LABUAN_MAX_LONG)
        return LABUAN;
    else if (latitude >= EAST_MALAYSIA_MIN_LAT && latitude <= EAST_MALAYSIA_MAX_LAT && longitude >= EAST_MALAYSIA_MIN_LONG && longitude <= EAST_MALAYSIA_MAX_LONG)
        return EAST_MALAYSIA;
    else
        return REGION_UNKNOWN;
}*/

+ (BOOL)isSupportedTouchID
{
    BOOL isSupport = NO;
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
    {
        isSupport = YES;
    }
    else
    {
        // Could not evaluate policy; look at authError and present an appropriate message to user
        NSLog(@"device do not support Touch ID service");
        isSupport = NO;
    }
    
    return isSupport;
}

+ (NSString *) convertIntoMD5:(NSString *) string
{
    const char *cStr = [string UTF8String];
    unsigned char digest[16];
    
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *resultString = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [resultString appendFormat:@"%02x", digest[i]];
    return  resultString;
}

+ (UIImage *)thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time
{
    MPMoviePlayerViewController *video = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    UIImage *image = [video.moviePlayer thumbnailImageAtTime:time timeOption:MPMovieTimeOptionNearestKeyFrame];
    
    return image;
}

@end
