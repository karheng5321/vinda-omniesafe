//
//  ServerEnum.h
//  SCA
//
//  Created by kyTang on 21/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#ifndef ServerEnum_h
#define ServerEnum_h

typedef enum SubmitCellType
{
    SubmitCellTypeText = 0,
    SubmitCellTypeMedia
    
} SubmitCellType;

#endif /* ServerEnum_h */
