//
//  ResultApi.h
//  SmartHeart
//
//  Created by Siti on 3/27/14.
//  Copyright (c) 2014 Apps Design Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResultApi : NSObject {
	BOOL _status;
    NSInteger _errorNo;
	NSString *_errorMsg;
	id _result;
}

- (id)initWithStatus:(int)status errorNumber:(NSInteger)errorNo errorMessage:(NSString *)errorMsg;

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSInteger errorNo;
@property (nonatomic, retain) NSString *errorMsg;
@property (nonatomic, retain) id result;
@property (nonatomic) BOOL isType;

@end
