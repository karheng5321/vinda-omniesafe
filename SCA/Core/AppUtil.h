//
//  AppUtil.h
//  SmartHeart
//
//  Created by Siti on 5/20/14.
//  Copyright (c) 2014 Apps Design Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Constant.h"

/*typedef NS_ENUM (NSInteger, Region) {
    PENINSULAR_MALAYSIA = 0,
    LANGKAWI,
    LABUAN,
    EAST_MALAYSIA,
    REGION_UNKNOWN,
    LOCATION_SERVIS_DISABLE
};*/

@interface AppUtil : NSObject {
    NSURLConnection *_connection;
	NSMutableData *_data;
    NSString *_callId;
}

+ (BOOL)isFileExist:(NSString *)string;
+ (NSString *)makeDocumentFullPath:(NSString*)filename;
+ (BOOL)deleteFileAtPath:(NSString*)filePath;
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
+ (BOOL)writeData:(NSData *)data toFile:(NSString *)location;
+ (UIImage *)resizeImageForThumbnail:(UIImage*)image;
+ (UIImage *)resizeImageForUpload:(UIImage*)image;
+ (id)removeNullElementFromObject:(id)object;
//+ (BOOL)saveImageData:(NSData *)imageData fromUrl:(NSString *)imageUrl;
//+ (NSData *)getImageDataFromUrl:(NSString *)imageUrl;
+ (BOOL)saveData:(NSData *)data fromUrl:(NSString *)url;
+ (NSData *)dataFromUrl:(NSString *)url;
+ (BOOL)writeArray:(id)array forKey:(NSString *)key;
+ (id)arrayForKey:(NSString *)key;
+ (NSURL *)fileLocationFromUrl:(NSString *)url;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageFromUIView:(UIView*)view;
//+ (UIImage *)backButtonImage;
//+ (Region)getUserRegion;

+ (BOOL)writeContent:(id)content toFile:(NSString *)location;
+ (id)contentFromFile:(NSString *)location;


+ (BOOL)isSupportedTouchID;


+ (NSString *)convertIntoMD5:(NSString *) string;
+ (UIImage *)thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time;

@end
