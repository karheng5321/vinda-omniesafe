//
//  ResultApi.m
//  SmartHeart
//
//  Created by Siti on 3/27/14.
//  Copyright (c) 2014 Apps Design Studio. All rights reserved.
//

#import "ResultApi.h"

@implementation ResultApi

@synthesize status = _status;
@synthesize errorNo = _errorNo;
@synthesize errorMsg = _errorMsg;
@synthesize result = _result;

- (id)initWithStatus:(int)status errorNumber:(NSInteger)errorNo errorMessage:(NSString *)errorMsg
{
    self = [super init];
    if (self) {
		_status = status;
        _errorNo = errorNo;
		_errorMsg = errorMsg;
		_result = nil;
	}
	
	return self;
}

@end
