//
//  Constants.h
//  SCA
//
//  Created by kyTang on 21/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#define CELL_HEIGHT 50;
#define PICKER_CELL_HEIGHT 100;
#define VIDEO_DURATION 3.0;
#define FILE_UPLOAD_LIMIT 15000000

#define ACTIONSHEET_TAG_PHOTO 1001
#define ACTIONSHEET_TAG_VIDEO 1002

#define BULLET_BG_EMPTY_COLOR_HEX @"#E6E6E6"
#define BULLET_BG_FILLED_COLOR_HEX @"#4CAF25"

#define NSUSERDEFAULT_REMEMBER_ME_KEY @"NSUSERDEFAULT_REMEMBER_ME_KEY"
#define SAVED_USERNAME_KEY @"saved_username"
#define SAVED_PASSWORD_KEY @"saved_password"

#define REPORT_NO_LABEL @"Report No."
#define USERNAME_LABEL @"User Name"
#define DATETIME_LABEL @"Date and Time"
#define AREALOCATION_LABEL @"Area / Location"
#define AREAOWNER_LABEL @"Area Owner"
#define INCIDENTS_LABEL @"Incidents"
#define PICTUREVIDEO_LABEL @"Picture / Video Uploaded"
#define REMARK_LABEL @"Description"
#define STATUS_LABEL @"Status"
#define AFTERACTION_LABEL @"After Action Photo / Video"
#define SUBMITTER_LABEL @"Name of the submitter"
#define HAZARD_LABEL @"Type of Hazard"
#define CRITICALLY_LABEL @"Type of Criticallity"

#define SUBMIT_INSTRUCTION @"After you submit the report, you wouldn't be able to undo the action. Hit OK to confirm"
@end
