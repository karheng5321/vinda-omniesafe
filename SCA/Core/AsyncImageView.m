//
//  AppUtil.h
//
//  Created by Siti Norain Ishak on 5/20/14.
//

#import "AsyncImageView.h"
#import "AsyncApi.h"
#import "Constant.h"
#import "AppUtil.h"

@implementation AsyncImageView

@synthesize delegate;

static NSString * const GET_IMAGE_CALL_ID = @"getImage";

//- (id)initWithFrame:(CGRect)frame imageUrl:(NSString *)imageUrl objectKey:(NSString *)key
- (id)initWithFrame:(CGRect)frame imageUrl:(NSString *)imageUrl autoResize:(BOOL)resize downloadNew:(BOOL)redownload
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor clearColor];
        _resize = resize;
        
        if (!imageUrl.length)
        {
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Empty image URL." message:@"Please contact developer." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];*/
			return self;
        }
        
        _imageUrl = imageUrl;
        
        NSData *imageData = [AppUtil dataFromUrl:imageUrl];
        if (imageData)
        {
            self.image = [UIImage imageWithData:imageData];
            [self resizeToFullsize];
        }
        else
            [self getImage:imageUrl callId:GET_IMAGE_CALL_ID];
        
        if (redownload)
            [self getImage:imageUrl callId:GET_IMAGE_CALL_ID];
    }
    return self;
}

- (void)resizeToFullsize
{
    if (!_resize)
        return;
    
    float scale = self.image.size.width/self.frame.size.width;
    float height = self.image.size.height/scale;
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, height);
    
    if ([delegate respondsToSelector:@selector(imageView:resizeToSize:)])
        [delegate imageView:self resizeToSize:self.frame.size];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)getImage:(NSString *)imageUrl callId:(NSString *)callId
{
    /*if (self.branchPhoto)
     {
     self.branchPhotoView.image = self.branchPhoto;
     return;
     }*/
    
    AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
    [asyncApi downloadMedia:imageUrl :GET_IMAGE_CALL_ID];
}

- (void)reloadFromUrl:(NSString *)imageUrl
{
    //NSLog(@"reloadFromUrl: %@", imageUrl);
    self.image = nil;
    if (!imageUrl.length)
    {
        return;
    }
    
    _imageUrl = imageUrl;
    
    NSData *imageData = [AppUtil dataFromUrl:_imageUrl];
    if (imageData)
        self.image = [UIImage imageWithData:imageData];
    else
        [self getImage:_imageUrl callId:GET_IMAGE_CALL_ID];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
	//NSLog(@"callId: %@", callId);
	//NSLog(@"status: %d, error_msg: %@, result: %@", result.status, result.errorMsg, result.result);
    
    if ([callId isEqualToString:GET_IMAGE_CALL_ID])
	{
        if (result.status)
		{
            //NSString *objectKey = [callId stringByReplacingOccurrencesOfString:GET_IMAGE_CALL_ID withString:@""];
            
            NSData *imageData = (NSData *)result.result;
			self.image = [UIImage imageWithData:imageData];
            [self resizeToFullsize];
            
            //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //[defaults setObject:imageData forKey:_imageUrl];
            [AppUtil saveData:imageData fromUrl:_imageUrl];
            
            if ([delegate respondsToSelector:@selector(imageDownloaded:forFromUrl:)])
                [delegate imageDownloaded:self.image forFromUrl:_imageUrl];
		}
		else
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
			[alert show];
			return;
		}
	}
}

@end
