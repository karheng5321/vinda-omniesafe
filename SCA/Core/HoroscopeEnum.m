//
//  HoroscopeEnum.m
//
//  Created by Siti Norain on 1/24/13.
//

#import "HoroscopeEnum.h"

@implementation HoroscopeEnum

//+ (NSArray *)getAllHoroscope
//{
//	NSArray *allHoroscope = [NSArray arrayWithObjects:
//							 [HoroscopeEnum getHoroscopeDescription:CAPRICORN],
//							 [HoroscopeEnum getHoroscopeDescription:AQUARIUS],
//							 [HoroscopeEnum getHoroscopeDescription:PISCES],
//							 [HoroscopeEnum getHoroscopeDescription:ARIES],
//							 [HoroscopeEnum getHoroscopeDescription:TAURUS],
//							 [HoroscopeEnum getHoroscopeDescription:GEMINI],
//							 [HoroscopeEnum getHoroscopeDescription:CANCER],
//							 [HoroscopeEnum getHoroscopeDescription:LEO],
//							 [HoroscopeEnum getHoroscopeDescription:VIRGO],
//							 [HoroscopeEnum getHoroscopeDescription:LIBRA],
//							 [HoroscopeEnum getHoroscopeDescription:SCORPIO],
//							 [HoroscopeEnum getHoroscopeDescription:SAGITTARIUS],
//							 nil];
//	
//	return allHoroscope;
//}
//
//+ (NSString *)getHoroscopeDescription:(Horoscope)horoscopeEnum
//{
//	NSString *desc = nil;
//    
//    switch (horoscopeEnum) {
//		case CAPRICORN:
//            desc = @"Capricorn";
//            break;
//        case AQUARIUS:
//            desc = @"Aquarius";
//            break;
//        case PISCES:
//            desc = @"Pisces";
//            break;
//        case ARIES:
//            desc = @"Aries";
//            break;
//        case TAURUS:
//            desc = @"Taurus";
//            break;
//        case GEMINI:
//            desc = @"Gemini";
//            break;
//        case CANCER:
//            desc = @"Cancer";
//            break;
//        case LEO:
//            desc = @"Leo";
//            break;
//        case VIRGO:
//            desc = @"Virgo";
//            break;
//        case LIBRA:
//            desc = @"Libra";
//            break;
//        case SCORPIO:
//            desc = @"Scorpio";
//            break;
//        case SAGITTARIUS:
//            desc = @"Sagittarius";
//            break;
//        default:
//            break;
//    }
//    
//    return desc;
//}

/*+ (NSString *)getHoroscopeDate:(Horoscope)horoscopeEnum
{
	NSString *date = nil;
    
    switch (horoscopeEnum) {
		case CAPRICORN:
            date = @"1222";
            break;
        case AQUARIUS:
            date = @"Aquarius";
            break;
        case PISCES:
            desc = @"Pisces";
            break;
        case ARIES:
            desc = @"Aries";
            break;
        case TAURUS:
            desc = @"Taurus";
            break;
        case GEMINI:
            desc = @"Gemini";
            break;
        case CANCER:
            desc = @"Cancer";
            break;
        case LEO:
            desc = @"Leo";
            break;
        case VIRGO:
            desc = @"Virgo";
            break;
        case LIBRA:
            desc = @"Libra";
            break;
        case SCORPIO:
            desc = @"Scorpio";
            break;
        case SAGITTARIUS:
            desc = @"Sagittarius";
            break;
        default:
            break;
    }
    
    return desc;
}*/

//+ (Horoscope)getHoroscopeEnum:(NSDate *)birthday
//{
//	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//	[dateFormatter setDateFormat:@"MMdd"];
//	NSString *dateString = [dateFormatter stringFromDate:birthday];
//	int dateValue = [dateString intValue];
//	
//	if (dateValue >= 1222.0 || dateValue <= 0119.0)
//		return CAPRICORN;
//	else if (dateValue >= 0120.0 && dateValue <= 0218.0)
//		return AQUARIUS;
//	else if (dateValue >= 0218.0 && dateValue <= 0320.0)
//		return PISCES;
//	else if (dateValue >= 0321.0 && dateValue <= 0419.0)
//		return ARIES;
//	else if (dateValue >= 0420.0 && dateValue <= 0520.0)
//		return TAURUS;
//	else if (dateValue >= 0521.0 && dateValue <= 0620.0)
//		return GEMINI;
//	else if (dateValue >= 0621.0 && dateValue <= 0722.0)
//		return CANCER;
//	else if (dateValue >= 0723.0 && dateValue <= 0822.0)
//		return LEO;
//	else if (dateValue >= 0823.0 && dateValue <= 0922.0)
//		return VIRGO;
//	else if (dateValue >= 0923.0 && dateValue <= 1022.0)
//		return LIBRA;
//	else if (dateValue >= 1023.0 && dateValue <= 1121.0)
//		return SCORPIO;
//	else
//		return SAGITTARIUS;
//}

@end
