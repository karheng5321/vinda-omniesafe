//
//  GeneralHelper.m
//  SCA
//
//  Created by kyTang on 21/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "GeneralHelper.h"
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>


@implementation GeneralHelper

#pragma mark - Devices Screens Resolution

+ (BOOL) isDeviceiPhone4
{
    if ((int)[[UIScreen mainScreen] bounds].size.height == 480)
    {
        return YES;
    }
    return NO;
}

+ (BOOL) isDeviceiPhone5
{
    if ((int)[[UIScreen mainScreen] bounds].size.height == 568)
    {
        return YES;
    }
    return NO;
}

+ (BOOL) isDeviceiPhone6
{
    if ((int)[[UIScreen mainScreen] bounds].size.height == 667)
    {
        return YES;
    }
    return NO;
}

+ (BOOL) isDeviceiPhone6plus
{
    if ((int)[[UIScreen mainScreen] bounds].size.height == 736)
    {
        return YES;
    }
    return NO;
}

#pragma mark - Internet Connection Checking
+ (BOOL) hasConnection
{
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
    {
        return false;
    }
    return true;
}

#pragma mark - DateTime 
+ (NSString *) getCurrentDateTime {
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm a"];
    return [dateFormatter stringFromDate:[NSDate date]];
}

#pragma mark - Video
+ (UIImage *)generateThumbImage : (NSURL *)videoURL
{
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL: videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime requestedTime = CMTimeMake(1, 60);     // To create thumbnail image
    CGImageRef imgRef = [generator copyCGImageAtTime:requestedTime actualTime:NULL error:&err];
    NSLog(@"err = %@, imageRef = %@", err, imgRef);
    
    UIImage *thumbnailImage = [[UIImage alloc] initWithCGImage:imgRef];
    CGImageRelease(imgRef);    // MUST release explicitly to avoid memory leak
    
    return thumbnailImage;

}

@end
