//
//  AsyncApi.h
//  SmartHeart
//
//  Created by Siti on 3/27/14.
//  Copyright (c) 2014 Apps Design Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "ResultApi.h"
#import "Constant.h"

@protocol AsyncApiDelegate <NSObject>

@required

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result;

@optional
//For open ads
- (void)apiResponseAdsComplete:(NSString *)callId withResult:(ResultApi *)result;

@end

@interface AsyncApi : NSObject {
	id <AsyncApiDelegate> delegate;
	
	NSURLConnection *_connection;
	NSMutableData *_data;
	
	NSString *_callId;
    BOOL _isMedia;
    NSString *_mediaUrlString;
}

@property (nonatomic, retain)id <AsyncApiDelegate> delegate;

- (id)initWithCaller:(id)caller;

- (void)downloadMedia:(NSString *)mediaUrlString :(NSString *)callId;

//- (void)createUser:(NSString *)email password:(NSString *)password mobile:(NSString *)mobileNo firstname:(NSString *)firstname lastname:(NSString *)lastname country:(NSString *)countryCode :(NSString *)callId;
//- (void)createUser:(NSString *)email password:(NSString *)password mobile:(NSString *)mobileNo firstname:(NSString *)firstname lastname:(NSString *)lastname country:(NSString *)countryCode firstContactId:(NSString *)firstContactId :(NSString *)callId;
//- (void)createUser:(NSString *)email password:(NSString *)password mobile:(NSString *)mobileNo firstname:(NSString *)firstname lastname:(NSString *)lastname country:(NSString *)countryCode firstContactId:(NSString *)firstContactId :(NSString *)callId;

- (void)loginUser:(NSString *)username password:(NSString *)password language:(NSString *)language :(NSString *)callId;
- (void)updateDeviceToken:(NSString *)callId;
- (void)logoutFromServer:(NSString *)callId;
- (void)getAdminReportHistory:(NSString *)callId;
- (void)getAreaReportHistory:(NSString *)callId;
- (void)getUserReportHistory:(NSString *)callId;
- (void)report:(NSString *)reportId getDetails:(BOOL)sameSite :(NSString *)callId;
- (void)report:(NSString *)reportId close:(NSString *)callId;

- (void)getHazardControlList:(NSString *)callId;
- (void)getHazardList:(NSString *)callId;
- (void)getAreaList:(NSString *)callId;
- (void)getCriticalityList:(NSString *)callId;

- (void)submitReportHazard:(NSString *)hazardId area:(NSString *)areaId criticality:(NSString *)criticalityId picture:(NSData *)picData video:(NSData *)vidData thumbnail:(NSData *)thumbnailData remark:(NSString *)remark :(NSString *)callId;
- (void)report:(NSString *)reportId updateHazard:(NSString *)controlId picture:(NSData *)picData video:(NSData *)vidData thumbnail:(NSData *)thumbnailData remark:(NSString *)remark :(NSString *)callId;


- (void)bosUserReportHistory:(NSString *)callId;
- (void)bosAllReportHistory:(NSString *)callId;
- (void)bosGetBadgeNumber:(NSString *)callId;
- (void)bosReport:(NSString *)reportId getDetails:(BOOL)sameSite :(NSString *)callId;
- (void)bosGetLSRList:(NSString *)callId;
- (void)bosActionList:(NSString *)callId;

- (void)bosSearchEmpNo:(NSString *)empNo :(NSString *)callId;
- (void)user:(NSString *)userId reportBos:(NSString *)staffId lsr:(NSString *)lsrId behaviour:(NSString *)behaviour remark:(NSString *)remark :(NSString *)callId;
- (void)report:(NSString *)reportId takeAction:(NSString *)action :(NSString *)callId;

- (void)cancel;

@end
