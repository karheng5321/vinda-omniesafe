//
//  RiteColor.h
//  atmosferaApp
//
//  Created by Hafiz on 21/12/2015.
//  Copyright © 2015 Tang Kean Yong. All rights reserved.
//

#import "UIColor+UIColor.h"

#ifndef RiteColor_h
#define RiteColor_h

#define RITE_COLOR_MAGENTA                      [UIColor colorWithHexString:@"c51e4e"]
#define RITE_COLOR_MAGENTA_DARKER               [UIColor colorWithHexString:@"a51b43"]
#define RITE_COLOR_MAGENTA_LIGHTER              [UIColor colorWithHexString:@"e34673"]

#define RITE_COLOR_GREEN                        [UIColor colorWithHexString:@"47AE80"]

#define RITE_COLOR_BG_DEFAULT                   [UIColor colorWithHexString:@"E4F3F0"]
#define RITE_COLOR_BG_DEFAULT_DARKER            [UIColor colorWithHexString:@"202327"]
#define RITE_COLOR_BG_DEFAULT_LIGHTER           [UIColor colorWithHexString:@"E4F3F0"]
#define RITE_COLOR_BG_DEFAULT_LIGHTER_MORE      [UIColor colorWithHexString:@"474d54"]
#define RITE_COLOR_TEXT_PRIMARY                 [UIColor colorWithHexString:@"4A4A4A"]
#define RITE_COLOR_TEXT_SECONDARY               [UIColor colorWithHexString:@"4A4A4A"]
#define RITE_COLOR_TEXT_PLACEHOLDER             [UIColor colorWithHexString:@"4A4A4A"]
#define RITE_COLOR_TEXT_PLACEHOLDER_LIGHTER     [UIColor colorWithHexString:@"4A4A4A"]
#define RITE_COLOR_BG_DEFAULT_PROGRESS_VIEW     [UIColor colorWithHexString:@"E4F3F0"]

#define RITE_COLOR_TEXT_MAGENTA                 RITE_COLOR_MAGENTA

#define RITE_COLOR_TEXT_GREEN                   RITE_COLOR_GREEN

#define RITE_COLOR_CELL_DEFAULT                 [UIColor colorWithHexString:@"2c3136"]
#define RITE_COLOR_CELL_SPARATOR                [UIColor colorWithHexString:@"000000" alpha:0.2f]

#endif /* RiteColor_h */
