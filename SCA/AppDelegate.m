//
//  AppDelegate.m
//  SCA
//
//  Created by kyTang on 21/04/2016.
//  Copyright © 2016 ADS. All rights reserved.
//

#import "AppDelegate.h"
#import "AsyncApi.h"
#import "ReportDetailsViewController.h"
#import "CMNavBarNotificationView.h"
#import "Reachability.h"
#import "BOSDetailsViewController.h"

@interface AppDelegate () {
    NSString *_notifReportId;
    
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
    
    UIWindow *_indicatorView;
    UILabel *_indicatorLabel;
}

@end

@implementation AppDelegate

static NSString * const UPDATE_TOKEN_CALL_ID = @"updatedevicetoken";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self registerPushNotification:application];
    
    hostReach = [Reachability reachabilityWithHostname: @"www.apple.com"];
    [hostReach startNotifier];
    
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    
    wifiReach = [Reachability reachabilityForLocalWiFi];
    [wifiReach startNotifier];
    
    float screenWidth = [[UIScreen mainScreen] applicationFrame].size.width;
    
    _indicatorView = [[UIWindow alloc] initWithFrame:CGRectMake(0, 20.0, screenWidth, 20.0)];
    _indicatorView.backgroundColor = [UIColor clearColor];
    _indicatorView.windowLevel = UIWindowLevelStatusBar+1;
    
    UIImage *btnImage = [UIImage imageNamed:@"site_indicator.png"];
    float scale = btnImage.size.height/_indicatorView.frame.size.height;
    float btnWidth = btnImage.size.width/scale;
    float btnHeight = btnImage.size.height/scale;
    
    UIImageView *indicatorBg = [[UIImageView alloc] initWithFrame:CGRectMake(_indicatorView.frame.size.width-btnWidth, 0.0, btnWidth, btnHeight)];
    indicatorBg.image = btnImage;
    indicatorBg.contentMode = UIViewContentModeScaleAspectFit;
    [_indicatorView addSubview:indicatorBg];
    
    _indicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(btnHeight/2.0, 0.0, indicatorBg.frame.size.width-btnHeight/2.0, indicatorBg.frame.size.height)];
    _indicatorLabel.backgroundColor = [UIColor clearColor];
    _indicatorLabel.textColor = [UIColor blackColor];
    _indicatorLabel.text = @"";
    _indicatorLabel.textAlignment = NSTextAlignmentCenter;
    _indicatorLabel.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    [indicatorBg addSubview:_indicatorLabel];
    
    return YES;
}

- (void)setSiteIndicator:(NSString *)text
{
    _indicatorView.hidden = (text.length == 0);
    _indicatorLabel.text = text;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)registerPushNotification:(UIApplication *)application
{
    NSLog(@"registerPushNotification: %@", application);
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        //#ifdef __IPHONE_8_0
        NSLog(@"registerUserNotificationSettings");
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
        //#endif
    } else {
        NSLog(@"registerForRemoteNotificationTypes");
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
}

#pragma mark APNS CODE
/*
 * --------------------------------------------------------------------------------------------------------------
 *  BEGIN APNS CODE
 * --------------------------------------------------------------------------------------------------------------
 */

/**
 * Fetch and Format Device Token and Register Important Information to Remote Server
 */

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    NSLog(@"application didRegisterUserNotificationSettings");
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    NSLog(@"application didRegisterForRemoteNotificationsWithDeviceToken");
#if !TARGET_IPHONE_SIMULATOR
    
    // Prepare the Device Token for Registration (remove spaces and < >)
    NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"TOKEN : %@",deviceToken);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceToken forKey:NSUserDefaultKey_DEVICE_TOKEN];
    
    if ([defaults objectForKey:NSUserDefaultKey_USER])
    {
        AsyncApi *asyncApi = [[AsyncApi alloc] initWithCaller:self];
        [asyncApi updateDeviceToken:UPDATE_TOKEN_CALL_ID];
    }
    
#endif
}

/**
 * Failed to Register for Remote Notifications
 */
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
#if !TARGET_IPHONE_SIMULATOR
    
    NSLog(@"Error in registration. Error: %@", error);
    
#endif
}

- (void)handleNotification:(NSDictionary *)userInfo application:(UIApplication *)application needToOpenPage:(BOOL) isNeedToOpenPage{
    NSLog(@"remote notification: %@",[userInfo description]);
    NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    /*NSString *alert = [apsInfo objectForKey:@"alert"];
    
    if(application.applicationState == UIApplicationStateActive)
    {
        CMNavBarNotificationView *notification =
        [CMNavBarNotificationView notifyWithText:alert detail:nil andDuration:2.0];
        notification.textLabel.textColor = [UIColor whiteColor];
        notification.textLabel.backgroundColor = [UIColor clearColor];
        notification.detailTextLabel.textColor = [UIColor whiteColor];
        notification.detailTextLabel.backgroundColor = [UIColor clearColor];
        [notification.imageView setImage:[UIImage imageNamed:@"notif_icon.png"]];
        [notification.imageView setContentMode:UIViewContentModeScaleAspectFit];
        //[notification setBackgroundColor:[UIColor colorWithHexString:@"d82626"]];
        //[notification setBackgroundColor:[UIColor colorWithHexString:@"55c8eb"]];
        [notification setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
    }*/
    
    NSDictionary *body = [userInfo objectForKey:@"aps"];
    NSDictionary *payload = [body objectForKey:@"alert"];
    NSString *message = [payload objectForKey:@"body"];
    NSString *type = [payload objectForKey:@"push_type"];
    _notifReportId = [payload objectForKey:@"report_id"];
    
    NSLog(@"report_id: %@", _notifReportId);
    NSLog(@"application.applicationState: %ld", application.applicationState);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:NSUserDefaultKey_USER])
        return;
    
    if ([type isEqualToString:@"bos_omniesafe_pn"])
    {
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController *navigationController = (UINavigationController *)appDelegate.window.rootViewController;
        [navigationController popToRootViewControllerAnimated:NO];
        
        if(application.applicationState == UIApplicationStateActive)
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                BOSDetailsViewController *detailsVC = [[BOSDetailsViewController alloc] initWithId:[_notifReportId integerValue] fromNotification:YES sameSite:YES];
                [navigationController pushViewController:detailsVC animated:YES];
            }];
            
            [alertController addAction:cancelAction];
            
            [navigationController presentViewController:alertController animated:YES completion:nil];
            return;
        }
        
        BOSDetailsViewController *detailsVC = [[BOSDetailsViewController alloc] initWithId:[_notifReportId integerValue] fromNotification:YES sameSite:YES];
        [navigationController pushViewController:detailsVC animated:YES];
    }
    else
    {
        if(application.applicationState == UIApplicationStateActive)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            alert.tag = [_notifReportId integerValue];
            [alert show];
            return;
        }
        
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UINavigationController *navigationController = (UINavigationController *)appDelegate.window.rootViewController;
        [navigationController popToRootViewControllerAnimated:NO];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ReportDetailsViewController *detailsVC = [storyboard instantiateViewControllerWithIdentifier:@"ReportDetailsViewController"];
        detailsVC.reportNo = _notifReportId;
        detailsVC.fromNotification = YES;
        detailsVC.sameSite = YES;
        [navigationController pushViewController:detailsVC animated:YES];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    [self handleNotification:userInfo application:application needToOpenPage:YES];
    
}

- (BOOL)isInternetAvailable
{
    NetworkStatus internetStatus = [internetReach currentReachabilityStatus];
    NetworkStatus wifiStatus = [wifiReach currentReachabilityStatus];
    
    if ((internetStatus == NotReachable) && (wifiStatus == NotReachable))
        return NO;
    else
        return YES;
}

- (void)proceedLogout
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:NSUserDefaultKey_USER];
    [ReportDetailsModel clearAllReportDetailKey];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.window setRootViewController:navigationController];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:NSUserDefaultKey_USER])
        return;
    
    AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navigationController = (UINavigationController *)appDelegate.window.rootViewController;
    [navigationController popToRootViewControllerAnimated:NO];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ReportDetailsViewController *detailsVC = [storyboard instantiateViewControllerWithIdentifier:@"ReportDetailsViewController"];
    detailsVC.reportNo = [NSString stringWithFormat:@"%ld", alertView.tag];
    detailsVC.fromNotification = YES;
    detailsVC.sameSite = YES;
    [navigationController pushViewController:detailsVC animated:YES];
}

#pragma mark - AsyncApiDelegate

- (void)apiResponseComplete:(NSString *)callId withResult:(ResultApi *)result
{
    NSLog(@"callId: %@", callId);
    NSLog(@"status: %d, error_no: %ld, error_msg: %@\nresult: %@", result.status, (long)result.errorNo, result.errorMsg, result.result);
    
    if ([callId isEqualToString:UPDATE_TOKEN_CALL_ID])
    {
        if (result.status)
        {
            return;
        }
        else if (result.errorNo == NO_DATA_AVAILABLE)
        {
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:result.errorMsg message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
            [alert show];
            return;
        }
    }
}

@end
